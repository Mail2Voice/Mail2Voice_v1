Mail2Voice
==========

E-Mail client software dedicated to people with cognitive impairment or illiteracy,
or for the elderly persons and young children.


Design Principles
=================

  * Mail2Voice proposes a simplified graphical interface usable with a touch screen.
  * Voice recording is used to send messages (audio file attached).
  * Received messages can be read with speech synthesizer.
  * Addressbook displays portraits to make contacts selection easier.
  


  
More infos
==========

Users can visit the website : http://www.mail2voice.org
Developpers can visit the wiki : http://wiki.mail2voice.org

