﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    public class ConfigSettings
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        public bool Valid = false;
        private static ConfigSettings instance = null;

        private string configurationFilename = string.Empty;
        private Dictionary<string, string> config = new Dictionary<string, string>();
        private Dictionary<string, string> defaultConfig = null;

        public static ConfigSettings Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new ConfigSettings();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static ConfigSettings()
        {
            instance = new ConfigSettings();
            instance.InitDefaultConfig();
        }
            
        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/
        public bool Load(string configFilename)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            this.configurationFilename = configFilename;
            return this.LoadConfigurationFile(this.configurationFilename);
        }

        public void InitDefaultConfig()
        {
            if (this.defaultConfig == null)
            {
                this.defaultConfig = new Dictionary<string, string>();
            }
            else
            {
                this.defaultConfig.Clear();
            }
                
            this.defaultConfig.Add(Constants.CONFIG_EMAIL_LOGIN, string.Empty);
            this.defaultConfig.Add(Constants.CONFIG_EMAIL_PASSWORD, string.Empty);
            this.defaultConfig.Add(Constants.CONFIG_IMAP_ADDRESS, string.Empty);
            this.defaultConfig.Add(Constants.CONFIG_IMAP_PORT, Constants.VALUE_IMAP_PORT_SECURED);
            this.defaultConfig.Add(Constants.CONFIG_IMAP_SSL, Constants.TRUE);
            this.defaultConfig.Add(Constants.CONFIG_SMTP_ADDRESS, string.Empty);
            this.defaultConfig.Add(Constants.CONFIG_SMTP_PORT, Constants.VALUE_SMTP_PORT_UNSECURED);   // Keep unsecured port config by default due to compatibility issues
            this.defaultConfig.Add(Constants.CONFIG_SMTP_NAME, string.Empty);
            this.defaultConfig.Add(Constants.CONFIG_SMTP_SSL, Constants.FALSE);
            this.defaultConfig.Add(Constants.CONFIG_LOG_ACTIVATE, Constants.FALSE);
            this.defaultConfig.Add(Constants.CONFIG_SEND_SUBJECT, @"Vocal message");
            this.defaultConfig.Add(Constants.CONFIG_SEND_CONTENT, @"This email is a vocal message. Open the audio file attached to listen to it.");
            this.defaultConfig.Add(Constants.CONFIG_TRANSLATION_LANG, "english");
            this.defaultConfig.Add(Constants.CONFIG_LANG, string.Empty);
            this.defaultConfig.Add(Constants.CONFIG_MAIN_BUTTONS_HOVER_SPEECH_PREVIEW, Constants.TRUE);
            this.defaultConfig.Add(Constants.CONFIG_EMAIL_ACTIVATE_TRASH, Constants.TRUE);
            this.defaultConfig.Add(Constants.CONFIG_EMAIL_DELAYS, "5");
            this.defaultConfig.Add(Constants.CONFIG_KEYBOARD, "english.kb");
            this.defaultConfig.Add(Constants.CONFIG_UPDATE, Constants.TRUE);
            this.defaultConfig.Add(Constants.CONFIG_UPDATE_REPEAT, "1"); // Check for updates each week by default
            this.defaultConfig.Add(Constants.CONFIG_UPDATE_LAST_CHECK, DateTime.MinValue.ToString());
            this.defaultConfig.Add(Constants.CONFIG_ACTIVATE_KEYBOARD, Constants.FALSE);
            this.defaultConfig.Add(Constants.CONFIG_SEND_USAGE_STATS, Constants.TRUE);
        }

        public bool SetConfigToDefault(string configFilename)
        {
            this.config.Clear();

            foreach (string key in this.defaultConfig.Keys)
            {
                this.config.Add(key, this.defaultConfig[key]);
            }

            return this.SaveConfiguration();
        }

        // Create folders for current user account if they don't exist.
        public void InitUserAccountFolders()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            // Create folders if needed
            if (!Directory.Exists(Constants.CONFIG_CONTACTS_FOLDER))
            {
                Directory.CreateDirectory(Constants.CONFIG_CONTACTS_FOLDER);
            }

            // Create user folders if defined
            if (!string.IsNullOrEmpty(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN)))
            {
                // Define usable user folders
                string emailsFolderName = Constants.CONFIG_EMAIL_FOLDER;
                string login = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN);

                string inbox = emailsFolderName + login + "/" + Constants.EMAIL_INBOX;
                string trash = emailsFolderName + login + "/" + Constants.EMAIL_TRASH;

                Constants.EMAIL_USER_FOLDER = emailsFolderName;
                Constants.EMAIL_USER_FOLDER_INBOX = inbox + "/";
                Constants.EMAIL_USER_FOLDER_INBOX_READ = Constants.EMAIL_USER_FOLDER_INBOX + Constants.EMAIL_READ + "/";
                Constants.EMAIL_USER_FOLDER_INBOX_UNREAD = Constants.EMAIL_USER_FOLDER_INBOX + Constants.EMAIL_UNREAD + "/";

                Constants.EMAIL_USER_FOLDER_TRASH = trash + "/";
                Constants.EMAIL_USER_FOLDER_TRASH_READ = Constants.EMAIL_USER_FOLDER_TRASH + Constants.EMAIL_READ + "/";
                Constants.EMAIL_USER_FOLDER_TRASH_UNREAD = Constants.EMAIL_USER_FOLDER_TRASH + Constants.EMAIL_UNREAD + "/";

                Constants.EMAIL_USER_FOLDER_SENT = emailsFolderName + login + "/" + Constants.EMAIL_SENT + "/";

                if (!string.IsNullOrEmpty(Constants.EMAIL_USER_FOLDER)) 
                {
                    if (!Directory.Exists(Constants.EMAIL_USER_FOLDER))
                    {
                        Directory.CreateDirectory(Constants.EMAIL_USER_FOLDER);
                    }
                }

                if (!Directory.Exists(Constants.EMAIL_USER_FOLDER_SENT))
                {
                    Directory.CreateDirectory(Constants.EMAIL_USER_FOLDER_SENT);
                }

                if (!Directory.Exists(Constants.EMAIL_USER_FOLDER_INBOX))
                {
                    Directory.CreateDirectory(Constants.EMAIL_USER_FOLDER_INBOX);
                }

                if (!Directory.Exists(Constants.EMAIL_USER_FOLDER_INBOX_READ))
                {
                    Directory.CreateDirectory(Constants.EMAIL_USER_FOLDER_INBOX_READ);
                }

                if (!Directory.Exists(Constants.EMAIL_USER_FOLDER_INBOX_UNREAD))
                {
                    Directory.CreateDirectory(Constants.EMAIL_USER_FOLDER_INBOX_UNREAD);
                }

                if (!Directory.Exists(Constants.EMAIL_USER_FOLDER_TRASH))
                {
                    Directory.CreateDirectory(Constants.EMAIL_USER_FOLDER_TRASH);
                }
                    
                if (!Directory.Exists(Constants.EMAIL_USER_FOLDER_TRASH_READ))
                {
                    Directory.CreateDirectory(Constants.EMAIL_USER_FOLDER_TRASH_READ);
                }

                if (!Directory.Exists(Constants.EMAIL_USER_FOLDER_TRASH_UNREAD))
                {
                    Directory.CreateDirectory(Constants.EMAIL_USER_FOLDER_TRASH_UNREAD);
                }
            }
        }

        // Save current configuration in file.
        public bool SaveConfiguration()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            bool saveSuccess = false;

            try
            {
                XDocument document = new XDocument(new XElement("Settings"));

                foreach (KeyValuePair<string, string> kvp in this.config)
                {
                    document.Element("Settings").Add(
                        new XElement(
                            "Setting",
                            new XAttribute("name", kvp.Key),
                            new XAttribute("value", kvp.Value)));
                }

                document.Save(this.configurationFilename);
                saveSuccess = true;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }

            return saveSuccess;
        }

        // Return a setting value from settings list.
        public string GetSettingValue(string settingName)
        {
            string settingValue = string.Empty;

            if (this.config.ContainsKey(settingName))
            {
                settingValue = this.config[settingName].Trim();
            }

            if (settingValue == string.Empty && this.defaultConfig.ContainsKey(settingName))
            {
                settingValue = this.defaultConfig[settingName].Trim();
            }

            return settingValue;
        }

        // Update or add a setting values
        public void SetSettingValue(string name, string value)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "name: " + name + ", value: " + value);

            this.config[name] = value;
        }

        public bool IsSettingSet(string settingName)
        {
            return !string.IsNullOrEmpty(this.GetSettingValue(settingName));
        }

        public bool IsComplete()
        {
            string[] settingsNames = new string[]
            {
                Constants.CONFIG_EMAIL_LOGIN,
                Constants.CONFIG_EMAIL_PASSWORD,
                Constants.CONFIG_EMAIL_PASSWORD,
                Constants.CONFIG_IMAP_ADDRESS,
                Constants.CONFIG_IMAP_PORT,
                Constants.CONFIG_IMAP_SSL,
                Constants.CONFIG_SMTP_ADDRESS,
                Constants.CONFIG_SMTP_PORT,
                Constants.CONFIG_SMTP_NAME,
                Constants.CONFIG_SMTP_SSL,
                Constants.CONFIG_LOG_ACTIVATE,
                Constants.CONFIG_SEND_SUBJECT,
                Constants.CONFIG_SEND_CONTENT,
                Constants.CONFIG_TRANSLATION_LANG,
                Constants.CONFIG_LANG,
                Constants.CONFIG_MAIN_BUTTONS_HOVER_SPEECH_PREVIEW,
                Constants.CONFIG_EMAIL_ACTIVATE_TRASH,
                Constants.CONFIG_EMAIL_DELAYS,
                Constants.CONFIG_KEYBOARD,
                Constants.CONFIG_UPDATE,
                Constants.CONFIG_UPDATE_REPEAT,
                Constants.CONFIG_UPDATE_LAST_CHECK,
                Constants.CONFIG_ACTIVATE_KEYBOARD,
                Constants.CONFIG_SEND_USAGE_STATS
            };

            return settingsNames.FirstOrDefault(v => !this.IsSettingSet(v)) == null;
        }

        /***************************
         *      Private methods
         ***************************/
        private bool LoadConfigurationFile(string filename)
        {
            bool loadSuccess = false;

            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            try
            {
                if (File.Exists(this.configurationFilename))
                {
                    XDocument xmlDoc = XDocument.Load(filename);

                    this.config.Clear();

                    xmlDoc.Descendants("Settings").Elements().ToList().ForEach(
                        v => this.config.Add(
                            v.Attribute("name").Value,
                            v.Attribute("value").Value));

                    loadSuccess = true;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }

            return loadSuccess;
        }
    }
}