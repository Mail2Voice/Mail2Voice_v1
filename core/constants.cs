﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;

    public class Constants
    {
        // Password and key
        public const int PASSWORD_KEY = 129;
        public const string FILE_KEY = "password";

        // Application folders
        public const string CONFIG_FILE = "./config.xml";
        public const string CONFIG_CONTACTS_FILE = "./contacts/contacts.xml";

        public const string CONFIG_EMAIL_FOLDER = "./emails/";
        public const string CONFIG_LANGS_FOLDER = "./locales/";
        public const string CONFIG_CONTACTS_FOLDER = "./contacts/";
        public const string CONFIG_THEMES_FOLDER = "./themes/";
        public const string CONFIG_LOG_FILE = "./log.txt";
        public const string CONFIG_MP3_TEMP_FILE = "./message.mp3";

        public const string MAIL2VOICE_EXTENSION = ".m2v";

        #region "Configuration fields definition"
        public const string CONFIG_THEME = "theme";
        public const string CONFIG_LOG_ACTIVATE = "log_activate";

        public const string CONFIG_EMAIL_ACTIVATE_TRASH = "email_activate_trash";

        public const string CONFIG_UPDATE = "update";
        public const string CONFIG_UPDATE_REPEAT = "update_repeat";
        public const string CONFIG_UPDATE_LAST_CHECK = "update_lastcheck";
        public const string CONFIG_KEYBOARD = "keyboard";
        public const string CONFIG_ACTIVATE_KEYBOARD = "activate_keyboard";
        public const string CONFIG_EMAIL_DELAYS = "email_delays";
        public const string CONFIG_SEND_SUBJECT = "send_subject";
        public const string CONFIG_SEND_CONTENT = "send_content";
        public const string CONFIG_IMAP_ADDRESS = "imap_address";
        public const string CONFIG_IMAP_PORT = "imap_port";
        public const string CONFIG_IMAP_SSL = "imap_ssl";
        public const string CONFIG_SMTP_ADDRESS = "smtp_address";
        public const string CONFIG_SMTP_PORT = "smtp_port";
        public const string CONFIG_SMTP_NAME = "smtp_name";
        public const string CONFIG_SMTP_SSL = "smtp_ssl";
        public const string CONFIG_EMAIL_LOGIN = "email_login";
        public const string CONFIG_EMAIL_PASSWORD = "email_password";
        public const string CONFIG_SEND_USAGE_STATS = "send_usage_stats";
        public const string VALUE_IMAP_PORT_SECURED = "993";
        public const string VALUE_IMAP_PORT_UNSECURED = "143";
        public const string VALUE_SMTP_PORT_SECURED = "465";
        public const string VALUE_SMTP_PORT_UNSECURED = "25";

        public const string CONFIG_LANG = "lang";
        public const string CONFIG_MAIN_BUTTONS_HOVER_SPEECH_PREVIEW = "main_buttons_hover_speech_preview";
        public const string CONFIG_TRANSLATION_LANG = "translation_lang";
        #endregion "Configuration fields definition"

        public const string TYPE_IMAGE = "Image";
        public const string TYPE_AUDIO = "Audio";
        public const string TYPE_VIDEO = "Video";
        
        public const string EXTENSION_EML = ".eml";
        public const string EXTENSION_JPG = ".jpg";
        public const string EXTENSION_PNG = ".png";
        public const string EXTENSION_MP3 = ".mp3";

        public const string VOICE_SENTENCES_SECTION = "voiceSentences";

        public const string EMAIL_SENT = "SENT";
        public const string EMAIL_INBOX = "INBOX";
        public const string EMAIL_TRASH = "TRASH";

        public const string EMAIL_UNREAD = "UNSEEN";
        public const string EMAIL_READ = "SEEN";

        public const string InboxView = "INBOX_VIEW";
        public const string TrashView = "TRASH_VIEW";

        public const string ApplicationParentTag = "Mail2Voice";

        // Language file definition
        public const string LanguageCultureName = "CultureName";
        public const string LanguageParentTag = "Language";
        public const string LanguageName = "name";
        public const string LanguageValue = "value";

        // Theme file definition
        public const string ThemeParentTag = "Theme";
        public const string ThemeName = "name";
        public const string ThemeBackColor = "backColor";
        public const string ThemeForeColor = "foreColor";

        public const string TRUE = "True";
        public const string FALSE = "False";

        public static string EMAIL_USER_FOLDER { get; set; }

        public static string EMAIL_USER_FOLDER_INBOX { get; set; }

        public static string EMAIL_USER_FOLDER_INBOX_READ { get; set; }

        public static string EMAIL_USER_FOLDER_INBOX_UNREAD { get; set; }

        public static string EMAIL_USER_FOLDER_TRASH { get; set; }

        public static string EMAIL_USER_FOLDER_TRASH_READ { get; set; }

        public static string EMAIL_USER_FOLDER_TRASH_UNREAD { get; set; }

        public static string EMAIL_USER_FOLDER_SENT { get; set; }
    }
}
