﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    public class ContactsObject
    {
        /***************************
         *      Attributes
         ***************************/
        private static ContactsObject instance = null;

        private List<Contact> contactsList = null;
        private string defaultStorageFolder = string.Empty;
        private string contactsConfigFile = string.Empty; 
        private string contactsImagesFolder = string.Empty; 

        #region "Singleton"
        public static ContactsObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new ContactsObject();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static ContactsObject()
        {
            instance = new ContactsObject();
        }

        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/
        public List<Contact> GetContactsList()
        {
            return this.contactsList;
        }

        // Setup the default folder where contacts will be stored
        public void SetStorageFolder(string folder)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "folder: " + folder);

            this.defaultStorageFolder = folder;
            this.contactsConfigFile = this.defaultStorageFolder + "\\" + Constants.CONFIG_CONTACTS_FILE;
            this.contactsImagesFolder = this.defaultStorageFolder + "\\" + Constants.CONFIG_CONTACTS_FOLDER;
        }

        // Load contacts list from xml files
        public void LoadFromXml()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            try
            {
                this.contactsList = new List<Contact>();

                if (File.Exists(this.contactsConfigFile))
                {
                    XElement contacts = XElement.Load(this.contactsConfigFile);

                    // Read each contact entry and stores them in the internal contacts list
                    foreach (object o in contacts.Nodes())
                    {
                        this.contactsList.Add(
                            new Contact()
                            {
                            Email = ((XElement)o).Element("Email").Value.ToString(),
                            Name = ((XElement)o).Element("Name").Value.ToString()
                            });
                    }
                        
                    // Read linked images
                    for (int x = 0; x < this.contactsList.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(this.contactsList[x].Email))
                        {
                            string fileName = this.contactsImagesFolder + this.contactsList[x].Email + Constants.EXTENSION_JPG;

                            if (File.Exists(fileName))
                            {
                                Stream stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Delete);
                                this.contactsList[x].Image = Image.FromStream(stream);
                                stream.Close();
                            }
                            else
                            {
                                this.contactsList[x].Image = null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        // Save contacts list to xml file
        public void SaveToXml()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            try
            {
                XmlTextWriter myXmlTextWriter = new XmlTextWriter(this.contactsConfigFile, System.Text.Encoding.UTF8);
                myXmlTextWriter.Formatting = Formatting.Indented;
                myXmlTextWriter.WriteStartDocument(false);
                myXmlTextWriter.WriteComment("Last record: " + DateTime.Now.ToString());

                myXmlTextWriter.WriteStartElement("Contacts");
                for (int x = 0; x < this.contactsList.Count; x++)
                {
                    myXmlTextWriter.WriteStartElement("Contact", null);
                    myXmlTextWriter.WriteElementString("Name", null, this.contactsList[x].Name);
                    myXmlTextWriter.WriteElementString("Email", null, this.contactsList[x].Email);
                    myXmlTextWriter.WriteEndElement();
                }

                myXmlTextWriter.WriteEndElement();

                myXmlTextWriter.Flush();
                myXmlTextWriter.Close();
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void Add(Contact contactToAdd)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            
            if (contactToAdd == null)
            {
              return;
            }

            try
            {
                Contact contact = this.contactsList.FirstOrDefault(var => var.Email.Equals(contactToAdd.Email));
                if (contact == null)
                {
                    this.contactsList.Add(contactToAdd);

                    if (!string.IsNullOrEmpty(contactToAdd.Email) && contactToAdd.Image != null)
                    {
                        string fileName = this.contactsImagesFolder + contactToAdd.Email + ".jpg";
                        FileStream fileStreamWrite = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        MemoryStream memStream = new MemoryStream();
                        contactToAdd.Image.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        memStream.WriteTo(fileStreamWrite);
                        memStream.Close();
                        fileStreamWrite.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void Add(List<Contact> contactsToAdd)
        {
            foreach (Contact contact in contactsToAdd)
            {
                this.Add(contact);
            }
        }

        public void Delete(string email)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            
            if (string.IsNullOrEmpty(email))
            {
              return;
            }
            
            try
            {
                Contact contact = this.contactsList.FirstOrDefault(var => var.Email.Equals(email));
                if (contact != null)
                {
                    this.Delete(contact);
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void Delete(Contact contactToDelete)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            try
            {
                if (contactToDelete != null)
                {
                    this.contactsList.Remove(contactToDelete);

                    string fileName = this.defaultStorageFolder + "\\" + Constants.CONFIG_CONTACTS_FOLDER + contactToDelete.Email + ".jpg";
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void Update(Contact contactToUpdate, string email)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            try
            {
                if (this.contactsList != null)
                {
                    Contact contact = this.contactsList.FirstOrDefault(var => var.Email.Equals(email));
                    if (contact != null)
                    {
                        this.contactsList.Remove(contact);
                    }

                    this.contactsList.Add(contactToUpdate);
                    string fileName = this.defaultStorageFolder + "\\" + Constants.CONFIG_CONTACTS_FOLDER + email + ".jpg";
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }

                    FileStream fileStreamWrite = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    MemoryStream memStream = new MemoryStream();
                    contactToUpdate.Image.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    memStream.WriteTo(fileStreamWrite);
                    memStream.Close();
                    fileStreamWrite.Close();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public bool ContactExists(string email)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            return this.GetContact(email) != null;
        }

        public Contact GetContact(string email)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            if (string.IsNullOrEmpty(email))
            {
              return null;
            }

            Contact contact = null;
            try
            {
                if (this.contactsList != null)
                {
                    contact = this.contactsList.FirstOrDefault(var => var.Email.Equals(email));
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }

            return contact;
        }
    }

    public class Contact
    {
        /***************************
         *      Attributes
         ***************************/
        public string Email { get; set; } // Unique identifier
        public string Name { get; set; }
        public Image Image { get; set; }

        /***************************
         *      Constructor
         ***************************/
        public Contact()
        {
            this.Email = string.Empty;
            this.Name = string.Empty;
            this.Image = null;
        }
    }
}
