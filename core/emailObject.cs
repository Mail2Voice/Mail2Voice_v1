﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using CDO;

    public class EmailObject
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static EmailObject instance = null;

        public static EmailObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new EmailObject();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static EmailObject()
        {
            instance = new EmailObject();
        }

        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/
        #region "Inbox"
        public List<ReceiveMessage> LoadEmails(string window)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "window:" + window);
            List<ReceiveMessage> readEmailsList = null;
            List<ReceiveMessage> unreadEmailsList = null;
            List<ReceiveMessage> allEmailsList = new List<ReceiveMessage>();

            if (window == Constants.InboxView)
            {
                readEmailsList = this.LoadEmails(Constants.EMAIL_USER_FOLDER_INBOX_READ, Constants.EMAIL_READ);
                unreadEmailsList = this.LoadEmails(Constants.EMAIL_USER_FOLDER_INBOX_UNREAD, Constants.EMAIL_UNREAD);
            }
            else
            {
                readEmailsList = this.LoadEmails(Constants.EMAIL_USER_FOLDER_TRASH_READ, Constants.EMAIL_READ);
                unreadEmailsList = this.LoadEmails(Constants.EMAIL_USER_FOLDER_TRASH_UNREAD, Constants.EMAIL_UNREAD);
            }

            if (readEmailsList != null)
            {
                allEmailsList.AddRange(readEmailsList);
            }

            if (unreadEmailsList != null)
            {
                allEmailsList.AddRange(unreadEmailsList);
            }
                
            // Sort emails by date
            allEmailsList.Sort((x, y) => DateTime.Compare(y.Date, x.Date));

            return allEmailsList;
        }

        /***************************
         *      Private methods
         ***************************/
        private List<ReceiveMessage> LoadEmails(string folderName, string status)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "folderName: " + folderName + ", status: " + status);

            List<ReceiveMessage> emailsList = new List<ReceiveMessage>();
            try
            {
                DropDirectory dropDir = new DropDirectory();
                IMessages messages;

                // Get the messages from the Drop directory.
                string exePath = System.Reflection.Assembly.GetEntryAssembly().Location;
                string exeDir = Path.GetDirectoryName(exePath);
                messages = dropDir.GetMessages(exeDir + folderName);

                foreach (IMessage msg in messages)
                {
                    ReceiveMessage recvMsg = new ReceiveMessage();

                    recvMsg.FileName = messages.get_FileName(msg);
                    recvMsg.Id = Path.GetFileNameWithoutExtension(messages.get_FileName(msg));
                    recvMsg.Subject = msg.Subject;

                    if (msg.Fields["urn:schemas:mailheader:content-type"].Value != null)
                    {
                        string typeField = msg.Fields["urn:schemas:mailheader:content-type"].Value.ToString().ToLower();

                        if (typeField.Contains("html") | typeField.Contains("multipart"))
                        {
                            recvMsg.Type = "html";
                            if (msg.HTMLBody != null)
                            {
                                recvMsg.Content = this.GetMsgContentWithEmbedImages(msg);
                            }
                            else
                            {
                                recvMsg.Content = string.Empty;
                            }
                        }
                        else
                        {
                            recvMsg.Type = "plain";
                            recvMsg.Content = msg.TextBody.Trim();
                        }
                    }

                    if (msg.Fields["urn:schemas:httpmail:datereceived"].Value != null)
                    {
                        recvMsg.Date = Convert.ToDateTime(msg.Fields["urn:schemas:httpmail:datereceived"].Value.ToString());
                    }
                    else if (msg.Fields["urn:schemas:mailheader:date"].Value != null)
                    {
                        recvMsg.Date = Convert.ToDateTime(msg.Fields["urn:schemas:mailheader:date"].Value.ToString());
                    }
                    else
                    {
                        // Can't determine original date
                    }

                    if (msg.Fields["urn:schemas:httpmail:sendername"].Value != null)
                    {
                        recvMsg.SenderName = msg.Fields["urn:schemas:httpmail:sendername"].Value.ToString();
                    }

                    if (msg.Fields["urn:schemas:httpmail:senderemail"].Value != null)
                    {
                        recvMsg.SenderEmail = msg.Fields["urn:schemas:httpmail:senderemail"].Value.ToString();
                    }

                    if (msg.Fields["urn:schemas:httpmail:fromemail"].Value != null)
                    {
                        string tmp = msg.Fields["urn:schemas:httpmail:fromemail"].Value.ToString();
                        recvMsg.FromEmail = tmp.Substring(1, tmp.Length - 2).ToLower();
                    }

                    if (msg.Fields["urn:schemas:httpmail:from"].Value != null)
                    {
                        string tmp = msg.Fields["urn:schemas:httpmail:from"].Value.ToString();
                        recvMsg.FromName = tmp.Substring(1, tmp.IndexOf("\"", 1) - 1);
                    }

                    if (msg.Fields["urn:schemas:httpmail:sender"].Value != null)
                    {
                        recvMsg.Sender = msg.Fields["urn:schemas:httpmail:sender"].Value.ToString();
                    }

                    if (msg.Attachments.Count > 0)
                    {
                        if (!Directory.Exists(folderName + recvMsg.Id))
                        {
                            Directory.CreateDirectory(folderName + recvMsg.Id);
                            for (int x = 1; x <= msg.Attachments.Count; x++)
                            {
                                msg.Attachments[x].SaveToFile(exeDir + folderName + recvMsg.Id + "/" + msg.Attachments[x].FileName);
                            }
                        }

                        recvMsg.HasAttachment = true;
                    }

                    if (status == Constants.EMAIL_READ)
                    {
                        recvMsg.IsRead = true;
                    }

                    emailsList.Add(recvMsg);
                }

                messages = null;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }

            return emailsList;
        }

        string GetMsgContentWithEmbedImages(IMessage msg)
        {
            string tempContent = this.SanitizeHTML(msg.HTMLBody);
            Regex embedImageUriIdRgx = new Regex("src=.cid:[^\\\"]+", RegexOptions.IgnoreCase | RegexOptions.Compiled);

            MatchCollection embedImagesUris = embedImageUriIdRgx.Matches(tempContent);

            List<string> imageIds = new List<string>();

            foreach (Match imageUri in embedImagesUris)
            {
                imageIds.Add(imageUri.ToString().Substring(5));
            }

            if (imageIds.Count > 0)
            {
                string messageContent = string.Empty;

                try 
                {
                   messageContent = msg.GetStream().ReadText(-1);
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                    LogObject.Singleton.Write("Failed to get text stream from email");
                }

                if (messageContent != string.Empty)
                {
                    string[] emailParts = messageContent.Split(new string[] { "------=_Part" }, StringSplitOptions.RemoveEmptyEntries);
                
                    foreach (string emailPart in emailParts)
                    {
                        Regex contentTypeJpgRgx = new Regex("[cC]ontent-[tT]ype: *image/(jpeg|jpg)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                        Regex contentTypePngRgx = new Regex("[cC]ontent-[tT]ype: *image/png", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                        Regex contentEncodingBase64Rgx = new Regex("[cC]ontent-[tT]ransfer-[eE]ncoding: *base64", RegexOptions.IgnoreCase | RegexOptions.Compiled);

                        string contentType = string.Empty;

                        if (contentTypePngRgx.IsMatch(emailPart))
                        {
                            contentType = "image/png";
                        }
                        else if (contentTypeJpgRgx.IsMatch(emailPart))
                        {
                            contentType = "image/jpeg";
                        }

                        if (contentType != string.Empty && contentEncodingBase64Rgx.IsMatch(emailPart))
                        {
                            StringReader reader = new StringReader(emailPart);

                            string imageId = string.Empty;
                            string imageBase64 = string.Empty;
                            string line = string.Empty;
                            Regex base64EncodingRgx = new Regex("^(?:[A-Za-z0-9\\+/]{4})*(?:[A-Za-z0-9\\+/]{2}==|[A-Za-z0-9\\+/]{3}=|[A-Za-z0-9\\+/]{4})$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

                            while ((line = reader.ReadLine()) != null)
                            {
                                if (line.ToLower().StartsWith("content-id"))
                                {
                                    imageId = "cid:" + line.Substring(line.IndexOf("<") + 1).TrimEnd('>');

                                    if (!imageIds.Contains(imageId))
                                    {
                                        imageId = string.Empty;
                                        break;
                                    }
                                }

                                // TODO: find a stronger test to identify the line as a base64 encoding chunk
                                if (imageId != string.Empty && base64EncodingRgx.IsMatch(line))
                                {
                                    imageBase64 += line;
                                }
                            }

                            if (imageId != string.Empty && imageBase64 != string.Empty && base64EncodingRgx.IsMatch(imageBase64))
                            {
                                tempContent = tempContent.Replace(imageId, "data:" + contentType + ";base64," + imageBase64);
                            }
                        }
                    }
                }
            }
               
            return tempContent;
        }

        // The only thing we do for now is to remove "new winwdow" directives in links.
        private string SanitizeHTML(string htmlContent)
        {
            return htmlContent.Replace("target=\"_blank\"", "");
        }

        #endregion "Inbox"
    }

    public class ReceiveMessage
    {
        /***************************
         *      Attributes
         ***************************/
        public string FileName { get; set; }
        public string Id { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public string SenderEmail { get; set; } // TODO: There is confusion here with "fromEmail"
        public string SenderName { get; set; } // Same as above.
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public string Sender { get; set; }
        public bool HasAttachment { get; set; }
        public bool IsRead { get; set; }

        /***************************
        *      Constructor
        ***************************/
        public ReceiveMessage()
        {
            this.FileName = string.Empty;
            this.Id = string.Empty;
            this.Subject = string.Empty;
            this.Content = string.Empty;
            this.Type = string.Empty;
            this.Date = DateTime.Now;
            this.SenderEmail = string.Empty;
            this.SenderName = string.Empty;
            this.FromEmail = string.Empty;
            this.FromName = string.Empty;
            this.Sender = string.Empty;
            this.HasAttachment = false;
            this.IsRead = false;
        }
    }
}
