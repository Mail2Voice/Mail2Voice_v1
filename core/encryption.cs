﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    public class Encryption
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static Encryption instance = null;

        public static Encryption Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new Encryption();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static Encryption()
        {
            instance = new Encryption();
        }

        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/

        // TODO: look for a real encryption 
        public string EncryptString(string textToEncrypt, int passwordKey)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            StringBuilder inSb = new StringBuilder(textToEncrypt);
            StringBuilder outSb = new StringBuilder(textToEncrypt.Length);
            char c;

            for (int i = 0; i < textToEncrypt.Length; i++)
            {
                c = inSb[i];
                c = (char)(c ^ passwordKey);
                outSb.Append(c);
            }

            return outSb.ToString();
        }

        public string ByteArrayToString(byte[] arrInput)
        {
            int i;
            StringBuilder output = new StringBuilder(arrInput.Length);
            for (i = 0; i < arrInput.Length - 1; i++) 
            {
                output.Append(arrInput[i].ToString("X2"));
            }

            return output.ToString();
        }
    }
}
