﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    public class FileObject
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static FileObject instance = null;
        private ImageList imageList = null;

        public static FileObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new FileObject();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static FileObject()
        {
            instance = new FileObject();
        }

        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/
        public void Init(ImageList imageList)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            this.imageList = imageList;
        }

        // Move an email from a folder to another
        public void MoveEmail(string srcFolder, string destFolder, string file)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "srcFolder: " + srcFolder + ", destFolder: " + destFolder + ", file: " + file);

            try
            {
                // Delete file from dstFolder if it already exists in both folders
                if (File.Exists(srcFolder + file + Constants.EXTENSION_EML) &&
                    File.Exists(destFolder + file + Constants.EXTENSION_EML))
                {
                    File.Delete(destFolder + file + Constants.EXTENSION_EML);
                }

                // Delete attachments from dstFolder if they already exist in both folders
                if (Directory.Exists(srcFolder + file) &&
                    Directory.Exists(destFolder + file))
                {
                    Directory.Delete(destFolder + file, true);
                }

                // Copy email
                if (File.Exists(srcFolder + file + Constants.EXTENSION_EML))
                {
                    File.Move(srcFolder + file + Constants.EXTENSION_EML, destFolder + file + Constants.EXTENSION_EML);
                }

                // Copy attachments
                if (Directory.Exists(srcFolder + file))
                {
                    Directory.Move(srcFolder + file, destFolder + file);
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public bool EmailExists(string folder, string file)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "folder: " + folder + ", file: " + file);

            bool find = false;

            find = File.Exists(folder + file + Constants.EXTENSION_EML);
            if (!find)
            {
                find = Directory.Exists(folder + file);
            }

            return find;
        }

        public void DeleteEmailInFolders(string folder, string file)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "folder: " + folder + ", file: " + file);

            try
            {
                // Delete email
                if (File.Exists(folder + file + Constants.EXTENSION_EML))
                {
                    File.Delete(folder + file + Constants.EXTENSION_EML);
                }

                // delete attachment
                if (Directory.Exists(folder + file))
                {
                    Directory.Delete(folder + file);
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public List<string> ListFilesFromDirectory(string folder)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, " folder: " + folder);

            string[] files;
            files = Directory.GetFileSystemEntries(folder);
            return files.ToList();
        }

        public List<string> ListFilenamesFromFolder(string folder)
        {
            if (string.IsNullOrEmpty(folder))
            {
                return new List<string>();
            }

            List<string> filenamesList = new List<string>();
            string[] files;
            files = Directory.GetFiles(folder);

            for (int x = 0; x < files.Length; x++)
            {
                filenamesList.Add(Path.GetFileNameWithoutExtension(files[x]));
            }

            return filenamesList;
        }

        public string FileType(string filename)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "filename: " + filename);

            if (string.IsNullOrEmpty(filename))
            {
                return string.Empty;
            }

            string extension = Path.GetExtension(filename).Substring(1) + ".png";
            if (this.imageList.Images.ContainsKey(extension))
            {
                return extension;
            }
            else
            {
                return "unknown.png";
            }
        }

        // Clear remaining mp3 files from the last execution.
        public void DeleteMp3Files()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            List<string> files = null;
            files = Directory.GetFileSystemEntries(Path.GetDirectoryName(Application.ExecutablePath)).ToList();
            foreach (string s in files)
            {
                try
                {
                    if (Path.GetExtension(s).Equals(Constants.EXTENSION_MP3))
                    {
                        File.Delete(s);
                    }
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                }
            }
        }

        public void DeleteAttachment(string folderName)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "folderName: " + folderName);

            if (string.IsNullOrEmpty(folderName))
            {
                return;
            }
                
            List<string> dirs = null;
            dirs = Directory.GetDirectories(folderName).ToList();
            foreach (string d in dirs)
            {
                try
                {
                    if (!File.Exists(d + Constants.EXTENSION_EML))
                    {
                        Directory.Delete(d, true);
                    }
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                }
            }
        }

        public string GetFileCreationTime(string file)
        {
            string date = string.Empty;
            DateTime dt = DateTime.Now;

            if (File.Exists(file))
            {
                dt = File.GetCreationTime(file);
                date = dt.Day + "/" + dt.Month + "/" + dt.Year;
            }

            return date;
        }

        public string GetFileVersion(string file)
        {
            string version = "0.0.0";
            if (File.Exists(file))
            {
                FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(file);
                version = versionInfo.FileMajorPart + "." + versionInfo.FileMinorPart + "." + versionInfo.FileBuildPart;
            }

            return version;
        }

        // The following function come from Rick Strahl's blog: http://weblog.west-wind.com/posts/2007/Nov/28/Detecting-Text-Encoding-for-StreamReader

        /// <summary>
        /// Detects the byte order mark of a file and returns
        /// an appropriate encoding for the file.
        /// </summary>
        /// <param name="srcFile"></param>
        /// <returns></returns>
        public Encoding GetFileEncoding(string srcFile)
        {
            // *** Use Default of Encoding.Default (Ansi CodePage)
            Encoding enc = Encoding.UTF8;

            // *** Detect byte order mark if any - otherwise assume default
            byte[] buffer = new byte[5];
            FileStream file = new FileStream(srcFile, FileMode.Open);
            file.Read(buffer, 0, 5);
            file.Close();

            if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
            {
                enc = Encoding.UTF8;
            }
            else if (buffer[0] == 0xfe && buffer[1] == 0xff)
            {
                enc = Encoding.Unicode;
            }
            else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
            {
                enc = Encoding.UTF32;
            }
            else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
            {
                enc = Encoding.UTF7;
            }

            return enc;
        }

        public Image CropImage(Image img)
        {
            Size size = img.Size;
            Size newSize = size;
            Point origin = new Point(0, 0);

            int targetW = 200;
            int targetH = 200;

            if (size.Height < size.Width)
            {
                newSize.Width = size.Height;
                origin.X = (size.Width - newSize.Width) / 2;
            }
            else
            {
                newSize.Height = size.Width;
                origin.Y = (size.Height - newSize.Height) / 2;
            }

            Bitmap croppedImg = new Bitmap(targetW, targetH);
            croppedImg.SetResolution(72, 72);
            Graphics photo = Graphics.FromImage(croppedImg);

            photo.DrawImage(img, new Rectangle(0, 0, targetW, targetH), new Rectangle(origin, newSize), GraphicsUnit.Pixel);

            photo.Dispose();

            return croppedImg;
        }
    }
}
