﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml.Linq;

    public class LanguageObject
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private const string LANG_FILE_EXTENSION = ".lng";

        private static LanguageObject instance = null;
        private string langRootFolder = string.Empty;
        private string currentLang = string.Empty;
        private Dictionary<string, string> langDictionary = new Dictionary<string, string>();

        public CultureInfo CurrentCulture;

        public static LanguageObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new LanguageObject();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static LanguageObject()
        {
            instance = new LanguageObject();
        }

        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/
        public bool SetRootFolder(string folder)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "folder: " + folder);

            if (Directory.Exists(folder))
            {
                this.langRootFolder = folder;
                return true;
            }

            return false;
        }

        // Retrieve language files' name
        public List<string> GetLanguagesList()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            List<string> langsList = new List<string>();
            if (Directory.Exists(this.langRootFolder))
            {
                DirectoryInfo directory = new DirectoryInfo(this.langRootFolder);
                FileInfo[] langFiles = directory.GetFiles("*" + LANG_FILE_EXTENSION);

                foreach (FileInfo file in langFiles)
                {
                    langsList.Add(file.Name);
                }
            }

            return langsList;
        }

        public string GetCurrentLanguage()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            return this.currentLang;
        }

        public bool SetCurrentLang(string selectedLang)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "selectedLang: " + selectedLang);

            string langFile = this.langRootFolder + "\\" + selectedLang + LANG_FILE_EXTENSION;
            if (File.Exists(langFile))
            {
                this.currentLang = selectedLang;
                Dictionary<string, string> tmpDict = new Dictionary<string, string>();
                try
                {
                    // Setup the language dictionary with the given lang file
                    XDocument xmlDoc = XDocument.Load(langFile);

                    try
                    {
                        this.CurrentCulture = new CultureInfo(xmlDoc.Descendants(Constants.LanguageCultureName).First().Value);
                    }
                    catch (Exception ex)
                    {
                        LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                        this.CurrentCulture = new CultureInfo("en-US");
                    }

                    xmlDoc.Descendants(Constants.ApplicationParentTag).Descendants(Constants.LanguageParentTag).Elements().ToList().ForEach(
                        parent => parent.Elements().ToList().ForEach(
                            node => tmpDict.Add(
                                parent.Name.ToString().ToUpper() + "#" + node.Attribute(Constants.LanguageName).Value.ToString().ToUpper(), 
                                node.Attribute(Constants.LanguageValue).Value.ToString())));
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                }

                if (tmpDict.Count > 0)
                {
                    this.langDictionary = tmpDict;
                }

                return true;
            }

            return false;
        }

        public void Translate(Control parent, string mainParentName)
        {
            foreach (Control ctrl in parent.Controls)
            {
                try
                {
                    if (ctrl is DataGridView)
                    {
                        for (int x = 0; x < ((DataGridView)ctrl).ColumnCount; x++)
                        {
                            ((DataGridView)ctrl).Columns[x].HeaderText = this.Translate(mainParentName.ToString() + "#" + ((DataGridView)ctrl).Columns[x].Name.ToString(), ((DataGridView)ctrl).Columns[x].HeaderText);
                        }
                    }
                    else if (!(ctrl is WebBrowser))
                    {
                        ctrl.Text = this.Translate(mainParentName.ToString() + "#" + ctrl.Name.ToString(), ctrl.Text);
                        this.Translate(ctrl, mainParentName);
                    }
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                }
            }
        }

        public string Translate(string key, string defaultValue)
        {
            if (this.langDictionary.ContainsKey(key.ToUpper()))
            {
                return this.langDictionary[key.ToUpper()];
            }

            return defaultValue;
        }
    }
}
