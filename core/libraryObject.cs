﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Windows.Forms;
    using ImapX;
    using Mail2VoiceCore;

    public class LibraryObject
    {
        public enum Level
        {
            classic,
            debug
        }

        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static LibraryObject instance = null;

        private List<char> listSeparator = new List<char>() { ' ', ',', ';', '?', '!', '\n', '\r' };

        public static LibraryObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new LibraryObject();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static LibraryObject()
        {
            instance = new LibraryObject();
        }

        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/
        public string GetMonthString(int index)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "index: " + index);

            string[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            if (index < 0 || index > months.Length)
            {
                return months.ElementAt(0);
            }

            return months.ElementAt(index - 1);
        }

        public int TestImapServer(string host, int port, bool ssl, string login, string password)
        {
            LogObject.Singleton.Write(
                new System.Diagnostics.StackFrame().GetMethod().Name,
                "host: " + host + ", port: " + port + ", ssl: " + ssl + ", login: " + login + ", password: " + password);

            try
            {
                ImapClient client = null;
                client = new ImapClient(host, port, ssl);
                bool isConnected = client.Connection();
                if (isConnected)
                {
                    bool isLoginOk = client.LogIn(login, password);
                    if (isLoginOk)
                    {
                        client.LogOut();
                        client.Disconnect();
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -2;
                }

                return 0;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                return -3;
            }
        }

        public bool TestSmtpServer(string smtpServerAddress, int port)
        {
            LogObject.Singleton.Write(
                new System.Diagnostics.StackFrame().GetMethod().Name,
                "smtpServerAddress: " + smtpServerAddress + ", port: " + port);

            try
            {
                if (string.IsNullOrEmpty(smtpServerAddress))
                {
                    return false;
                }

                IPHostEntry hostEntry = Dns.GetHostEntry(smtpServerAddress);
                IPEndPoint endPoint = new IPEndPoint(hostEntry.AddressList[0], port);
                using (Socket tcpSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
                {
                    IAsyncResult result = tcpSocket.BeginConnect(endPoint, null, null);

                    bool success = result.AsyncWaitHandle.WaitOne(5000, true);

                    if (!tcpSocket.Connected || !CheckConnection(tcpSocket, 220))
                    {
                        return false;
                    }

                    SendData(tcpSocket, string.Format("HELO {0}\r\n", Dns.GetHostName()));
                    if (!CheckConnection(tcpSocket, 250))
                    {
                        return false;
                    }

                    tcpSocket.Close();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);

                return false;
            }
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                Regex emailregex = new Regex(@".+@.+", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                return emailregex.IsMatch(email);
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }

            return false;
        }

        /***************************
         *      Private methods
         ***************************/
        private static bool CheckConnection(Socket socket, int expectedCode)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            int timeout = 5000;

            while (socket.Available == 0 && timeout > 0)
            {
                Thread.Sleep(100);
                timeout -= 100;
            }

            if (timeout == 0)
            {
                return false;
            }

            byte[] responseArray = new byte[1024];
            socket.Receive(responseArray, 0, socket.Available, SocketFlags.None);
            string responseData = Encoding.ASCII.GetString(responseArray);
            int responseCode = Convert.ToInt32(responseData.Substring(0, 3));
            if (responseCode == expectedCode)
            {
                return true;
            }

            return false;
        }

        private static void SendData(Socket socket, string data)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            byte[] dataArray = Encoding.ASCII.GetBytes(data);
            socket.Send(dataArray, 0, dataArray.Length, SocketFlags.None);
        }
    }
}
