﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.IO;
    using System.Text;

    public class LogObject
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static LogObject instance = null;
        private string defaultDirectory = string.Empty;

        public static LogObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new LogObject();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static LogObject()
        {
            instance = new LogObject();
            File.Delete(Constants.CONFIG_LOG_FILE);
        }
            
        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/
        public void Init(string directory)
        {
            this.defaultDirectory = directory;
        }

        public void Write(string message)
        {
            this.Write(message, string.Empty);
        }

        public void Write(string message1, string message2)
        {
            try
            {
                string activate = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_LOG_ACTIVATE);
                bool active = false;
                if (bool.TryParse(activate, out active))
                {
                    if (active)
                    {
                        StreamWriter file = new StreamWriter(Constants.CONFIG_LOG_FILE, true, Encoding.UTF8);
                        file.WriteLine(DateTime.Now.ToString() + " - " + message1);

                        if (!string.IsNullOrEmpty(message2))
                        {
                          file.WriteLine("\t" + message2);
                        }

                        file.Close();
                        this.ControlSize();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(new System.Diagnostics.StackFrame().GetMethod().Name + ": " + ex.ToString());
            }
        }

        public void Write(string caller, Exception exception)
        {
            try
            {
                if (Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_LOG_ACTIVATE)))
                {
                    StreamWriter file = new StreamWriter(Constants.CONFIG_LOG_FILE, true, Encoding.UTF8);
                    file.WriteLine(DateTime.Now.ToString() + " - " + caller);
                    file.WriteLine("\t" + exception.Message);
                    file.Close();
                    this.ControlSize();
                }
            }
            catch (Exception ex)
            {
                Console.Write(new System.Diagnostics.StackFrame().GetMethod().Name + ": " + ex.ToString());
            }
        }

        /***************************
         *      Private methods
         ***************************/
        private void ControlSize()
        {
            try
            {
                if (File.Exists(Constants.CONFIG_LOG_FILE))
                {
                    FileInfo f = new FileInfo(Constants.CONFIG_LOG_FILE);
                    DateTime ficTime = DateTime.Now;

                    // 25Mo
                    if (f.Length > 26214400) 
                    {
                        string destFile = Path.GetFileNameWithoutExtension(Constants.CONFIG_LOG_FILE) +
                                          ficTime.Year + "_" + ficTime.Month + "_" + ficTime.Day + "__" +
                                          ficTime.Hour + "_" + ficTime.Minute + "_" + ficTime.Second + ".txt";

                        File.Copy(Constants.CONFIG_LOG_FILE, destFile);

                        File.Delete(Constants.CONFIG_LOG_FILE);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(new System.Diagnostics.StackFrame().GetMethod().Name + ": " + ex.ToString());
            }
        }
    }
}
