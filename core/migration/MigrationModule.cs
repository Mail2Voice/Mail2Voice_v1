﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Xml;
    using System.Xml.XPath;
    using System.Xml.Xsl;
   
    public class MigrationModule
    {
        public MigrationModule()
        {
        }

        public bool ImportOldAccount(string folder)
        {
            bool importSuccess = false;

            try
            {
                /*
                 * Convert XML files
                 */
                string expectedOldConfigFile = folder + "config.xml";
                string expectedOldContactsFile = folder + "contacts.xml";

                string outputNewConfigFile = AppDomain.CurrentDomain.BaseDirectory + "config.xml";
                string outputNewContactFile = AppDomain.CurrentDomain.BaseDirectory + "contacts\\contacts.xml";

                string xsltConfigFile = AppDomain.CurrentDomain.BaseDirectory + "migration\\config.xsl";
                string xsltContactsFile = AppDomain.CurrentDomain.BaseDirectory + "migration\\contacts.xsl";

                if (File.Exists(expectedOldConfigFile))
                {
                    if (!this.ConvertXmlFile(expectedOldConfigFile, xsltConfigFile, outputNewConfigFile))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

                if (File.Exists(expectedOldContactsFile))
                {
                    if (!File.Exists(outputNewContactFile))
                    {
                        if (!this.ConvertXmlFile(expectedOldContactsFile, xsltContactsFile, outputNewContactFile))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (!this.ConvertXmlFile(expectedOldContactsFile, xsltContactsFile, outputNewContactFile + ".tmp"))
                        {
                            return false;
                        }
                        else
                        {
                            // the other documents will be merged into this one 
                            // under the Container element 
                            XmlDocument doc = new XmlDocument();
                            XmlDeclaration xmldecl = doc.CreateXmlDeclaration("1.0", "utf-8", "no");
                            XmlNode cont = doc.CreateElement("Contacts");
                            doc.AppendChild(cont);

                            // first document to merge 
                            XmlDocument stuff = new XmlDocument();
                            stuff.Load(outputNewContactFile);
                            XmlNode imported = doc.ImportNode(stuff.DocumentElement, true);
                            foreach (XmlNode contactNode in imported.ChildNodes)
                            {
                                doc.DocumentElement.AppendChild(contactNode);
                            }

                            // second document to merge 
                            stuff.Load(outputNewContactFile + ".tmp");
                            imported = doc.ImportNode(stuff.DocumentElement, true);
                            foreach (XmlNode contactNode in imported.ChildNodes)
                            {
                                doc.DocumentElement.AppendChild(contactNode);
                            }

                            doc.InsertBefore(xmldecl, doc.DocumentElement);
                            doc.Save(outputNewContactFile);
                            File.Delete(outputNewContactFile + ".tmp");
                        }
                    }
                }

                /*
                 * Copy contacts photos
                 */
                string expectedOldContactsFolder = folder + "contacts\\";
                string newContactsFolder = AppDomain.CurrentDomain.BaseDirectory + "contacts\\";

                Directory.CreateDirectory(newContactsFolder);

                if (Directory.Exists(expectedOldContactsFolder))
                {
                    string[] files = Directory.GetFiles(expectedOldContactsFolder);

                    foreach (string file in files)
                    {
                        string newFile = newContactsFolder + file.Remove(0, file.LastIndexOf("\\"));
                        Image img = Image.FromFile(file);
                        Image newImage = FileObject.Singleton.CropImage(img);
                        newImage.Save(newFile);
                    }
                }

                /*
                 * Copy emails
                 */
                string expectedOldEmailsFolder = folder + "emails\\";
                string newEmailsFolder = AppDomain.CurrentDomain.BaseDirectory + "emails\\";

                Directory.CreateDirectory(newEmailsFolder);

                if (Directory.Exists(expectedOldEmailsFolder))
                {
                    // Now Create all of the directories
                    foreach (string dirPath in Directory.GetDirectories(expectedOldEmailsFolder, "*",  SearchOption.AllDirectories))
                    {
                        string path = dirPath.Replace("SENDED", "SENT");
                        Directory.CreateDirectory(path.Replace(expectedOldEmailsFolder, newEmailsFolder));
                    }

                    // Copy all the files & Replaces any files with the same name
                    foreach (string newPath in Directory.GetFiles(expectedOldEmailsFolder, "*.*", SearchOption.AllDirectories))
                    {
                        File.Copy(newPath, newPath.Replace(expectedOldEmailsFolder, newEmailsFolder), true);
                    }
                }

                importSuccess = true;
            }
            catch (Exception e)
            {
                LogObject.Singleton.Write("Failed to import settings: " + e);
                importSuccess = false;
            }

            return importSuccess;
        }

        public bool ConvertXmlFile(string inputFile, string xsltFile, string outputFile)
        {
            try
            {
                // Open books.xml as an XPathDocument.
                XPathDocument doc = new XPathDocument(inputFile);

                // Create a writer for writing the transformed file.
                XmlWriter writer = XmlWriter.Create(outputFile);

                // Create and load the transform with script execution enabled.
                XslCompiledTransform transform = new XslCompiledTransform();
                XsltSettings settings = new XsltSettings();
                settings.EnableScript = true;
                transform.Load(xsltFile, settings, null);

                // Execute the transformation.
                transform.Transform(doc, writer);
                writer.Close();
            }
            catch (Exception e)
            {
                LogObject.Singleton.Write("Failed to transform XML file: " + e);
                return false;
            }

            return true;
        }
    }
}