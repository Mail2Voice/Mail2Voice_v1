<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/>

  <xsl:template match="/Settings">
    <Settings>
	  <!-- Convert old settings to new ones -->
      <xsl:apply-templates select="Setting"/>
	  
	  <!-- New Settings -->
	  <Setting name="main_buttons_hover_speech_preview" value="True" />
	  <Setting name="keyboard" value="english.kb" />
	  <Setting name="update" value="True" />
      <Setting name="update_repeat" value="1" />
      <Setting name="update_lastcheck" value="01/01/0001 00:00:00" />
      <Setting name="activate_keyboard" value="False" />
      <Setting name="send_usage_stats" value="False" />
    </Settings>
  </xsl:template>

  <xsl:template match="Setting">
    <xsl:apply-templates select="Setting[@name='email_adresse']"/>
    <xsl:apply-templates select="Setting[@name='email_password']"/>
    <xsl:apply-templates select="Setting[@name='imap_adresse']"/>
	<xsl:apply-templates select="Setting[@name='imap_port']"/>
	<xsl:apply-templates select="Setting[@name='imap_ssl']"/>
	<xsl:apply-templates select="Setting[@name='smtp_adresse']"/>
	<xsl:apply-templates select="Setting[@name='smtp_port']"/>
	<xsl:apply-templates select="Setting[@name='smtp_name']"/>
	<xsl:apply-templates select="Setting[@name='smtp_ssl']"/>
	<xsl:apply-templates select="Setting[@name='log_activer']"/>
	<xsl:apply-templates select="Setting[@name='envoi_sujet']"/>
	<xsl:apply-templates select="Setting[@name='envoi_corps']"/>
	<xsl:apply-templates select="Setting[@name='traduction_langue']"/>
	<xsl:apply-templates select="Setting[@name='langue']"/>
	<xsl:apply-templates select="Setting[@name='email_activer_corbeille']"/>
	<xsl:apply-templates select="Setting[@name='email_delais']"/>
  </xsl:template>

  <xsl:template match="Setting[@name='email_adresse']">
    <Setting name="email_login" value="{@value}" />
  </xsl:template>

  <xsl:template match="Setting[@name='email_password']">
    <Setting name="email_password" value="{@value}" />
  </xsl:template>

  <xsl:template match="Setting[@name='imap_adresse']">
    <Setting name="imap_address" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='imap_port']">
    <Setting name="imap_port" value="{@value}" />
  </xsl:template>

  <xsl:template match="Setting[@name='imap_ssl']">
    <Setting name="imap_ssl" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='smtp_adresse']">
    <Setting name="smtp_address" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='smtp_port']">
    <Setting name="smtp_port" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='smtp_name']">
    <Setting name="smtp_name" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='smtp_ssl']">
    <Setting name="smtp_ssl" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='log_activer']">
    <Setting name="log_activate" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='envoi_sujet']">
    <Setting name="send_subject" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='envoi_corps']">
    <Setting name="send_content" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='traduction_langue']">
    <Setting name="translation_lang" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='langue']">
    <Setting name="lang" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='email_activer_corbeille']">
    <Setting name="email_activate_trash" value="{@value}" />
  </xsl:template>
  
  <xsl:template match="Setting[@name='email_delais']">
    <Setting name="email_delays" value="{@value}" />
  </xsl:template>
  
</xsl:stylesheet>