<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/>

  <xsl:template match="/Contacts">
    <Contacts>
      <xsl:apply-templates select="Contact"/>
    </Contacts>
  </xsl:template>

  <xsl:template match="Contact">
    <Contact>
    <Name>
      <xsl:value-of select="Nom" />
      <xsl:text> </xsl:text>
      <xsl:value-of select="Prenom" />
    </Name>
    <Email>
       <xsl:value-of select="Email" />
    </Email>
	</Contact>
  </xsl:template>

</xsl:stylesheet>