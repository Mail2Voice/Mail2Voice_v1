﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Runtime.InteropServices;
    using System.Text;

    public class NetObject
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static NetObject instance = null;

        public static NetObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new NetObject();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static NetObject()
        {
            instance = new NetObject();
        }

        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/

        // API declaration
        [DllImport("wininet.dll")]
        public static extern bool InternetGetConnectedState(out int description, int reservedValue);

        public bool IsConnected()
        {
            int desc;
            return InternetGetConnectedState(out desc, 0);
        }
    }
}
