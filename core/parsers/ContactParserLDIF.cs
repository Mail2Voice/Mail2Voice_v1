﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    public class ContactParserLDIF : IContactsParser
    {
        /***************************
         *      Constructor
         ***************************/
        public ContactParserLDIF()
        {
        }

        /***************************
         *      Public methods
         ***************************/
        public List<Contact> Parse(string filepath)
        {
            List<Contact> importedContacts = new List<Contact>();
            Encoding enc = FileObject.Singleton.GetFileEncoding(filepath);
            StreamReader sr = new StreamReader(filepath, enc);
            string tmpContactFullName = string.Empty;
            string tmpContactFirstName = string.Empty;
            string tmpContactLastName = string.Empty;
            string tmpContactMail = string.Empty;

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string tmpLine = line.ToLower();

                if (line == string.Empty)
                {
                    if (tmpContactMail != string.Empty && LibraryObject.Singleton.IsValidEmail(tmpContactMail))
                    {
                        if (tmpContactFullName == string.Empty)
                        {
                            if (tmpContactFirstName != string.Empty || tmpContactLastName != string.Empty)
                            {
                                tmpContactFullName = tmpContactFirstName + " " + tmpContactLastName;
                            }
                            else
                            {
                                tmpContactFullName = tmpContactMail;
                            }
                        }

                        Contact contact = new Contact();

                        contact.Email = tmpContactMail;
                        contact.Name = tmpContactFullName;

                        importedContacts.Add(contact);
                    }

                    tmpContactFullName = tmpContactFirstName = tmpContactLastName = tmpContactMail = string.Empty;
                }
                else if (tmpLine.StartsWith("cn::"))
                {
                    tmpContactFullName = this.DecodeBase64String(line.Split(':').Last().Trim());
                }
                else if (tmpLine.StartsWith("cn:"))
                {
                    tmpContactFullName = line.Split(':').Last().Trim();
                }
                else if (tmpLine.StartsWith("givenname::"))
                {
                    tmpContactFirstName = this.DecodeBase64String(line.Split(':').Last().Trim());
                }
                else if (tmpLine.StartsWith("givenname:"))
                {
                    tmpContactFirstName = line.Split(':').Last().Trim();
                }
                else if (tmpLine.StartsWith("sn::"))
                {
                    tmpContactLastName = this.DecodeBase64String(line.Split(':').Last().Trim());
                }
                else if (tmpLine.StartsWith("sn:"))
                {
                    tmpContactLastName = line.Split(':').Last().Trim();
                }
                else if (tmpLine.StartsWith("mail:"))
                {
                    tmpContactMail = line.Split(':').Last().Trim();
                }
            }

            return importedContacts;
        }

        /***************************
         *      Private methods
         ***************************/
        private string DecodeBase64String(string base64Str)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(base64Str));
        }
    }
}