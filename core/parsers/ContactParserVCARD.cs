﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    public class ContactParserVCARD : IContactsParser
    {
        /***************************
         *      Constructor
         ***************************/
        public ContactParserVCARD()
        {
        }

        /***************************
         *      Public methods
         ***************************/
        public List<Contact> Parse(string filepath)
        {
            List<Contact> importedContacts = new List<Contact>();
            Encoding enc = FileObject.Singleton.GetFileEncoding(filepath);
            StreamReader sr = new StreamReader(filepath, enc);
            string tmpContactFullName = string.Empty;
            string tmpContactFirstName = string.Empty;
            string tmpContactLastName = string.Empty;
            string tmpContactMail = string.Empty;
            bool sectionStarted = false;

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();

                if (sectionStarted)
                {
                    if (line == "END:VCARD")
                    {
                        sectionStarted = false;
                        if (tmpContactMail != string.Empty && LibraryObject.Singleton.IsValidEmail(tmpContactMail))
                        {
                            if (tmpContactFullName == string.Empty)
                            {
                                if (tmpContactFirstName != string.Empty || tmpContactLastName != string.Empty)
                                {
                                    tmpContactFullName = tmpContactFirstName + " " + tmpContactLastName;
                                }
                                else
                                {
                                    tmpContactFullName = tmpContactMail;
                                }
                            }

                            Contact contact = new Contact();

                            contact.Email = tmpContactMail;
                            contact.Name = tmpContactFullName;

                            importedContacts.Add(contact);
                        }

                        tmpContactFullName = tmpContactFirstName = tmpContactLastName = tmpContactMail = string.Empty;
                    }
                    else if (line.StartsWith("FN:"))
                    {
                        tmpContactFullName = line.Split(':').Last().Trim();
                    }
                    else if (line.StartsWith("N:"))
                    {
                        string[] names = line.Split(':').Last().Split(';');

                        tmpContactFirstName = names[1].Trim();
                        tmpContactLastName = names[0].Trim();
                    }
                    else if (line.StartsWith("EMAIL;"))
                    {
                        tmpContactMail = line.Split(':').Last().Trim();
                    }
                }
                else if (line == "BEGIN:VCARD")
                {
                    sectionStarted = true;
                }
            }

            return importedContacts;
        }
    }
}