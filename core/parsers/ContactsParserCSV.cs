﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    public class ContactsParserCSV : IContactsParser
    {
        /***************************
         *      Constructor
         ***************************/
        public ContactsParserCSV()
        {
        }

        /***************************
         *      Public methods
         ***************************/
        public List<Contact> Parse(string filepath)
        {
            List<Contact> importedContacts = new List<Contact>();
            Encoding enc = FileObject.Singleton.GetFileEncoding(filepath);
            StreamReader sr = new StreamReader(filepath, enc);
            List<string> list = new List<string>();

            while (!sr.EndOfStream)
            {
                list.Add(sr.ReadLine());
            }

            if (list.Count == 0)
            {
                return importedContacts;
            }

            list.RemoveAt(0); // Removes the headers

            foreach (string s in list)
            {
                string[] contactFields = { };
                string emailAddress = string.Empty;

                if (s.Contains("\""))
                {
                    List<int> indexOfComasToRemove = new List<int>();
                    List<string> splits = new List<string>();
                    int lastIndex = 0;

                    for (int i = 0, n = s.Length; i < n; ++i)
                    {
                        if (s[i] == ',')
                        {
                            splits.Add(s.Substring(lastIndex, i - lastIndex));
                            lastIndex = i + 1;
                        }
                        else if (s[i] == '"')
                        {
                            int textStop = s.IndexOf('"', i + 1);
                            lastIndex = i + 1;

                            if (textStop != -1)
                            {
                                i = textStop;
                                splits.Add(s.Substring(lastIndex, i - lastIndex));
                                lastIndex = i + 1;
                            }
                        }
                    }

                    contactFields = splits.ToArray();
                }
                else
                {
                    contactFields = s.Split(',');
                }

                for (int i = 0, n = contactFields.Length; i < n; ++i)
                {
                    if (contactFields[i].Length > 2 && contactFields[i].StartsWith("\"") && contactFields[i].EndsWith("\""))
                    {
                        contactFields[i] = contactFields[i].Substring(1, contactFields[i].Length - 2);
                    }

                    if (contactFields[i] == "\"\"")
                    {
                        contactFields[i] = string.Empty;
                    }

                    if (i >= 4 && emailAddress == string.Empty && LibraryObject.Singleton.IsValidEmail(contactFields[i]))
                    {
                        emailAddress = contactFields[i];
                    }
                }

                if (emailAddress != string.Empty)
                {
                    string fullname = this.ExtractFullName(contactFields);

                    if (fullname == string.Empty)
                    {
                        fullname = emailAddress;
                    }

                    Contact contact = new Contact();
                    contact.Name = fullname;
                    contact.Email = emailAddress;

                    importedContacts.Add(contact);
                }
            }

            return importedContacts;
        }

        /***************************
         *      Private methods
         ***************************/
        private string ExtractFullName(string[] contactFields)
        {
            string fullname = string.Empty;
            List<string> fullnameFallback = new List<string>();
            int numberOfFieldsToScan = 4;
            bool gotFullname = false;

            if (contactFields.Length < numberOfFieldsToScan)
            {
                numberOfFieldsToScan = contactFields.Length;
            }

            for (int i = 0; i < numberOfFieldsToScan && !gotFullname; ++i)
            {
                if (contactFields[i] == string.Empty)
                {
                    continue;
                }

                for (int j = 0; j < numberOfFieldsToScan && !gotFullname; ++j)
                {
                    if (j == i || contactFields[j] == string.Empty)
                    {
                        continue;
                    }

                    if (contactFields[i] != contactFields[j] && contactFields[i].Contains(contactFields[j]))
                    {
                        fullname = contactFields[i];
                        gotFullname = true;
                    }
                }

                fullnameFallback.Add(contactFields[i]);
            }

            if (!gotFullname && fullnameFallback.Count > 0)
            {
                fullname = string.Join(" ", fullnameFallback.Distinct().ToArray());
            }

            return fullname;
        }
    }
}