﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using ImapX;
    using Mail2VoiceCore;

    public class ReceiveObject
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static ReceiveObject instance = null;
        private volatile bool stopThread;
        private ImapClient client = null;
        private object lockObject = new object();
        private object lockMailSync = new object();

        private static Dictionary<string, string> localOperations = new Dictionary<string, string>();
        private static Dictionary<string, string> receivedMessages = new Dictionary<string, string>();
        private static Dictionary<string, int> presumedDeletedMessages = new Dictionary<string, int>();
        private static Dictionary<string, int> presumedMovedMessages = new Dictionary<string, int>();
        private Dictionary<string, string> userFolders = new Dictionary<string, string>();

        public static ReceiveObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new ReceiveObject();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static ReceiveObject()
        {
            instance = new ReceiveObject();
        }

        #endregion "Singleton"

        public static event EventHandler EmailsListChanged;

        /***************************
         *      Public methods
         ***************************/
        public void RequestStop()
        {
            this.stopThread = true;
            if (this.client != null)
            {
                this.client.Disconnect();
                this.client = null;
            }
        }

        public void EmailsPolling()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            string[] folders = { Constants.EMAIL_INBOX, Constants.EMAIL_TRASH };
            string[] states = { Constants.EMAIL_UNREAD, Constants.EMAIL_READ };

            bool emailsListToUpdate = false;

            while (!this.stopThread)
            {
                lock (this.lockMailSync)
                {
                    localOperations.Clear();
                }

                foreach (string folder in folders)
                {
                    foreach (string state in states)
                    {
                        if (!this.stopThread)
                        {
                            if (this.FetchAvailableEmails(folder, state))
                            {
                                emailsListToUpdate = true;
                            }
                        }
                    }
                }

                bool mailSyncOk = true;

                if (mailSyncOk)
                {
                    if (emailsListToUpdate && ReceiveObject.EmailsListChanged != null)
                    {
                        ReceiveObject.EmailsListChanged(null, EventArgs.Empty);
                        emailsListToUpdate = false;
                    }

                    Thread.Sleep(5000);
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        public bool Connect()
        {
            bool isLoginOk = false;

            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            try
            {
                this.client = new ImapClient(
                    ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_ADDRESS),
                    Convert.ToInt32(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_PORT)),
                    Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_SSL)));

                if (this.client != null)
                {
                    this.client.Connection();
                
                    if (this.client.IsConnected)
                    {
                        isLoginOk = this.client.LogIn(
                            ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN),
                            Encryption.Singleton.EncryptString(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_PASSWORD), Constants.PASSWORD_KEY));
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }

            return isLoginOk;
        }

        public void Disconnect()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            try
            {
                if (this.client.IsLogined)
                {
                    this.client.LogOut();
                }

                if (this.client.IsConnected)
                {
                    this.client.Disconnect();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public string GetDistantFolder(string inFolder)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "inFolder: " + inFolder);

            if (this.client == null || !this.client.IsConnected || !this.client.IsLogined)
            {
                return string.Empty;
            }

            string folder = inFolder;

            try
            {
                FolderCollection fc = this.client.Folders;
                foreach (Folder f in fc)
                {
                    if (f.Name.ToUpper().Equals(inFolder))
                    {
                        folder = f.Name;
                        break;
                    }
                    else if (f.Name.ToUpper().Equals(Constants.EMAIL_TRASH))
                    {
                        folder = f.Name;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }

            return folder;
        }

        public string GetUserFolder(string inFolder, string type)
        {
            if (this.userFolders.Count == 0)
            {
                this.userFolders.Add(Constants.EMAIL_INBOX + Constants.EMAIL_READ, Constants.EMAIL_USER_FOLDER_INBOX_READ);
                this.userFolders.Add(Constants.EMAIL_INBOX + Constants.EMAIL_UNREAD, Constants.EMAIL_USER_FOLDER_INBOX_UNREAD);
                this.userFolders.Add(Constants.EMAIL_TRASH + Constants.EMAIL_READ, Constants.EMAIL_USER_FOLDER_TRASH_READ);
                this.userFolders.Add(Constants.EMAIL_TRASH + Constants.EMAIL_UNREAD, Constants.EMAIL_USER_FOLDER_TRASH_UNREAD);
            }

            return this.userFolders[inFolder + type];
        }

        public bool FetchAvailableEmails(string inFolder, string type)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "inFolder: " + inFolder + ", type: " + type);

            bool emailsListChanged = false;

            lock (this.lockObject)
            {
                try
                {
                    bool isLoginOk = false;

                    isLoginOk = this.client != null && this.client.IsConnected && this.client.IsLogined;

                    if (!isLoginOk)
                    {
                        isLoginOk = this.Connect();
                    }

                    if (isLoginOk)
                    {
                        string userFolder = this.GetUserFolder(inFolder, type);
                        string distantFolder = this.GetDistantFolder(inFolder);

                        // List all emails contained in a folder (READ & UNREAD)
                        List<string> emailsList = new List<string>();
                        emailsList.AddRange(FileObject.Singleton.ListFilenamesFromFolder(userFolder));

                        if (this.client == null)
                        {
                            return false;
                        }

                        MessageCollection mcol = null;

                        // Retrieve messages on server
                        try
                        {
                            this.client._client.SelectFolder(distantFolder);
                            mcol = this.client.Folders[distantFolder].Search(type, false);
                        }
                        catch (Exception ex)
                        {
                            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                            this.client.Disconnect();
                            return false;
                        }

                        // Consrtruct a list containing emails id from server and check for new mails
                        List<string> list = new List<string>();

                        bool savedEmail = false;
                        bool movedEmail = false;
                        string local = distantFolder + "/" + type;

                        foreach (ImapX.Message message in mcol)
                        {
                            try
                            {
                                message.Process(false);

                                // If bad message object
                                if (message.ReplyTo == null && message.Subject == null && message.From.Count == 0 && message.To.Count == 0)
                                {
                                    continue;
                                }

                                // Ensure the message have an id before adding it
                                if (message.MessageId == null)
                                {
                                    // As we don't have a proper unique ID, we create one
                                    message.MessageId = this.GenerateFakeMessageId(message);
                                }

                                // If the message was presumed to be deleted on server, we remove it from the watch list as it still exists
                                if (presumedDeletedMessages.ContainsKey(userFolder + message.MessageId))
                                {
                                    presumedDeletedMessages.Remove(userFolder + message.MessageId);
                                }

                                list.Add(message.MessageId);

                                lock (this.lockMailSync)
                                {
                                    string messageIdCleaned = message.MessageId.Replace("<", string.Empty).Replace(">", string.Empty);

                                    if (receivedMessages.ContainsKey(messageIdCleaned))
                                    {
                                        if (receivedMessages[messageIdCleaned] == userFolder
                                        && presumedMovedMessages.ContainsKey(messageIdCleaned))
                                        {
                                            presumedMovedMessages.Remove(messageIdCleaned);
                                        }
                                        else
                                        {
                                            if (presumedMovedMessages.ContainsKey(messageIdCleaned))
                                            {
                                                presumedMovedMessages[messageIdCleaned]--;

                                                if (presumedMovedMessages[messageIdCleaned] == 0)
                                                {
                                                    FileObject.Singleton.MoveEmail(receivedMessages[messageIdCleaned], userFolder, messageIdCleaned);
                                                    receivedMessages[messageIdCleaned] = userFolder;
                                                    presumedMovedMessages.Remove(messageIdCleaned);
                                                    movedEmail = true;
                                                }
                                            }
                                            else
                                            {
                                                presumedMovedMessages.Add(messageIdCleaned, 2);
                                            }
                                        }
                                    }
                                    else if (!localOperations.ContainsKey(messageIdCleaned)) 
                                    {
                                        receivedMessages.Add(messageIdCleaned, userFolder);

                                        // Save message on disk if not present
                                        if (!FileObject.Singleton.EmailExists(userFolder, messageIdCleaned))
                                        {
                                            message.SaveAsEmlToFile(userFolder, messageIdCleaned);
                                            message.ProcessBody();
                                            savedEmail = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                            }
                        }

                        bool deletedEmail = false;

                        // Delete local stored emails that are no longer on server
                        deletedEmail = this.DeleteEmailsNotOnServer(list, userFolder, local);

                        emailsListChanged = savedEmail || deletedEmail || movedEmail;
                    }
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                }
            }

            return emailsListChanged;
        }

        public bool DeleteEmailsNotOnServer(List<string> emailsOnServer, string folder, string local)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "emailsOnServer.Count.ToString(): " + emailsOnServer.Count.ToString() + ", folder: " + folder);

            bool deletedSomeFiles = false;
            List<string> filesList = new List<string>();
            filesList.AddRange(FileObject.Singleton.ListFilenamesFromFolder(folder));

            foreach (string file in filesList)
            {
                string emailId = file;

                bool emailsExistsOnServer = false;

                foreach (string id in emailsOnServer)
                {
                    if (id.Contains(file))
                    {
                        emailsExistsOnServer = true;
                        break;
                    }
                }

                if (!emailsExistsOnServer && !file.Contains("OECustomProperty") && file != "$DATA")
                {
                    lock (this.lockMailSync)
                    {
                        if (!localOperations.ContainsKey(file))
                        {
                            if (presumedDeletedMessages.ContainsKey(folder + emailId))
                            {
                                presumedDeletedMessages[folder + emailId]--;
                                if (presumedDeletedMessages[folder + emailId] == 0)
                                {
                                    presumedDeletedMessages.Remove(folder + emailId);
                                    receivedMessages.Remove(emailId);
                                    FileObject.Singleton.DeleteEmailInFolders(folder, file);
                                    deletedSomeFiles = true;
                                }
                            }
                            else
                            {
                                presumedDeletedMessages.Add(folder + emailId, 3);
                            }
                        }
                    }
                }
            }

            return deletedSomeFiles;
        }

        public void SetEmailSeen(string inFolder, string messageId)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "inFolder: " + inFolder + ", messageId: " + messageId);

            Thread thread = new Thread(new ParameterizedThreadStart(this._tSetEmailSeen));
            thread.IsBackground = true;
            thread.Start(new List<string>() { inFolder, messageId });
        }

        public void _tSetEmailSeen(object parameters)
        {
            string folder = (parameters as List<string>)[0];
            string messageToSetSeenId = (parameters as List<string>)[1];

            this.AddLocalOperation(messageToSetSeenId, folder, Constants.EMAIL_READ);

            lock (this.lockObject)
            {
                try
                {
                    if (!NetObject.Singleton.IsConnected())
                    {
                        return;
                    }

                    bool isLoginOk = false;
                    isLoginOk = this.client != null && this.client.IsConnected && this.client.IsLogined;

                    if (!isLoginOk)
                    {
                        isLoginOk = this.Connect();
                    }

                    if (isLoginOk)
                    {
                        folder = this.GetDistantFolder(folder);

                        Message m = new Message();
                        MessageCollection mcol = this.client.Folders[folder].Messages;
                        List<string> list = new List<string>();

                        foreach (ImapX.Message message in mcol)
                        {
                            try
                            {
                                message.Process(false);

                                // Ensure the message have an id before adding it
                                if (message.MessageId == null)
                                {
                                    // As we don't have a proper unique ID, we create one
                                    message.MessageId = this.GenerateFakeMessageId(message);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                            }

                            try
                            {
                                if (message.MessageId.Equals("<" + messageToSetSeenId + ">"))
                                {
                                    message.SetFlag(ImapFlags.SEEN);
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                            }
                        }
                    }

                    this.Disconnect();
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                }
            }
        }

        public void MoveEmail(string srcFolder, string dstFolder, string type, string messageId)
        {
            LogObject.Singleton.Write(
                new System.Diagnostics.StackFrame().GetMethod().Name,
                "srcFolder: " + srcFolder + ", dstFolder: " + dstFolder + "type: " + type + ", messageId: " + messageId);

            Thread thread = new Thread(new ParameterizedThreadStart(this._tMoveEmail));
            thread.IsBackground = true;
            thread.Start(new List<string>() { srcFolder, dstFolder, type, messageId });
        }

        public void _tMoveEmail(object parameters)
        {
            string folder = (parameters as List<string>)[0];
            string destination = (parameters as List<string>)[1];
            string type = (parameters as List<string>)[2];
            string messageToMoveId = (parameters as List<string>)[3];

            this.AddLocalOperation(messageToMoveId, destination, type);

            lock (this.lockObject)
            {
                try
                {
                    if (!NetObject.Singleton.IsConnected())
                    {
                        return;
                    }

                    string folder2 = string.Empty;

                    if (folder == Constants.EMAIL_TRASH)
                    {
                        if (type == Constants.EMAIL_READ)
                        {
                            folder2 = Constants.EMAIL_USER_FOLDER_TRASH_READ;
                        }
                        else
                        {
                            folder2 = Constants.EMAIL_USER_FOLDER_TRASH_UNREAD;
                        }
                    }
                    else if (folder == Constants.EMAIL_INBOX)
                    {
                        if (type == Constants.EMAIL_READ)
                        {
                            folder2 = Constants.EMAIL_USER_FOLDER_INBOX_READ;
                        }
                        else
                        {
                            folder2 = Constants.EMAIL_USER_FOLDER_INBOX_UNREAD;
                        }
                    }

                    bool isLoginOk = false;

                    isLoginOk = this.client != null && this.client.IsConnected && this.client.IsLogined;

                    if (!isLoginOk)
                    {
                        isLoginOk = this.Connect();
                    }

                    if (isLoginOk)
                    {
                        folder = this.GetDistantFolder(folder);

                        destination = this.GetDistantFolder(destination);

                        Message m = new Message();
                        MessageCollection mcol = this.client.Folders[folder].Search(type, false);
                        List<string> liste = new List<string>();

                        foreach (ImapX.Message message in mcol)
                        {
                            try
                            {
                                message.Process(false);

                                if (message.MessageId == null)
                                {
                                    // generate fake id
                                    message.MessageId = this.GenerateFakeMessageId(message);
                                }

                                if (message.MessageId.Equals("<" + messageToMoveId + ">") || message.MessageId.Equals(messageToMoveId))
                                {
                                    m = message;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                            }
                        }

                        if (m != null)
                        {
                            bool result = false;

                            // TODO: gmail hack
                            if (ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_ADDRESS).Contains("gmail"))
                            {
                                result = this.client.Folders[folder].CopyMessageToFolder(m, this.client.Folders[destination]);
                            }
                            else
                            {
                                result = this.client.Folders[folder].MoveMessageToFolder(m, this.client.Folders[destination]);
                            }
                        }
                    }

                    this.Disconnect(); 
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                }
            }
        }

        public void DeleteEmail(string folder, string type, string messageId)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "folder: " + folder + ", type: " + type + ", messageId: " + messageId);

            Thread thread = new Thread(new ParameterizedThreadStart(this.ThreadDeleteEmail));
            thread.IsBackground = true;
            thread.Start(new List<string>() { folder, type, messageId });
        }

        public void ThreadDeleteEmail(object parameters)
        {
            string folder = (parameters as List<string>)[0];
            string type = (parameters as List<string>)[1];
            string messageId = (parameters as List<string>)[2];

            this.AddLocalOperation(messageId, string.Empty, string.Empty);
            lock (this.lockObject)
            {
                try
                {
                    if (!NetObject.Singleton.IsConnected())
                    {
                        return;
                    }

                    string folder2 = string.Empty;

                    if (folder == Constants.EMAIL_TRASH)
                    {
                        if (type == Constants.EMAIL_READ)
                        {
                            folder2 = Constants.EMAIL_USER_FOLDER_TRASH_READ;
                        }
                        else
                        {
                            folder2 = Constants.EMAIL_USER_FOLDER_TRASH_UNREAD;
                        }
                    }
                    else if (folder == Constants.EMAIL_INBOX)
                    {
                        if (type == Constants.EMAIL_READ)
                        {
                            folder2 = Constants.EMAIL_USER_FOLDER_INBOX_READ;
                        }
                        else
                        {
                            folder2 = Constants.EMAIL_USER_FOLDER_INBOX_UNREAD;
                        }
                    }

                    bool isLoginOk = false;
                    isLoginOk = this.client != null && this.client.IsConnected && this.client.IsLogined;

                    if (!isLoginOk)
                    {
                        isLoginOk = this.Connect();
                    }

                    if (isLoginOk)
                    {
                        folder = this.GetDistantFolder(folder);

                        Message m = new Message();
                        MessageCollection mcol = this.client.Folders[folder].Search(type, false);
                        List<string> list = new List<string>();
                        foreach (ImapX.Message message in mcol)
                        {
                            try
                            {
                                message.Process(false);

                                if (message.MessageId == null)
                                {
                                    // generate fake id
                                    message.MessageId = this.GenerateFakeMessageId(message);
                                }

                                if (message.MessageId.Equals("<" + messageId + ">") || message.MessageId.Equals(messageId))
                                {
                                    m = message;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                            }
                        }

                        if (m != null)
                        {
                            bool result = false;
                            result = this.client.Folders[folder].DeleteMessage(m);
                        }
                    }

                    this.Disconnect();
                }
                catch (Exception ex)
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                }
            }
        }

        public void AddLocalOperation(string messageId, string folder, string type)
        {
            lock (this.lockMailSync)
            {
                if (!localOperations.ContainsKey(messageId))
                {
                    localOperations.Add(messageId, folder + "/" + type);
                }
            }
        }

        /***************************
         *      Private methods
         ***************************/

        private string GenerateFakeMessageId(ImapX.Message message)
        {
            string messageId = "_bad_message_id_";  // ensure we don't return a null string

            if (message != null)
            {
                if (message.Date != null)
                {
                    string strToHash = message.Received + message.Subject + message.Date.ToString();
                    byte[] tmpSource;
                    byte[] tmpHash;
                    tmpSource = ASCIIEncoding.ASCII.GetBytes(strToHash);
                    tmpHash = new MD5CryptoServiceProvider().ComputeHash(tmpSource);
                    messageId = "<" + Encryption.Singleton.ByteArrayToString(tmpHash) + ">";
                }
                else
                {
                    LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "can't generate message Id with message.Received: " + message.Received + ", message.Subject: " + message.Subject + ", message.Date: " + message.Date);
                }
            }
            else
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "Message is null.");
            }

            return messageId;
        }
    }
}
