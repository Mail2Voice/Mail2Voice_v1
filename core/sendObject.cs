﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Threading;
    using ImapX;
    using Mail2VoiceCore;

    public class SendObject
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static SendObject instance = null;

        private Thread sendThread = null;
        private SmtpClient client = null;
        private SmtpClient localStore = null;
        private MailMessage message = null;
        private object lockObject = new object();

        public static SendObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new SendObject();
                }

                return instance;
            }
        }

        /**************************
         *      Constructor
         ***************************/
        static SendObject()
        {
            instance = new SendObject();
        }

        #endregion "Singleton"

        /***************************
         *      Public events
         ***************************/    
        public static event EventHandler SendEmailCompleted;
        public static event EventHandler SendEmailError;

        /***************************
         *      Public methods
         ***************************/

        #region "Send"
        public void SendMail(string fullName, string email, string subject, string message, string tmpFile, bool ssl)
        {
            LogObject.Singleton.Write(
                new System.Diagnostics.StackFrame().GetMethod().Name,
                "fullName: " + fullName + ", email: " + email + ", subject: " + subject + ", message: " + message + ", tmpFile:" + tmpFile + ", ssl: " + ssl);
            try
            {
                List<string> fileList = new List<string>();
                fileList.Add(tmpFile);

                this.SendMailMultiAttach(fullName, email, subject, message, fileList, ssl);
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void SendMailMultiAttach(string fullName, string email, string subject, string message, List<string> attachObjects, bool ssl)
        {
            LogObject.Singleton.Write(
                new System.Diagnostics.StackFrame().GetMethod().Name,
                "fullName: " + fullName + ", email: " + email + ", subject: " + subject + ", message: " + message + ", attachObjects.Count.ToString(): " + attachObjects.Count.ToString() + ", ssl: " + ssl.ToString());
            try
            {
                if (!NetObject.Singleton.IsConnected())
                {
                    return;
                }

                if (this.sendThread != null && this.sendThread.ThreadState == ThreadState.Running)
                {
                    return;
                }

                if (!string.IsNullOrEmpty(email))
                {
                    lock (this.lockObject)
                    {
                        this.sendThread = new Thread(this.SendEmail);

                        this.message = new MailMessage();
                        this.message.From = new System.Net.Mail.MailAddress(
                            ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN),
                            ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_NAME));

                        this.message.To.Add(new System.Net.Mail.MailAddress(email, fullName));
                        this.message.Subject = subject;
                        this.message.Body = message;

                        for (int x = 0; x < attachObjects.Count; x++)
                        {
                            if (File.Exists(attachObjects[x]))
                            {
                                this.message.Attachments.Add(new System.Net.Mail.Attachment(attachObjects[x]));
                            }
                        }

                        this.client = new SmtpClient(
                            ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_ADDRESS),
                            Convert.ToInt32(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_PORT)));

                        this.localStore = new SmtpClient(
                            ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_ADDRESS),
                                Convert.ToInt32(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_PORT)));

                        this.localStore.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                        this.localStore.PickupDirectoryLocation = AppDomain.CurrentDomain.BaseDirectory + "emails\\" + ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN) + "\\OUTGOING\\";
                        
                        if (!Directory.Exists(this.localStore.PickupDirectoryLocation))
                        {
                            Directory.CreateDirectory(this.localStore.PickupDirectoryLocation);
                        }

                        this.client.EnableSsl = ssl;
                        this.client.Credentials = new System.Net.NetworkCredential(
                            ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN),
                            Encryption.Singleton.EncryptString(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_PASSWORD), Constants.PASSWORD_KEY));
                    }

                    this.sendThread.Start();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void SendEmail()
        {
            lock (this.lockObject)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
                if (this.client != null && this.message != null)
                {
                    try
                    {
                        this.client.Send(this.message);
                        this.localStore.Send(this.message);

                        var imapClient = new ImapClient(
                            ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_ADDRESS),
                            Convert.ToInt32(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_PORT)),
                            Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_SSL)));

                        if (imapClient != null)
                        {
                            imapClient.Connection();

                            if (imapClient.IsConnected)
                            {
                                bool isLoginOk = imapClient.LogIn(
                                    ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN),
                                    Encryption.Singleton.EncryptString(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_PASSWORD), Constants.PASSWORD_KEY));

                                if (isLoginOk)
                                {
                                    var directory = new DirectoryInfo("emails\\" + ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN) + "\\OUTGOING\\");
                                    var myFile = (from f in directory.GetFiles()
                                        orderby f.LastWriteTime descending
                                        select f).FirstOrDefault();

                                    string filename = myFile.DirectoryName + "\\" + myFile.Name;

                                    string selectedFolder = "Sent";
                                    imapClient._client.SelectFolder(selectedFolder);
                                    ArrayList arrayList = new ArrayList();
                                    string text = File.ReadAllText(filename);
                                    int length = text.Length;
                                    string flag = "\\draft";
                                    string command = string.Concat(new object[]
                                        {
                                            "APPEND \"", 
                                            "Sent", 
                                            "\" (", 
                                            flag, 
                                            ") {", 
                                            length - 2, 
                                            "}\r\n"
                                        });
                                    
                                    if (imapClient._client.SendAndReceive(command, ref arrayList) && imapClient._client.SendData(text))
                                    {
                                        // Nothing to do?
                                    }

                                    File.Delete(filename);
                                }
                            }

                            imapClient.Disconnect();
                        }

                        if (SendObject.SendEmailCompleted != null)
                        {
                            SendObject.SendEmailCompleted(null, EventArgs.Empty);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (SendObject.SendEmailError != null)
                        {
                            SendObject.SendEmailError(null, EventArgs.Empty);
                        }

                        LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
                    }
                }
            }
        }
        #endregion "Send"
    }
}
