﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice Library.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceCore
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Speech.Synthesis;

    public class SpeechObject
    {
        /***************************
         *      Attributes
         ***************************/
        #region "Singleton"
        private static SpeechObject instance = null;
        private SpeechSynthesizer speechSynth = new SpeechSynthesizer();
        private string currentVoiceName = string.Empty;
        private Dictionary<string, string> defaultVoiceStrings = new Dictionary<string, string>();

        public static SpeechObject Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new SpeechObject();
                }

                return instance;
            }
        }
        #endregion "Singleton"

        public string CurrentVoiceName
        {
            get { return this.currentVoiceName; }
        }

        /***************************
         *      Public delegates
         ***************************/
        public delegate void SpeakCompletingHandler(object sender, SpeakCompletedEventArgs e);
        public delegate void StateChangingHandler(object sender, StateChangedEventArgs e);
        public delegate void SpeakProgressingHandler(object sender, SpeakProgressEventArgs e);

        /***************************
         *      Public events
         ***************************/  
        public event SpeakCompletingHandler SpeakCompleted;
        public event StateChangingHandler StateChanged;
        public event SpeakProgressingHandler SpeakProgress;

        /***************************
         *      Public methods
         ***************************/
        public void Init()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            this.speechSynth.SpeakCompleted += new EventHandler<SpeakCompletedEventArgs>(this.Speech_SpeakCompleted);
            this.speechSynth.SpeakProgress += new EventHandler<SpeakProgressEventArgs>(this.Speech_SpeakProgress);
            this.speechSynth.StateChanged += new EventHandler<StateChangedEventArgs>(this.Speech_StateChanged);
        }

        public List<string> GetVoicesList()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            try
            {
                List<string> names = new List<string>();

                if (this.speechSynth != null)
                {
                    ReadOnlyCollection<InstalledVoice> voices = this.speechSynth.GetInstalledVoices();

                    for (int x = 0; x < voices.Count; x++)
                    {
                        names.Add(voices[x].VoiceInfo.Name);
                    }
                        
                    return names;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }

            return null;
        }

        public void SetVoice(string name)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "name: " + name);

            try
            {
                this.currentVoiceName = name;
                if (!string.IsNullOrEmpty(name))
                {
                    if (this.speechSynth != null)
                    {
                        this.speechSynth.SelectVoice(name);
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void AddVoiceString(string id, string voiceString)
        {
            if (!id.StartsWith(Constants.VOICE_SENTENCES_SECTION + "#"))
            {
                id = Constants.VOICE_SENTENCES_SECTION + "#" + id;
            }

            if (!this.defaultVoiceStrings.ContainsKey(id))
            {
                this.defaultVoiceStrings.Add(id, voiceString);
            }
        }

        public string GetVoiceString(string id)
        {
            if (!id.StartsWith(Constants.VOICE_SENTENCES_SECTION + "#"))
            {
                id = Constants.VOICE_SENTENCES_SECTION + "#" + id;
            }

            return LanguageObject.Singleton.Translate(id, this.defaultVoiceStrings[id]);
        }

        public void Speak(string message)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "message: " + message);

            this.speechSynth.Rate = 5;

            try
            {
                if (this.speechSynth != null)
                {
                    this.Stop();

                    // TODO: find a better way
                    this.speechSynth.Dispose();
                    this.speechSynth = new SpeechSynthesizer();
                    this.speechSynth.SpeakCompleted += new EventHandler<SpeakCompletedEventArgs>(this.Speech_SpeakCompleted);
                    this.speechSynth.SpeakProgress += new EventHandler<SpeakProgressEventArgs>(this.Speech_SpeakProgress);
                    this.speechSynth.StateChanged += new EventHandler<StateChangedEventArgs>(this.Speech_StateChanged);
                    this.speechSynth.SetOutputToDefaultAudioDevice();

                    this.SetVoice(this.currentVoiceName);
                    this.speechSynth.SpeakAsync(message.Trim());
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void Stop()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            try
            {
                if (this.speechSynth != null)
                {
                    this.speechSynth.SpeakAsyncCancelAll();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void Pause()
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            try
            {
                if (this.speechSynth != null)
                {
                    if (this.speechSynth.State == SynthesizerState.Speaking)
                    {
                        this.speechSynth.Pause();
                    }
                    else if (this.speechSynth.State == SynthesizerState.Paused)
                    {
                        this.speechSynth.Resume();
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        /********************************
         *      Private event handlers
         ********************************/

        #region "Handle events"
        private void Speech_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);

            if (this.SpeakCompleted != null)
            {
                this.SpeakCompleted(sender, e);
            }
        }

        private void Speech_StateChanged(object sender, StateChangedEventArgs e)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            if (this.StateChanged != null)
            {
                this.StateChanged(sender, e);
            }
        }

        private void Speech_SpeakProgress(object sender, SpeakProgressEventArgs e)
        {
            LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name);
            if (this.SpeakProgress != null)
            {
                this.SpeakProgress(sender, e);
            }
        }
        #endregion "Handle events"
    }
}
