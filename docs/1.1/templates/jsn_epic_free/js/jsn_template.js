/**
* @author    JoomlaShine.com http://www.joomlashine.com
* @copyright Copyright (C) 2008 - 2011 JoomlaShine.com. All rights reserved.
* @license   GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
*/
var JSNTemplate= {
	_templateParams:		{},

	initOnDomReady: function()
	{
		if (!_templateParams.enableMobile) {
			// Menu dropdown setup
			JSNUtils.setDropdownMenu("menu-mainmenu", 0, {});
			// General layout setup
			JSNUtils.setInnerLayout(["jsn-content_inner3", "jsn-leftsidecontent", "jsn-rightsidecontent", "jsn-pos-innerleft", "jsn-pos-innerright"]);
		}
	},

	initOnLoad: function()
	{
		if (!_templateParams.enableMobile) {
			// Setup vertical positions of stickleft, stickright positions
			JSNUtils.setVerticalPosition("jsn-pos-stick-leftmiddle", 'middle');
			JSNUtils.setVerticalPosition("jsn-pos-stick-rightmiddle", 'middle');

			// Fix IE6 PNG issue
			if (JSNUtils.isIE6()) {
				DD_belatedPNG.fix('#jsn-logo img, ul.menu-mainmenu a, ul.menu-mainmenu a span, ul.menu-mainmenu ul, .menu-treemenu a, .breadcrumbs a, .breadcrumbs span, .createdate, .list-arrow li, .jsn-top_inner, .jsn-middle, .jsn-middle_inner, .jsn-bottom, .jsn-bottom_inner');
			}
		}
	},

	initTemplate: function(templateParams)
	{
		// Store template parameters
		_templateParams = templateParams;
		
		// Init template on "domready" event
		window.addEvent('domready', JSNTemplate.initOnDomReady);

		// Init template on "load" event
		JSNUtils.addEvent(window, 'load', JSNTemplate.initOnLoad);
	}
}
