﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice
{
    using Mail2Voice.Widgets;
    partial class MainForm
    {
        private MainButton inboxButton;
        private MainButton sendButton;
        private MainButton trashButton;
        private MainButton configurationButton;
        private MainButton helpButton;
        private MainButton quitButton;
        private MainButton contactsButton;
        private System.Windows.Forms.Panel globalControlsPanel;
        private System.Windows.Forms.Panel mailGenericControlsPanel;
        private System.Windows.Forms.ImageList mimeTypesImageList;
        private System.Windows.Forms.Timer receptionTimer;
        private System.Windows.Forms.ImageList themeImageList64;
        private System.Windows.Forms.PictureBox operationPicture;
        private System.Windows.Forms.Timer timerVersion;

        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.globalControlsPanel = new System.Windows.Forms.Panel();
            this.operationPicture = new System.Windows.Forms.PictureBox();
            this.mailGenericControlsPanel = new System.Windows.Forms.Panel();
            this.inboxButton = new MainButton(Mail2Voice.properties.Resources.inbox, Mail2Voice.properties.Resources.inbox_BW);
            this.contactsButton = new MainButton(Mail2Voice.properties.Resources.addressbook, Mail2Voice.properties.Resources.addressbook_BW);
            this.helpButton = new MainButton(Mail2Voice.properties.Resources.help, Mail2Voice.properties.Resources.help_BW);
            this.configurationButton = new MainButton(Mail2Voice.properties.Resources.tools, Mail2Voice.properties.Resources.tools_BW);
            this.quitButton = new MainButton(Mail2Voice.properties.Resources.quit, Mail2Voice.properties.Resources.quit_BW);
            this.sendButton = new MainButton(Mail2Voice.properties.Resources.media_record, Mail2Voice.properties.Resources.media_record_BW);
            this.trashButton = new MainButton(Mail2Voice.properties.Resources.trash, Mail2Voice.properties.Resources.trash_BW);
            this.mimeTypesImageList = new System.Windows.Forms.ImageList(this.components);
            this.receptionTimer = new System.Windows.Forms.Timer(this.components);
            this.themeImageList64 = new System.Windows.Forms.ImageList(this.components);
            this.timerVersion = new System.Windows.Forms.Timer(this.components);
            this.globalControlsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) this.operationPicture).BeginInit();
            this.mailGenericControlsPanel.SuspendLayout();
            this.SuspendLayout();

            // globalControlsPanel
            this.globalControlsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(234)))), ((int)(((byte)(219)))));
            this.globalControlsPanel.Controls.Add(this.operationPicture);
            this.globalControlsPanel.Controls.Add(this.inboxButton);
            this.globalControlsPanel.Controls.Add(this.mailGenericControlsPanel);
            this.globalControlsPanel.Controls.Add(this.sendButton);
            this.globalControlsPanel.Controls.Add(this.trashButton);
            this.globalControlsPanel.Controls.Add(this.contactsButton);
            this.globalControlsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.globalControlsPanel.Location = new System.Drawing.Point(0, 0);
            this.globalControlsPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.globalControlsPanel.Name = "globalControlsPanel";
            this.globalControlsPanel.Size = new System.Drawing.Size(1016, 94);
            this.globalControlsPanel.TabIndex = 0;

            // operationPicture
            this.operationPicture.Image = Mail2Voice.properties.Resources.working_48_B;
            this.operationPicture.Location = new System.Drawing.Point(468, 4);
            this.operationPicture.Name = "operationPicture";
            this.operationPicture.Size = new System.Drawing.Size(80, 80);
            this.operationPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.operationPicture.TabIndex = 4;
            this.operationPicture.TabStop = false;
            this.operationPicture.Visible = false;

            // inboxButton
            this.inboxButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.inboxButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.inboxButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte) 0);
            this.inboxButton.Image = Mail2Voice.properties.Resources.mailbox;
            this.inboxButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.inboxButton.Location = new System.Drawing.Point(12, 4);
            this.inboxButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.inboxButton.Name = "inboxButton";
            this.inboxButton.Size = new System.Drawing.Size(86, 86);
            this.inboxButton.TabIndex = 0;
			this.inboxButton.Text = "Inbox";
            this.inboxButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.inboxButton.UseVisualStyleBackColor = true;
            this.inboxButton.Click += new System.EventHandler(this.InboxButton_Click);
            this.inboxButton.MouseHover += new System.EventHandler(this.MainButton_Hover);

            // mailGenericControlsPanel
            this.mailGenericControlsPanel.Controls.Add(this.helpButton);
            this.mailGenericControlsPanel.Controls.Add(this.configurationButton);
            this.mailGenericControlsPanel.Controls.Add(this.quitButton);
            this.mailGenericControlsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.mailGenericControlsPanel.Location = new System.Drawing.Point(587, 0);
            this.mailGenericControlsPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.mailGenericControlsPanel.MaximumSize = new System.Drawing.Size(310, 0);
            this.mailGenericControlsPanel.Name = "mailGenericControlsPanel";
            this.mailGenericControlsPanel.Size = new System.Drawing.Size(310, 94);
            this.mailGenericControlsPanel.TabIndex = 3;

            // contactsButton
            this.contactsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.contactsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.contactsButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte) 0);
            this.contactsButton.Image = Mail2Voice.properties.Resources.adressbook;
            this.contactsButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.contactsButton.Location = new System.Drawing.Point(450, 4);
            this.contactsButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.contactsButton.Name = "contactsButton";
            this.contactsButton.Size = new System.Drawing.Size(86, 86);
            this.contactsButton.TabIndex = 0;
            this.contactsButton.Text = "Contacts";
            this.contactsButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.contactsButton.UseVisualStyleBackColor = true;
            this.contactsButton.Click += new System.EventHandler(this.ContactsButton_Click);
            this.contactsButton.MouseHover += new System.EventHandler(this.MainButton_Hover);

            // helpButton
            this.helpButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.helpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.helpButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte) 0);
            this.helpButton.Image = (System.Drawing.Image) resources.GetObject("helpButton.Image");
            this.helpButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.helpButton.Location = new System.Drawing.Point(4, 4);
            this.helpButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(86, 86);
            this.helpButton.TabIndex = 2;
			this.helpButton.Text = "Help";
            this.helpButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.HelpButton_Click);
            this.helpButton.MouseHover += new System.EventHandler(this.MainButton_Hover);

            // configurationButton
            this.configurationButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.configurationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.configurationButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte) 0);
            this.configurationButton.Image = (System.Drawing.Image) resources.GetObject("configurationButton.Image");
            this.configurationButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.configurationButton.Location = new System.Drawing.Point(100, 4);
            this.configurationButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.configurationButton.Name = "configurationButton";
            this.configurationButton.Size = new System.Drawing.Size(86, 86);
            this.configurationButton.TabIndex = 1;
			this.configurationButton.Text = "Settings";
            this.configurationButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.configurationButton.UseVisualStyleBackColor = true;
            this.configurationButton.Click += new System.EventHandler(this.ConfigurationButton_Click);
            this.configurationButton.MouseHover += new System.EventHandler(this.MainButton_Hover);

            // quitButton
            this.quitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quitButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte) 0);
            this.quitButton.Image = (System.Drawing.Image) resources.GetObject("quitButton.Image");
            this.quitButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.quitButton.Location = new System.Drawing.Point(216, 4);
            this.quitButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(86, 86);
            this.quitButton.TabIndex = 3;
			this.quitButton.Text = "Quit";
            this.quitButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.QuitButton_Click);
            this.quitButton.MouseHover += new System.EventHandler(this.MainButton_Hover);

            // sendButton
            this.sendButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendButton.Image = Mail2Voice.properties.Resources.write;
            this.sendButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.sendButton.Location = new System.Drawing.Point(110, 4);
            this.sendButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(86, 86);
            this.sendButton.TabIndex = 1;
			this.sendButton.Text = "Send";
            this.sendButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.SendButton_Click);
            this.sendButton.MouseHover += new System.EventHandler(this.MainButton_Hover);
            // 
            // trashButton
            // 
            this.trashButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.trashButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.trashButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trashButton.Image = ((System.Drawing.Image)(resources.GetObject("garbageButton.Image")));
            this.trashButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.trashButton.Location = new System.Drawing.Point(212, 4);
            this.trashButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trashButton.Name = "trashButton";
            this.trashButton.Size = new System.Drawing.Size(86, 86);
            this.trashButton.TabIndex = 2;
			this.trashButton.Text = "Trash";
            this.trashButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.trashButton.UseVisualStyleBackColor = true;
            this.trashButton.Click += new System.EventHandler(this.TrashButton_Click);
            this.trashButton.MouseHover += new System.EventHandler(this.MainButton_Hover);
            // 
            // mimeTypesImageList
            // 
            this.mimeTypesImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("mimeTypesImageList.ImageStream")));
            this.mimeTypesImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.mimeTypesImageList.Images.SetKeyName(0, "zip.png");
            this.mimeTypesImageList.Images.SetKeyName(1, "7z.png");
            this.mimeTypesImageList.Images.SetKeyName(2, "aac.png");
            this.mimeTypesImageList.Images.SetKeyName(3, "ac3.png");
            this.mimeTypesImageList.Images.SetKeyName(4, "ace.png");
            this.mimeTypesImageList.Images.SetKeyName(5, "arc.png");
            this.mimeTypesImageList.Images.SetKeyName(6, "avi.png");
            this.mimeTypesImageList.Images.SetKeyName(7, "cda.png");
            this.mimeTypesImageList.Images.SetKeyName(8, "doc.png");
            this.mimeTypesImageList.Images.SetKeyName(9, "flac.png");
            this.mimeTypesImageList.Images.SetKeyName(10, "iso.png");
            this.mimeTypesImageList.Images.SetKeyName(11, "jpg.png");
            this.mimeTypesImageList.Images.SetKeyName(12, "midi.png");
            this.mimeTypesImageList.Images.SetKeyName(13, "mime.png");
            this.mimeTypesImageList.Images.SetKeyName(14, "mp2.png");
            this.mimeTypesImageList.Images.SetKeyName(15, "mp3.png");
            this.mimeTypesImageList.Images.SetKeyName(16, "mp4.png");
            this.mimeTypesImageList.Images.SetKeyName(17, "mpeg.png");
            this.mimeTypesImageList.Images.SetKeyName(18, "nfo.png");
            this.mimeTypesImageList.Images.SetKeyName(19, "ods.png");
            this.mimeTypesImageList.Images.SetKeyName(20, "odt.png");
            this.mimeTypesImageList.Images.SetKeyName(21, "ogg.png");
            this.mimeTypesImageList.Images.SetKeyName(22, "pdf.png");
            this.mimeTypesImageList.Images.SetKeyName(23, "png.png");
            this.mimeTypesImageList.Images.SetKeyName(24, "ps.png");
            this.mimeTypesImageList.Images.SetKeyName(25, "readme.png");
            this.mimeTypesImageList.Images.SetKeyName(26, "rtf.png");
            this.mimeTypesImageList.Images.SetKeyName(27, "tar.png");
            this.mimeTypesImageList.Images.SetKeyName(28, "unknown.png");
            this.mimeTypesImageList.Images.SetKeyName(29, "url.png");
            this.mimeTypesImageList.Images.SetKeyName(30, "wav.png");
            this.mimeTypesImageList.Images.SetKeyName(31, "wma.png");
            this.mimeTypesImageList.Images.SetKeyName(32, "xls.png");

            // receptionTimer
            this.receptionTimer.Interval = 500;
            this.receptionTimer.Tick += new System.EventHandler(this.ReceptionTimer_Tick);

            // themeImageList64
            this.themeImageList64.ImageStream = (System.Windows.Forms.ImageListStreamer) resources.GetObject("themeImageList64.ImageStream");
            this.themeImageList64.TransparentColor = System.Drawing.Color.Transparent;
            this.themeImageList64.Images.SetKeyName(0, "user_new.png");
            this.themeImageList64.Images.SetKeyName(1, "addressbook.png");
            this.themeImageList64.Images.SetKeyName(2, "addressbook_BW.png");
            this.themeImageList64.Images.SetKeyName(3, "document_open.png");
            this.themeImageList64.Images.SetKeyName(4, "Help.png");
            this.themeImageList64.Images.SetKeyName(5, "help_BW.png");
            this.themeImageList64.Images.SetKeyName(6, "inbox.png");
            this.themeImageList64.Images.SetKeyName(7, "inbox_BW.png");
            this.themeImageList64.Images.SetKeyName(8, "mail_attach.png");
            this.themeImageList64.Images.SetKeyName(9, "mail_read.png");
            this.themeImageList64.Images.SetKeyName(10, "mail_sendreceive.png");
            this.themeImageList64.Images.SetKeyName(11, "mail_unread.png");
            this.themeImageList64.Images.SetKeyName(12, "media_record.png");
            this.themeImageList64.Images.SetKeyName(13, "media_record_BW.png");
            this.themeImageList64.Images.SetKeyName(14, "no.png");
            this.themeImageList64.Images.SetKeyName(15, "ok.png");
            this.themeImageList64.Images.SetKeyName(16, "quit.png");
            this.themeImageList64.Images.SetKeyName(17, "quit_BW.png");
            this.themeImageList64.Images.SetKeyName(18, "Tools.png");
            this.themeImageList64.Images.SetKeyName(19, "tools_BW.png");
            this.themeImageList64.Images.SetKeyName(20, "transparent.png");
            this.themeImageList64.Images.SetKeyName(21, "trash.png");
            this.themeImageList64.Images.SetKeyName(22, "trash_BW.png");
            this.themeImageList64.Images.SetKeyName(23, "user.png");
            this.themeImageList64.Images.SetKeyName(24, "user_delete.png");
            this.themeImageList64.Images.SetKeyName(25, "user_edit.png");
            this.themeImageList64.Images.SetKeyName(26, "user_unknow.png");
            // 
            // timerVersion
            // 
            this.timerVersion.Interval = 60000;
            this.timerVersion.Tick += new System.EventHandler(this.TimerVersion_Tick);
            // 
            // mainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1016, 574);
            this.Controls.Add(this.globalControlsPanel);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(41)))), ((int)(((byte)(104)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "Mail2Voice";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.globalControlsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.operationPicture)).EndInit();
            this.mailGenericControlsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}