﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    partial class ConfigurationControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationControl));
            this.appLangLabel = new System.Windows.Forms.Label();
            this.appLangComboBox = new System.Windows.Forms.ComboBox();
            this.appVoiceLabel = new System.Windows.Forms.Label();
            this.appVoiceComboBox = new System.Windows.Forms.ComboBox();
            this.mainButtonSpeechPreviewCheckBox = new System.Windows.Forms.CheckBox();
            this.inServerParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.testImapButton = new System.Windows.Forms.Button();
            this.imapSSLCheckbox = new System.Windows.Forms.CheckBox();
            this.inServerPortTextBox = new System.Windows.Forms.TextBox();
            this.inServerPortLabel = new System.Windows.Forms.Label();
            this.inServerAddressTextBox = new System.Windows.Forms.TextBox();
            this.inServerAddressLabel = new System.Windows.Forms.Label();
            this.outServerParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.smtpSSLCheckbox = new System.Windows.Forms.CheckBox();
            this.testSmtpButton = new System.Windows.Forms.Button();
            this.outServerAddressTextBox = new System.Windows.Forms.TextBox();
            this.outServerPortLabel = new System.Windows.Forms.Label();
            this.outServerPortTextBox = new System.Windows.Forms.TextBox();
            this.outServerAddressLabel = new System.Windows.Forms.Label();
            this.accountNameTextBox = new System.Windows.Forms.TextBox();
            this.accountNameLabel = new System.Windows.Forms.Label();
            this.loginParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.accountPasswordTextBox = new System.Windows.Forms.TextBox();
            this.accountPasswordLabel = new System.Windows.Forms.Label();
            this.accountAddressTextBox = new System.Windows.Forms.TextBox();
            this.accountAddressLabel = new System.Windows.Forms.Label();
            this.accountParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.defaultMsgParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.defaultMsgContentTextBox = new System.Windows.Forms.TextBox();
            this.defaultMsgObjectTextBox = new System.Windows.Forms.TextBox();
            this.defaultMsgContentLabel = new System.Windows.Forms.Label();
            this.defaultMsgObjectLabel = new System.Windows.Forms.Label();
            this.globalParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.activateLogsCheckBox = new System.Windows.Forms.CheckBox();
            this.activateTrashCheckBox = new System.Windows.Forms.CheckBox();
            this.advancedParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.appUpdateCheckFreqComboBox = new System.Windows.Forms.ComboBox();
            this.activateUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.activateDetailedLogsCheckBox = new System.Windows.Forms.CheckBox();
            this.addressBookParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.importAddressBookButton = new System.Windows.Forms.Button();
            this.importOldAccountGroupBox = new System.Windows.Forms.GroupBox();
            this.importOldAccountButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.activateUsageStatsCheckBox = new System.Windows.Forms.CheckBox();
            this.openAddressBookDialog = new System.Windows.Forms.OpenFileDialog();
            this.openOldAccountFolderDialog = new System.Windows.Forms.OpenFileDialog();
            this.inServerParamsGroupBox.SuspendLayout();
            this.outServerParamsGroupBox.SuspendLayout();
            this.loginParamsGroupBox.SuspendLayout();
            this.accountParamsGroupBox.SuspendLayout();
            this.defaultMsgParamsGroupBox.SuspendLayout();
            this.globalParamsGroupBox.SuspendLayout();
            this.advancedParamsGroupBox.SuspendLayout();
            this.addressBookParamsGroupBox.SuspendLayout();
            this.importOldAccountGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // appLangLabel
            // 
            this.appLangLabel.AutoSize = true;
            this.appLangLabel.Location = new System.Drawing.Point(19, 35);
            this.appLangLabel.Name = "appLangLabel";
            this.appLangLabel.Size = new System.Drawing.Size(58, 16);
            this.appLangLabel.TabIndex = 0;
            this.appLangLabel.Text = "Lang:";
            // 
            // appLangComboBox
            // 
            this.appLangComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.appLangComboBox.FormattingEnabled = true;
            this.appLangComboBox.Location = new System.Drawing.Point(83, 32);
            this.appLangComboBox.Name = "appLangComboBox";
            this.appLangComboBox.Size = new System.Drawing.Size(307, 24);
            this.appLangComboBox.TabIndex = 1;
            this.appLangComboBox.SelectionChangeCommitted += new System.EventHandler(this.LangComboBox_SelectionChangeCommitted);
            // 
            // appVoiceLabel
            // 
            this.appVoiceLabel.AutoSize = true;
            this.appVoiceLabel.Location = new System.Drawing.Point(36, 78);
            this.appVoiceLabel.Name = "appVoiceLabel";
            this.appVoiceLabel.Size = new System.Drawing.Size(41, 16);
            this.appVoiceLabel.TabIndex = 4;
            this.appVoiceLabel.Text = "Voice:";
            // 
            // appVoiceComboBox
            // 
            this.appVoiceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.appVoiceComboBox.FormattingEnabled = true;
            this.appVoiceComboBox.Location = new System.Drawing.Point(83, 75);
            this.appVoiceComboBox.Name = "appVoiceComboBox";
            this.appVoiceComboBox.Size = new System.Drawing.Size(307, 24);
            this.appVoiceComboBox.TabIndex = 2;
            // 
            // mainButtonSpeechPreviewCheckBox
            // 
            this.mainButtonSpeechPreviewCheckBox.AutoSize = true;
            this.mainButtonSpeechPreviewCheckBox.Location = new System.Drawing.Point(83, 110);
            this.mainButtonSpeechPreviewCheckBox.Name = "mainButtonSpeechPreviewCheckBox";
            this.mainButtonSpeechPreviewCheckBox.Size = new System.Drawing.Size(78, 20);
            this.mainButtonSpeechPreviewCheckBox.TabIndex = 3;
            this.mainButtonSpeechPreviewCheckBox.Text = "Speech announcement for menus on mouse hover";
            this.mainButtonSpeechPreviewCheckBox.UseVisualStyleBackColor = true;
            // 
            // inServerParamsGroupBox
            // 
            this.inServerParamsGroupBox.Controls.Add(this.testImapButton);
            this.inServerParamsGroupBox.Controls.Add(this.imapSSLCheckbox);
            this.inServerParamsGroupBox.Controls.Add(this.inServerPortTextBox);
            this.inServerParamsGroupBox.Controls.Add(this.inServerPortLabel);
            this.inServerParamsGroupBox.Controls.Add(this.inServerAddressTextBox);
            this.inServerParamsGroupBox.Controls.Add(this.inServerAddressLabel);
            this.inServerParamsGroupBox.Location = new System.Drawing.Point(4, 246);
            this.inServerParamsGroupBox.Name = "inServerParamsGroupBox";
            this.inServerParamsGroupBox.Size = new System.Drawing.Size(227, 177);
            this.inServerParamsGroupBox.TabIndex = 3;
            this.inServerParamsGroupBox.TabStop = false;
            this.inServerParamsGroupBox.Text = "Incoming server";
            // 
            // testImapButton
            // 
            this.testImapButton.Location = new System.Drawing.Point(66, 140);
            this.testImapButton.Name = "testImapButton";
            this.testImapButton.Size = new System.Drawing.Size(75, 23);
            this.testImapButton.TabIndex = 4;
            this.testImapButton.Text = "Test";
            this.testImapButton.UseVisualStyleBackColor = true;
            this.testImapButton.Click += new System.EventHandler(this.TestImapButton_Click);
            // 
            // imapSSLCheckbox
            // 
            this.imapSSLCheckbox.AutoSize = true;
            this.imapSSLCheckbox.Location = new System.Drawing.Point(133, 100);
            this.imapSSLCheckbox.Name = "imapSSLCheckbox";
            this.imapSSLCheckbox.Size = new System.Drawing.Size(78, 20);
            this.imapSSLCheckbox.TabIndex = 3;
            this.imapSSLCheckbox.Text = "Secured";
            this.imapSSLCheckbox.UseVisualStyleBackColor = true;
            this.imapSSLCheckbox.Click += new System.EventHandler(this.ImapSSLCheckbox_Click);
            // 
            // inServerPortTextBox
            // 
            this.inServerPortTextBox.Location = new System.Drawing.Point(51, 98);
            this.inServerPortTextBox.Name = "inServerPortTextBox";
            this.inServerPortTextBox.Size = new System.Drawing.Size(66, 22);
            this.inServerPortTextBox.TabIndex = 2;
            // 
            // inServerPortLabel
            // 
            this.inServerPortLabel.AutoSize = true;
            this.inServerPortLabel.Location = new System.Drawing.Point(5, 101);
            this.inServerPortLabel.Name = "inServerPortLabel";
            this.inServerPortLabel.Size = new System.Drawing.Size(40, 16);
            this.inServerPortLabel.TabIndex = 2;
            this.inServerPortLabel.Text = "Port:";
            // 
            // inServerAddressTextBox
            // 
            this.inServerAddressTextBox.Location = new System.Drawing.Point(6, 54);
            this.inServerAddressTextBox.Name = "inServerAddressTextBox";
            this.inServerAddressTextBox.Size = new System.Drawing.Size(215, 22);
            this.inServerAddressTextBox.TabIndex = 1;
            // 
            // inServerAddressLabel
            // 
            this.inServerAddressLabel.AutoSize = true;
            this.inServerAddressLabel.Location = new System.Drawing.Point(3, 35);
            this.inServerAddressLabel.Name = "inServerAddressLabel";
            this.inServerAddressLabel.Size = new System.Drawing.Size(163, 16);
            this.inServerAddressLabel.TabIndex = 0;
            this.inServerAddressLabel.Text = "IMAP server address:";
            // 
            // outServerParamsGroupBox
            // 
            this.outServerParamsGroupBox.Controls.Add(this.smtpSSLCheckbox);
            this.outServerParamsGroupBox.Controls.Add(this.testSmtpButton);
            this.outServerParamsGroupBox.Controls.Add(this.outServerAddressTextBox);
            this.outServerParamsGroupBox.Controls.Add(this.outServerPortLabel);
            this.outServerParamsGroupBox.Controls.Add(this.outServerPortTextBox);
            this.outServerParamsGroupBox.Controls.Add(this.outServerAddressLabel);
            this.outServerParamsGroupBox.Location = new System.Drawing.Point(243, 246);
            this.outServerParamsGroupBox.Name = "outServerParamsGroupBox";
            this.outServerParamsGroupBox.Size = new System.Drawing.Size(230, 177);
            this.outServerParamsGroupBox.TabIndex = 4;
            this.outServerParamsGroupBox.TabStop = false;
            this.outServerParamsGroupBox.Text = "Outgoing server";
            // 
            // smtpSSLCheckbox
            // 
            this.smtpSSLCheckbox.AutoSize = true;
            this.smtpSSLCheckbox.Location = new System.Drawing.Point(140, 100);
            this.smtpSSLCheckbox.Name = "smtpSSLCheckbox";
            this.smtpSSLCheckbox.Size = new System.Drawing.Size(78, 20);
            this.smtpSSLCheckbox.TabIndex = 4;
            this.smtpSSLCheckbox.Text = "Secured";
            this.smtpSSLCheckbox.UseVisualStyleBackColor = true;
            this.smtpSSLCheckbox.Click += new System.EventHandler(this.SmtpSSLCheckbox_Click);
            // 
            // testSmtpButton
            // 
            this.testSmtpButton.Location = new System.Drawing.Point(77, 140);
            this.testSmtpButton.Name = "testSmtpButton";
            this.testSmtpButton.Size = new System.Drawing.Size(75, 23);
            this.testSmtpButton.TabIndex = 5;
            this.testSmtpButton.Text = "Test";
            this.testSmtpButton.UseVisualStyleBackColor = true;
            this.testSmtpButton.Click += new System.EventHandler(this.TestSmtpButton_Click);
            // 
            // outServerAddressTextBox
            // 
            this.outServerAddressTextBox.Location = new System.Drawing.Point(7, 55);
            this.outServerAddressTextBox.Name = "outServerAddressTextBox";
            this.outServerAddressTextBox.Size = new System.Drawing.Size(218, 22);
            this.outServerAddressTextBox.TabIndex = 1;
            // 
            // outServerPortLabel
            // 
            this.outServerPortLabel.AutoSize = true;
            this.outServerPortLabel.Location = new System.Drawing.Point(6, 101);
            this.outServerPortLabel.Name = "outServerPortLabel";
            this.outServerPortLabel.Size = new System.Drawing.Size(40, 16);
            this.outServerPortLabel.TabIndex = 6;
            this.outServerPortLabel.Text = "Port :";
            // 
            // outServerPortTextBox
            // 
            this.outServerPortTextBox.Location = new System.Drawing.Point(52, 98);
            this.outServerPortTextBox.Name = "outServerPortTextBox";
            this.outServerPortTextBox.Size = new System.Drawing.Size(66, 22);
            this.outServerPortTextBox.TabIndex = 2;
            // 
            // outServerAddressLabel
            // 
            this.outServerAddressLabel.AutoSize = true;
            this.outServerAddressLabel.Location = new System.Drawing.Point(6, 35);
            this.outServerAddressLabel.Name = "outServerAddressLabel";
            this.outServerAddressLabel.Size = new System.Drawing.Size(167, 16);
            this.outServerAddressLabel.TabIndex = 4;
            this.outServerAddressLabel.Text = "SMTP server address:";
            // 
            // accountNameTextBox
            // 
            this.accountNameTextBox.Location = new System.Drawing.Point(6, 48);
            this.accountNameTextBox.Name = "accountNameTextBox";
            this.accountNameTextBox.Size = new System.Drawing.Size(215, 22);
            this.accountNameTextBox.TabIndex = 1;
            // 
            // accountNameLabel
            // 
            this.accountNameLabel.AutoSize = true;
            this.accountNameLabel.Location = new System.Drawing.Point(3, 29);
            this.accountNameLabel.Name = "accountNameLabel";
            this.accountNameLabel.Size = new System.Drawing.Size(43, 16);
            this.accountNameLabel.TabIndex = 8;
            this.accountNameLabel.Text = "Name:";
            // 
            // loginParamsGroupBox
            // 
            this.loginParamsGroupBox.Controls.Add(this.accountPasswordTextBox);
            this.loginParamsGroupBox.Controls.Add(this.accountPasswordLabel);
            this.loginParamsGroupBox.Controls.Add(this.accountAddressTextBox);
            this.loginParamsGroupBox.Controls.Add(this.accountNameTextBox);
            this.loginParamsGroupBox.Controls.Add(this.accountAddressLabel);
            this.loginParamsGroupBox.Controls.Add(this.accountNameLabel);
            this.loginParamsGroupBox.Location = new System.Drawing.Point(4, 18);
            this.loginParamsGroupBox.Name = "loginParamsGroupBox";
            this.loginParamsGroupBox.Size = new System.Drawing.Size(227, 201);
            this.loginParamsGroupBox.TabIndex = 1;
            this.loginParamsGroupBox.TabStop = false;
            this.loginParamsGroupBox.Text = "Login infos";
            // 
            // accountPasswordTextBox
            // 
            this.accountPasswordTextBox.Location = new System.Drawing.Point(6, 164);
            this.accountPasswordTextBox.Name = "accountPasswordTextBox";
            this.accountPasswordTextBox.PasswordChar = '*';
            this.accountPasswordTextBox.Size = new System.Drawing.Size(215, 22);
            this.accountPasswordTextBox.TabIndex = 3;
            // 
            // accountPasswordLabel
            // 
            this.accountPasswordLabel.AutoSize = true;
            this.accountPasswordLabel.Location = new System.Drawing.Point(3, 145);
            this.accountPasswordLabel.Name = "accountPasswordLabel";
            this.accountPasswordLabel.Size = new System.Drawing.Size(95, 16);
            this.accountPasswordLabel.TabIndex = 2;
            this.accountPasswordLabel.Text = "Password:";
            // 
            // accountAddressTextBox
            // 
            this.accountAddressTextBox.Location = new System.Drawing.Point(6, 106);
            this.accountAddressTextBox.Name = "accountAddressTextBox";
            this.accountAddressTextBox.Size = new System.Drawing.Size(215, 22);
            this.accountAddressTextBox.TabIndex = 2;
            this.accountAddressTextBox.TextChanged += new System.EventHandler(this.AccountAddressTextBox_TextChanged);
            // 
            // accountAddressLabel
            // 
            this.accountAddressLabel.AutoSize = true;
            this.accountAddressLabel.Location = new System.Drawing.Point(3, 87);
            this.accountAddressLabel.Name = "accountAddressLabel";
            this.accountAddressLabel.Size = new System.Drawing.Size(99, 16);
            this.accountAddressLabel.TabIndex = 0;
            this.accountAddressLabel.Text = "Email address:";
            // 
            // accountParamsGroupBox
            // 
            this.accountParamsGroupBox.Controls.Add(this.defaultMsgParamsGroupBox);
            this.accountParamsGroupBox.Controls.Add(this.loginParamsGroupBox);
            this.accountParamsGroupBox.Controls.Add(this.outServerParamsGroupBox);
            this.accountParamsGroupBox.Controls.Add(this.inServerParamsGroupBox);
            this.accountParamsGroupBox.Location = new System.Drawing.Point(21, 48);
            this.accountParamsGroupBox.Name = "accountParamsGroupBox";
            this.accountParamsGroupBox.Size = new System.Drawing.Size(481, 429);
            this.accountParamsGroupBox.TabIndex = 1;
            this.accountParamsGroupBox.TabStop = false;
            this.accountParamsGroupBox.Text = "Account settings";
            // 
            // defaultMsgParamsGroupBox
            // 
            this.defaultMsgParamsGroupBox.Controls.Add(this.defaultMsgContentTextBox);
            this.defaultMsgParamsGroupBox.Controls.Add(this.defaultMsgObjectTextBox);
            this.defaultMsgParamsGroupBox.Controls.Add(this.defaultMsgContentLabel);
            this.defaultMsgParamsGroupBox.Controls.Add(this.defaultMsgObjectLabel);
            this.defaultMsgParamsGroupBox.Location = new System.Drawing.Point(243, 18);
            this.defaultMsgParamsGroupBox.Name = "defaultMsgParamsGroupBox";
            this.defaultMsgParamsGroupBox.Size = new System.Drawing.Size(230, 201);
            this.defaultMsgParamsGroupBox.TabIndex = 2;
            this.defaultMsgParamsGroupBox.TabStop = false;
            this.defaultMsgParamsGroupBox.Text = "Default message";
            // 
            // defaultMsgContentTextBox
            // 
            this.defaultMsgContentTextBox.Location = new System.Drawing.Point(6, 106);
            this.defaultMsgContentTextBox.Multiline = true;
            this.defaultMsgContentTextBox.Name = "defaultMsgContentTextBox";
            this.defaultMsgContentTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.defaultMsgContentTextBox.Size = new System.Drawing.Size(218, 80);
            this.defaultMsgContentTextBox.TabIndex = 2;
            this.defaultMsgContentTextBox.Text = "This email is a vocal message. Open the audio file attached to listen to it.";
            // 
            // defaultMsgObjectTextBox
            // 
            this.defaultMsgObjectTextBox.Location = new System.Drawing.Point(6, 48);
            this.defaultMsgObjectTextBox.Name = "defaultMsgObjectTextBox";
            this.defaultMsgObjectTextBox.Size = new System.Drawing.Size(218, 22);
            this.defaultMsgObjectTextBox.TabIndex = 1;
            this.defaultMsgObjectTextBox.Text = "Vocal message";
            // 
            // defaultMsgContentLabel
            // 
            this.defaultMsgContentLabel.AutoSize = true;
            this.defaultMsgContentLabel.Location = new System.Drawing.Point(6, 87);
            this.defaultMsgContentLabel.Name = "defaultMsgContentLabel";
            this.defaultMsgContentLabel.Size = new System.Drawing.Size(69, 16);
            this.defaultMsgContentLabel.TabIndex = 1;
            this.defaultMsgContentLabel.Text = "Message:";
            // 
            // defaultMsgObjectLabel
            // 
            this.defaultMsgObjectLabel.AutoSize = true;
            this.defaultMsgObjectLabel.Location = new System.Drawing.Point(6, 29);
            this.defaultMsgObjectLabel.Name = "defaultMsgObjectLabel";
            this.defaultMsgObjectLabel.Size = new System.Drawing.Size(47, 16);
            this.defaultMsgObjectLabel.TabIndex = 0;
            this.defaultMsgObjectLabel.Text = "Object:";
            // 
            // globalParamsGroupBox
            // 
            this.globalParamsGroupBox.Controls.Add(this.appVoiceComboBox);
            this.globalParamsGroupBox.Controls.Add(this.appVoiceLabel);
            this.globalParamsGroupBox.Controls.Add(this.appLangComboBox);
            this.globalParamsGroupBox.Controls.Add(this.appLangLabel);
            this.globalParamsGroupBox.Controls.Add(this.mainButtonSpeechPreviewCheckBox);
            this.globalParamsGroupBox.Location = new System.Drawing.Point(598, 48);
            this.globalParamsGroupBox.Name = "globalParamsGroupBox";
            this.globalParamsGroupBox.Size = new System.Drawing.Size(406, 135);
            this.globalParamsGroupBox.TabIndex = 2;
            this.globalParamsGroupBox.TabStop = false;
            this.globalParamsGroupBox.Text = "Global settings";
            // 
            // activateLogsCheckBox
            // 
            this.activateLogsCheckBox.AutoSize = true;
            this.activateLogsCheckBox.Location = new System.Drawing.Point(19, 21);
            this.activateLogsCheckBox.Name = "activateLogsCheckBox";
            this.activateLogsCheckBox.Size = new System.Drawing.Size(140, 20);
            this.activateLogsCheckBox.TabIndex = 1;
            this.activateLogsCheckBox.Text = "Enable logs";
            this.activateLogsCheckBox.UseVisualStyleBackColor = true;
            // 
            // activateTrashCheckBox
            // 
            this.activateTrashCheckBox.AutoSize = true;
            this.activateTrashCheckBox.Checked = true;
            this.activateTrashCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.activateTrashCheckBox.Location = new System.Drawing.Point(19, 73);
            this.activateTrashCheckBox.Name = "activateTrashCheckBox";
            this.activateTrashCheckBox.Size = new System.Drawing.Size(214, 20);
            this.activateTrashCheckBox.TabIndex = 5;
            this.activateTrashCheckBox.Text = "Enable trash";
            this.activateTrashCheckBox.UseVisualStyleBackColor = true;
            //
            // activateUsageStatsCheckBox
            //
            this.activateUsageStatsCheckBox.AutoSize = true;
            this.activateUsageStatsCheckBox.Location = new System.Drawing.Point(19, 100);
            this.activateUsageStatsCheckBox.Name = "activateUsageStatsCheckBox";
            this.activateUsageStatsCheckBox.AutoSize = false;
            this.activateUsageStatsCheckBox.Size = new System.Drawing.Size(365, 40);
            this.activateUsageStatsCheckBox.TabIndex = 6;
            this.activateUsageStatsCheckBox.Text = "Send usage statistics to Mail2Voice team. This helps us to know how many Mail2Voice users there are.";
            this.activateUsageStatsCheckBox.UseVisualStyleBackColor = true;
            this.activateUsageStatsCheckBox.Checked = false;
            //
            // advancedParamsGroupBox
            //
            this.advancedParamsGroupBox.Controls.Add(this.appUpdateCheckFreqComboBox);
            this.advancedParamsGroupBox.Controls.Add(this.activateUpdateCheckBox);
            this.advancedParamsGroupBox.Controls.Add(this.activateDetailedLogsCheckBox);
            this.advancedParamsGroupBox.Controls.Add(this.activateTrashCheckBox);
            this.advancedParamsGroupBox.Controls.Add(this.activateLogsCheckBox);
            this.advancedParamsGroupBox.Controls.Add(this.activateUsageStatsCheckBox);
            this.advancedParamsGroupBox.Location = new System.Drawing.Point(598, 190);
            this.advancedParamsGroupBox.Name = "advancedParamsGroupBox";
            this.advancedParamsGroupBox.Size = new System.Drawing.Size(406, 150);
            this.advancedParamsGroupBox.TabIndex = 3;
            this.advancedParamsGroupBox.TabStop = false;
            this.advancedParamsGroupBox.Text = "Advanced settings";
            // 
            // appUpdateCheckFreqComboBox
            // 
            this.appUpdateCheckFreqComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.appUpdateCheckFreqComboBox.FormattingEnabled = true;
            this.appUpdateCheckFreqComboBox.Visible = false;

            // TODO: make this translatble
            this.appUpdateCheckFreqComboBox.Items.AddRange(new object[] {
                "Once a day",
                "Once a week",
                "Once a month"});
            this.appUpdateCheckFreqComboBox.Location = new System.Drawing.Point(261, 97);
            this.appUpdateCheckFreqComboBox.Name = "appUpdateCheckFreqComboBox";
            this.appUpdateCheckFreqComboBox.Size = new System.Drawing.Size(129, 24);
            this.appUpdateCheckFreqComboBox.TabIndex = 7;
            // 
            // activateUpdateCheckBox
            // 
            this.activateUpdateCheckBox.AutoSize = true;
            this.activateUpdateCheckBox.Location = new System.Drawing.Point(19, 99);
            this.activateUpdateCheckBox.Name = "activateUpdateCheckBox";
            this.activateUpdateCheckBox.Size = new System.Drawing.Size(240, 20);
            this.activateUpdateCheckBox.TabIndex = 6;
            this.activateUpdateCheckBox.Text = "Check for updates";
            this.activateUpdateCheckBox.UseVisualStyleBackColor = true;
            this.activateUpdateCheckBox.Visible = false;
            // 
            // activateDetailedLogsCheckBox
            // 
            this.activateDetailedLogsCheckBox.AutoSize = true;
            this.activateDetailedLogsCheckBox.Location = new System.Drawing.Point(19, 47);
            this.activateDetailedLogsCheckBox.Name = "activateDetailedLogsCheckBox";
            this.activateDetailedLogsCheckBox.Size = new System.Drawing.Size(192, 20);
            this.activateDetailedLogsCheckBox.TabIndex = 2;
            this.activateDetailedLogsCheckBox.Text = "Enable detailed logs";
            this.activateDetailedLogsCheckBox.UseVisualStyleBackColor = true;
            // 
            // addressBookParamsGroupBox
            // 
            this.addressBookParamsGroupBox.Controls.Add(this.importAddressBookButton);
            this.addressBookParamsGroupBox.Location = new System.Drawing.Point(598, 350);
            this.addressBookParamsGroupBox.Name = "addressBookParamsGroupBox";
            this.addressBookParamsGroupBox.Size = new System.Drawing.Size(406, 80);
            this.addressBookParamsGroupBox.TabIndex = 4;
            this.addressBookParamsGroupBox.TabStop = false;
			this.addressBookParamsGroupBox.Text = "Addressbook (CSV, LDIF, vCard)";
            // 
            // importAddressBookButton
            // 
            this.importAddressBookButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.importAddressBookButton.Image = Mail2Voice.properties.Resources.download;
            this.importAddressBookButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.importAddressBookButton.Location = new System.Drawing.Point(41, 25);
            this.importAddressBookButton.Name = "importAddressBookButton";
            this.importAddressBookButton.Size = new System.Drawing.Size(324, 40);
            this.importAddressBookButton.TabIndex = 6;
            this.importAddressBookButton.Text = "Import addressbook";
            this.importAddressBookButton.UseVisualStyleBackColor = true;
            this.importAddressBookButton.Click += new System.EventHandler(this.ImportAddressBookButton_Click);
            // 
            // addressBookParamsGroupBox
            // 
            this.importOldAccountGroupBox.Controls.Add(this.importOldAccountButton);
            this.importOldAccountGroupBox.Location = new System.Drawing.Point(598, 440);
            this.importOldAccountGroupBox.Name = "importOldAccountGroupBox";
            this.importOldAccountGroupBox.Size = new System.Drawing.Size(406, 80);
            this.importOldAccountGroupBox.TabIndex = 4;
            this.importOldAccountGroupBox.TabStop = false;
            this.importOldAccountGroupBox.Text = "Import settings from Mail2Voice 1.0 RC1";
            // 
            // importOldAccountButton
            // 
            this.importOldAccountButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.importOldAccountButton.Image = Mail2Voice.properties.Resources.import_settings;
            this.importOldAccountButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.importOldAccountButton.Location = new System.Drawing.Point(41, 25);
            this.importOldAccountButton.Name = "importOldAccountButton";
            this.importOldAccountButton.Size = new System.Drawing.Size(324, 40);
            this.importOldAccountButton.TabIndex = 6;
            this.importOldAccountButton.Text = "Import settings";
            this.importOldAccountButton.UseVisualStyleBackColor = true;
            this.importOldAccountButton.Click += new System.EventHandler(this.ImportOldAccount_Click);
            //
            // cancelButton
            //
            this.cancelButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancelButton.Image = ((System.Drawing.Image)(resources.GetObject("cancelButton.Image")));
            this.cancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancelButton.Location = new System.Drawing.Point(384, 550);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(160, 80);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.saveButton.Image = ((System.Drawing.Image)(resources.GetObject("saveButton.Image")));
            this.saveButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.saveButton.Location = new System.Drawing.Point(598, 550);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(160, 80);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "Save";
            this.saveButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButton_Click);
            //
            // openAddressBookDialog
            //
            this.openAddressBookDialog.Filter = "Contacts Files(*.CSV;*.LDIF;*.VCF)|*.CSV;*.LDIF;*.VCF";
            //
            // openOldAccountFolderDialog
            //
            this.openOldAccountFolderDialog.Filter = "config.xml|config.xml";
            // 
            // configurationControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(237)))));
            this.Controls.Add(this.addressBookParamsGroupBox);
            this.Controls.Add(this.importOldAccountGroupBox);
            this.Controls.Add(this.advancedParamsGroupBox);
            this.Controls.Add(this.globalParamsGroupBox);
            this.Controls.Add(this.accountParamsGroupBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "configurationControl";
            this.Size = new System.Drawing.Size(1024, 606);
            this.inServerParamsGroupBox.ResumeLayout(false);
            this.inServerParamsGroupBox.PerformLayout();
            this.outServerParamsGroupBox.ResumeLayout(false);
            this.outServerParamsGroupBox.PerformLayout();
            this.loginParamsGroupBox.ResumeLayout(false);
            this.loginParamsGroupBox.PerformLayout();
            this.accountParamsGroupBox.ResumeLayout(false);
            this.defaultMsgParamsGroupBox.ResumeLayout(false);
            this.defaultMsgParamsGroupBox.PerformLayout();
            this.globalParamsGroupBox.ResumeLayout(false);
            this.globalParamsGroupBox.PerformLayout();
            this.advancedParamsGroupBox.ResumeLayout(false);
            this.advancedParamsGroupBox.PerformLayout();
            this.addressBookParamsGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label appLangLabel;
        private System.Windows.Forms.ComboBox appLangComboBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label appVoiceLabel;
        private System.Windows.Forms.ComboBox appVoiceComboBox;
        private System.Windows.Forms.CheckBox mainButtonSpeechPreviewCheckBox;
        private System.Windows.Forms.GroupBox inServerParamsGroupBox;
        private System.Windows.Forms.GroupBox outServerParamsGroupBox;
        private System.Windows.Forms.GroupBox loginParamsGroupBox;
        private System.Windows.Forms.TextBox accountPasswordTextBox;
        private System.Windows.Forms.Label accountPasswordLabel;
        private System.Windows.Forms.TextBox accountAddressTextBox;
        private System.Windows.Forms.Label accountAddressLabel;
        private System.Windows.Forms.TextBox inServerPortTextBox;
        private System.Windows.Forms.Label inServerPortLabel;
        private System.Windows.Forms.TextBox inServerAddressTextBox;
        private System.Windows.Forms.Label inServerAddressLabel;
        private System.Windows.Forms.TextBox accountNameTextBox;
        private System.Windows.Forms.Label accountNameLabel;
        private System.Windows.Forms.TextBox outServerAddressTextBox;
        private System.Windows.Forms.Label outServerPortLabel;
        private System.Windows.Forms.TextBox outServerPortTextBox;
        private System.Windows.Forms.Label outServerAddressLabel;
        private System.Windows.Forms.CheckBox imapSSLCheckbox;
        private System.Windows.Forms.Button testImapButton;
        private System.Windows.Forms.Button testSmtpButton;
        private System.Windows.Forms.GroupBox accountParamsGroupBox;
        private System.Windows.Forms.GroupBox globalParamsGroupBox;
        private System.Windows.Forms.CheckBox activateLogsCheckBox;
        private System.Windows.Forms.CheckBox activateTrashCheckBox;
        private System.Windows.Forms.GroupBox advancedParamsGroupBox;
        private System.Windows.Forms.GroupBox defaultMsgParamsGroupBox;
        private System.Windows.Forms.TextBox defaultMsgContentTextBox;
        private System.Windows.Forms.TextBox defaultMsgObjectTextBox;
        private System.Windows.Forms.Label defaultMsgContentLabel;
        private System.Windows.Forms.Label defaultMsgObjectLabel;
        private System.Windows.Forms.CheckBox smtpSSLCheckbox;
        private System.Windows.Forms.CheckBox activateDetailedLogsCheckBox;
        private System.Windows.Forms.Button importAddressBookButton;
        private System.Windows.Forms.Button importOldAccountButton;
        private System.Windows.Forms.GroupBox addressBookParamsGroupBox;
        private System.Windows.Forms.GroupBox importOldAccountGroupBox;
        private System.Windows.Forms.ComboBox appUpdateCheckFreqComboBox;
        private System.Windows.Forms.CheckBox activateUpdateCheckBox;
        private System.Windows.Forms.CheckBox activateUsageStatsCheckBox;
        private System.Windows.Forms.OpenFileDialog openAddressBookDialog;
        private System.Windows.Forms.OpenFileDialog openOldAccountFolderDialog;
    }
}
