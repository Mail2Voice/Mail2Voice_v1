﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows.Forms;
    using Mail2VoiceCore;

    public partial class ConfigurationControl : UserControl
    {
        /***************************
         *      Constructor
         ***************************/
        public ConfigurationControl()
        {
            this.InitializeComponent();
            SpeechObject.Singleton.AddVoiceString("connectionOK", "Connection to server successful.");
            SpeechObject.Singleton.AddVoiceString("connectionKO", "Connection to server failed.");
            SpeechObject.Singleton.AddVoiceString("contactsImportSuccess", "Contacts import succeeded.");
            SpeechObject.Singleton.AddVoiceString("contactsImportFailure", "Contacts import failed.");
            SpeechObject.Singleton.AddVoiceString("accountImportSuccess", "Settings import succeeded.");
            SpeechObject.Singleton.AddVoiceString("accountImportFailure", "Settings import failed.");
        }

        /***************************
         *      Public methods
         ***************************/
        public void Init()
        {
            this.LoadConfiguration();
        }

        public void UpdateDefaultMessage()
        {
            this.defaultMsgObjectTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_SUBJECT);
            this.defaultMsgContentTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_CONTENT);
        }

        /***************************
         *      Private methods
         ***************************/

        // Load current configuration
        private void LoadConfiguration()
        {
            // Setup languages drop-down list
            this.appLangComboBox.Items.Clear();
            foreach (string str in LanguageObject.Singleton.GetLanguagesList())
            {
                this.appLangComboBox.Items.Add(str.Substring(0, str.Length - 4));
            }
                
            this.appLangComboBox.SelectedItem = LanguageObject.Singleton.GetCurrentLanguage();

            // Voices list update
            this.appVoiceComboBox.Items.Clear();
            foreach (string str in SpeechObject.Singleton.GetVoicesList())
            {
                this.appVoiceComboBox.Items.Add(str);
            }
                
            this.appVoiceComboBox.SelectedItem = SpeechObject.Singleton.CurrentVoiceName;
            this.mainButtonSpeechPreviewCheckBox.Checked = Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_MAIN_BUTTONS_HOVER_SPEECH_PREVIEW));

            this.appUpdateCheckFreqComboBox.SelectedIndex = Convert.ToInt32(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_UPDATE_REPEAT));

            // Connexion parameters
            this.accountAddressTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN);
            this.accountPasswordTextBox.Text = Encryption.Singleton.EncryptString(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_PASSWORD), Constants.PASSWORD_KEY);

            this.inServerAddressTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_ADDRESS);
            this.inServerPortTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_PORT);

            this.outServerAddressTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_ADDRESS);
            this.outServerPortTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_PORT);
            this.accountNameTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_NAME);

            this.imapSSLCheckbox.Checked = Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_SSL));
            this.smtpSSLCheckbox.Checked = Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_SSL));

            this.activateLogsCheckBox.Checked = Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_LOG_ACTIVATE));
            this.activateTrashCheckBox.Checked = Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_ACTIVATE_TRASH));
            this.activateUpdateCheckBox.Checked = Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_UPDATE));
            this.activateUsageStatsCheckBox.Checked = Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_USAGE_STATS));

            this.defaultMsgObjectTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_SUBJECT);
            this.defaultMsgContentTextBox.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_CONTENT);
        }

        /********************************
         *      Private event handlers
         ********************************/   
        private void SaveButton_Click(object sender, System.EventArgs e)
        {
            bool init = false;
            if (this.accountAddressTextBox.Text != ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_LOGIN)
            || this.accountPasswordTextBox.Text != Encryption.Singleton.EncryptString(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_PASSWORD), Constants.PASSWORD_KEY)
            || this.inServerAddressTextBox.Text != ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_ADDRESS)
            || this.inServerPortTextBox.Text != ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_PORT)
            || this.imapSSLCheckbox.Checked != Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_IMAP_SSL)))
            {
                init = true;
            }

            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_TRANSLATION_LANG, this.appLangComboBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_LANG, this.appVoiceComboBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_MAIN_BUTTONS_HOVER_SPEECH_PREVIEW, (this.mainButtonSpeechPreviewCheckBox.Checked == true) ? Constants.TRUE : Constants.FALSE);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_EMAIL_LOGIN, this.accountAddressTextBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_EMAIL_PASSWORD, Encryption.Singleton.EncryptString(this.accountPasswordTextBox.Text, Constants.PASSWORD_KEY));
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_IMAP_ADDRESS, this.inServerAddressTextBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_IMAP_PORT, this.inServerPortTextBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_IMAP_SSL, (this.imapSSLCheckbox.Checked == true) ? Constants.TRUE : Constants.FALSE);

            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_SMTP_ADDRESS, this.outServerAddressTextBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_SMTP_PORT, this.outServerPortTextBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_SMTP_NAME, this.accountNameTextBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_SMTP_SSL, (this.smtpSSLCheckbox.Checked == true) ? Constants.TRUE : Constants.FALSE);

            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_LOG_ACTIVATE, (this.activateLogsCheckBox.Checked == true) ? Constants.TRUE : Constants.FALSE);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_EMAIL_ACTIVATE_TRASH, (this.activateTrashCheckBox.Checked == true) ? Constants.TRUE : Constants.FALSE);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_SEND_USAGE_STATS, (this.activateUsageStatsCheckBox.Checked == true) ? Constants.TRUE : Constants.FALSE);

            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_SEND_SUBJECT, this.defaultMsgObjectTextBox.Text);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_SEND_CONTENT, this.defaultMsgContentTextBox.Text);

            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_UPDATE, (this.activateUpdateCheckBox.Checked == true) ? Constants.TRUE : Constants.FALSE);
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_UPDATE_REPEAT, this.appUpdateCheckFreqComboBox.SelectedIndex.ToString());

            ConfigSettings.Singleton.SaveConfiguration();

            ((MainForm)this.Parent).UpdateConfiguration();
            ((MainForm)this.Parent).ForceDisplayReceptionPanel(init);
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            ((MainForm)this.Parent).ForceDisplayReceptionPanel(false);
        }

        private void TestImapButton_Click(object sender, System.EventArgs e)
        {
            int result = LibraryObject.Singleton.TestImapServer(
                this.inServerAddressTextBox.Text,
                Convert.ToInt32(this.inServerPortTextBox.Text),
                this.imapSSLCheckbox.Checked,
                this.accountAddressTextBox.Text,
                this.accountPasswordTextBox.Text);

            if (result == 0)
            {
                string tmp = SpeechObject.Singleton.GetVoiceString("connectionOK");
                SpeechObject.Singleton.Speak(tmp);
                MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                string tmp = SpeechObject.Singleton.GetVoiceString("connectionKO");
                SpeechObject.Singleton.Speak(tmp);
                MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TestSmtpButton_Click(object sender, EventArgs e)
        {
            int smtpPort = Convert.ToInt32(this.outServerPortTextBox.Text);
            bool result = LibraryObject.Singleton.TestSmtpServer(this.outServerAddressTextBox.Text, smtpPort);

            if (result)
            {
                string tmp = SpeechObject.Singleton.GetVoiceString("connectionOK");
                SpeechObject.Singleton.Speak(tmp);
                MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                string tmp = SpeechObject.Singleton.GetVoiceString("connectionKO");
                SpeechObject.Singleton.Speak(tmp);
                MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ImportAddressBookButton_Click(object sender, EventArgs ev)
        {
            string filePath = string.Empty;
            List<Contact> importedContacts = new List<Contact>();

            if (this.openAddressBookDialog.ShowDialog() == DialogResult.OK)
            {
                filePath = this.openAddressBookDialog.FileName;
            }
            else
            {
                return;
            }

            if (filePath.ToLower().EndsWith(".csv"))
            {
                ContactsParserCSV parser = new ContactsParserCSV();

                try
                {
                    importedContacts = parser.Parse(filePath);
                }
                catch (Exception e)
                {
                    LogObject.Singleton.Write("Failed to parse CSV file: " + e);
                }
            }
            else if (filePath.ToLower().EndsWith(".ldif"))
            {
                ContactParserLDIF parser = new ContactParserLDIF();

                try
                {
                    importedContacts = parser.Parse(filePath);
                }
                catch (Exception e)
                {
                    LogObject.Singleton.Write("Failed to parse LDIF file: " + e);
                }
            }
            else if (filePath.ToLower().EndsWith(".vcf"))
            {
                ContactParserVCARD parser = new ContactParserVCARD();

                try
                {
                    importedContacts = parser.Parse(filePath);
                }
                catch (Exception e)
                {
                    LogObject.Singleton.Write("Failed to parse vCard file: " + e);
                }
            }

            if (importedContacts.Count > 0)
            {
                ContactsObject.Singleton.Add(importedContacts);
                ContactsObject.Singleton.SaveToXml();
                ((MainForm)this.Parent).UpdateContacts();

                string messageFailed = SpeechObject.Singleton.GetVoiceString("contactsImportSuccess");
                SpeechObject.Singleton.Speak(messageFailed);
                MessageBox.Show(messageFailed);
            }
            else
            {
                string messageError = SpeechObject.Singleton.GetVoiceString("contactsImportFailure");
                SpeechObject.Singleton.Speak(messageError);
                MessageBox.Show(messageError);
            }
        }

        private void ImportOldAccount_Click(object sender, EventArgs ev)
        {
            string folderPath = string.Empty;

            if (this.openOldAccountFolderDialog.ShowDialog() == DialogResult.OK)
            {
                folderPath = this.openOldAccountFolderDialog.FileName.Replace("config.xml", string.Empty);
            }
            else
            {
                return;
            }

            MigrationModule migrationModule = new MigrationModule();
            if (migrationModule.ImportOldAccount(folderPath))
            {
                ((MainForm)this.Parent).Configure();
                this.LoadConfiguration();
                ((MainForm)this.Parent).UpdateConfiguration();
                ((MainForm)this.Parent).RefreshEmails(Constants.InboxView);
                ((MainForm)this.Parent).RefreshEmails(Constants.TrashView);

                string message = SpeechObject.Singleton.GetVoiceString("accountImportSuccess");
                SpeechObject.Singleton.Speak(message);
                MessageBox.Show(message);
            }
            else
            {
                string message = SpeechObject.Singleton.GetVoiceString("accountImportFailure");
                SpeechObject.Singleton.Speak(message);
                MessageBox.Show(message);
            }
        }

        private void AccountAddressTextBox_TextChanged(object sender, EventArgs e)
        {
            string currentAddress = this.accountAddressTextBox.Text;

            if (currentAddress.Contains("@"))
            {
                try
                {
                    string serverDomain = currentAddress.Split('@')[1];

                    if (serverDomain.Length > 0)
                    {
                        this.inServerAddressTextBox.Text = "imap." + serverDomain;
                        this.outServerAddressTextBox.Text = "smtp." + serverDomain;
                    }
                }
                catch (SystemException exception)
                {
                    LogObject.Singleton.Write("failed to parser mail server domain: " + exception);
                }
            }
        }

        private void ImapSSLCheckbox_Click(object sender, EventArgs e)
        {
            if (this.imapSSLCheckbox.Checked)
            {
                this.inServerPortTextBox.Text = Constants.VALUE_IMAP_PORT_SECURED;
            }
            else
            {
                this.inServerPortTextBox.Text = Constants.VALUE_IMAP_PORT_UNSECURED;
            }
        }

        private void SmtpSSLCheckbox_Click(object sender, EventArgs e)
        {
            if (this.smtpSSLCheckbox.Checked)
            {
                this.outServerPortTextBox.Text = Constants.VALUE_SMTP_PORT_SECURED;
            }
            else
            {
                this.outServerPortTextBox.Text = Constants.VALUE_SMTP_PORT_UNSECURED;
            }
        }

        private void LangComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ConfigSettings.Singleton.SetSettingValue(Constants.CONFIG_TRANSLATION_LANG, this.appLangComboBox.Text);
            LanguageObject.Singleton.SetCurrentLang(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_TRANSLATION_LANG));
            ((MainForm)this.Parent).TranslateApplication();
        }
    }
}
