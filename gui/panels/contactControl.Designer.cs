﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    partial class ContactControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel5 = new System.Windows.Forms.Panel();
            this.contactsListView = new System.Windows.Forms.ListView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.upButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.nameLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.confirmationDeleteContact = new System.Windows.Forms.Label();
            this.contactPhotoLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.openContactPhotoDialog = new System.Windows.Forms.OpenFileDialog();           
            this.contactDetailsPanel = new System.Windows.Forms.Panel();
            this.keyboardButton3 = new System.Windows.Forms.Button();
            this.keyboardButton1 = new System.Windows.Forms.Button();
            this.mailtoContactButton = new System.Windows.Forms.Button();
            this.contactsDetailsPanelTitleLabel = new System.Windows.Forms.Label();
            this.deleteContactButton = new System.Windows.Forms.Button();
            this.updateContactButton = new System.Windows.Forms.Button();
            this.addContactButton = new System.Windows.Forms.Button();
            this.changeContactPhotoButton = new System.Windows.Forms.Button();
            this.contactPhoto = new System.Windows.Forms.PictureBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.submitButton = new System.Windows.Forms.Button();
            this.panel5.SuspendLayout();
            this.contactDetailsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contactPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.AutoSize = true;
            this.panel5.Controls.Add(this.contactsListView);
            this.panel5.Controls.Add(this.upButton);
            this.panel5.Controls.Add(this.downButton);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel5.MinimumSize = new System.Drawing.Size(170, 606);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(550, 606);
            this.panel5.TabIndex = 13;
            // 
            // contactsListView
            // 
            this.contactsListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.contactsListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.contactsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contactsListView.FullRowSelect = true;
            this.contactsListView.GridLines = true;
            this.contactsListView.HideSelection = false;
            this.contactsListView.LargeImageList = this.imageList;
            this.contactsListView.Location = new System.Drawing.Point(0, 47);
            this.contactsListView.MultiSelect = false;
            this.contactsListView.Name = "contactsListView";
            this.contactsListView.Size = new System.Drawing.Size(550, 512);
            this.contactsListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.contactsListView.TabIndex = 4;
            this.contactsListView.TileSize = new System.Drawing.Size(101, 101);
            this.contactsListView.UseCompatibleStateImageBehavior = false;
            this.contactsListView.SelectedIndexChanged += new System.EventHandler(this.ContactsListView_SelectedIndexChanged);
            this.contactsListView.Click += new System.EventHandler(this.ContactsListView_Click);
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(100, 100);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // upButton
            // 
            this.upButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.upButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.upButton.Image = Mail2Voice.properties.Resources.arrow_up;
            this.upButton.Location = new System.Drawing.Point(0, 0);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(550, 47);
            this.upButton.TabIndex = 1;
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.UpButton_Click);
            // 
            // downButton
            // 
            this.downButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.downButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.downButton.Image = Mail2Voice.properties.Resources.arrow_dw;
            this.downButton.Location = new System.Drawing.Point(0, 559);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(550, 47);
            this.downButton.TabIndex = 0;
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.DownButton_Click);
            // 
            // nameLabel
            // 
            this.nameLabel.BackColor = System.Drawing.Color.Transparent;
            this.nameLabel.Location = new System.Drawing.Point(3, 277);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(85, 16);
            this.nameLabel.TabIndex = 17;
            this.nameLabel.Text = "Name:";
            this.nameLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // emailLabel
            // 
            this.emailLabel.BackColor = System.Drawing.Color.Transparent;
            this.emailLabel.Location = new System.Drawing.Point(3, 336);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(85, 16);
            this.emailLabel.TabIndex = 19;
            this.emailLabel.Text = "Email:";
            this.emailLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            // confirmationDeleteContact
            //
            this.confirmationDeleteContact.BackColor = System.Drawing.Color.Transparent;
            this.confirmationDeleteContact.Location = new System.Drawing.Point(3, 420);
            this.confirmationDeleteContact.Name = "confirmationDeleteContact";
            this.confirmationDeleteContact.Size = new System.Drawing.Size(350, 25);
            this.confirmationDeleteContact.TabIndex = 19;
            this.confirmationDeleteContact.Text = "Delete this contact?";
            this.confirmationDeleteContact.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.confirmationDeleteContact.ForeColor = System.Drawing.Color.Red;
            this.confirmationDeleteContact.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmationDeleteContact.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.confirmationDeleteContact.Visible = false;
            // 
            // contactPhotoLabel
            // 
            this.contactPhotoLabel.BackColor = System.Drawing.Color.Transparent;
            this.contactPhotoLabel.Location = new System.Drawing.Point(1, 111);
            this.contactPhotoLabel.Name = "contactPhotoLabel";
            this.contactPhotoLabel.Size = new System.Drawing.Size(85, 16);
            this.contactPhotoLabel.TabIndex = 21;
			this.contactPhotoLabel.Text = "Portrait:";
            this.contactPhotoLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // nameTextBox
            // 
            this.nameTextBox.BackColor = System.Drawing.Color.White;
            this.nameTextBox.Location = new System.Drawing.Point(89, 274);
            this.nameTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.ReadOnly = true;
            this.nameTextBox.Size = new System.Drawing.Size(251, 22);
            this.nameTextBox.TabIndex = 22;
            this.nameTextBox.Enabled = false;
            // 
            // emailTextBox
            // 
            this.emailTextBox.BackColor = System.Drawing.Color.White;
            this.emailTextBox.Location = new System.Drawing.Point(88, 333);
            this.emailTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.ReadOnly = true;
            this.emailTextBox.Size = new System.Drawing.Size(252, 22);
            this.emailTextBox.TabIndex = 24;
            this.emailTextBox.TextChanged += new System.EventHandler(this.EmailTextBox_TextChanged);
            this.emailTextBox.Enabled = false;
            // 
            // openContactPhotoDialog
            //
            this.openContactPhotoDialog.Filter = "Image Files(*.JPEG;*.JPG)|*.JPEG;*.JPG";
            // 
            // contactDetailsPanel
            // 
            this.contactDetailsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(237)))));
            this.contactDetailsPanel.Controls.Add(this.keyboardButton3);
            this.contactDetailsPanel.Controls.Add(this.keyboardButton1);
            this.contactDetailsPanel.Controls.Add(this.mailtoContactButton);
            this.contactDetailsPanel.Controls.Add(this.contactsDetailsPanelTitleLabel);
            this.contactDetailsPanel.Controls.Add(this.deleteContactButton);
            this.contactDetailsPanel.Controls.Add(this.updateContactButton);
            this.contactDetailsPanel.Controls.Add(this.addContactButton);
            this.contactDetailsPanel.Controls.Add(this.nameTextBox);
            this.contactDetailsPanel.Controls.Add(this.nameLabel);
            this.contactDetailsPanel.Controls.Add(this.changeContactPhotoButton);
            this.contactDetailsPanel.Controls.Add(this.emailLabel);
            this.contactDetailsPanel.Controls.Add(this.confirmationDeleteContact);
            this.contactDetailsPanel.Controls.Add(this.contactPhoto);
            this.contactDetailsPanel.Controls.Add(this.contactPhotoLabel);
            this.contactDetailsPanel.Controls.Add(this.emailTextBox);
            this.contactDetailsPanel.Controls.Add(this.cancelButton);
            this.contactDetailsPanel.Controls.Add(this.submitButton);
            this.contactDetailsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.contactDetailsPanel.Location = new System.Drawing.Point(550, 0);
            this.contactDetailsPanel.Name = "contactDetailsPanel";
            this.contactDetailsPanel.Size = new System.Drawing.Size(474, 606);
            this.contactDetailsPanel.TabIndex = 30;
            // 
            // keyboardButton3 // TODO: WHAT IS THIS ??
            // 
            this.keyboardButton3.Image = Mail2Voice.properties.Resources.keyboard_32;
            this.keyboardButton3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.keyboardButton3.Location = new System.Drawing.Point(346, 328);
            this.keyboardButton3.Name = "keyboardButton3";
            this.keyboardButton3.Size = new System.Drawing.Size(87, 33);
            this.keyboardButton3.TabIndex = 34;
			this.keyboardButton3.Text = "Keyboard";
            this.keyboardButton3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.keyboardButton3.UseVisualStyleBackColor = true;
            this.keyboardButton3.Click += new System.EventHandler(this.KeyboardButton3_Click);
            // 
            // keyboardButton1 // TODO: WHAT IS THIS ??
            // 
            this.keyboardButton1.Image = Mail2Voice.properties.Resources.keyboard_32;
            this.keyboardButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.keyboardButton1.Location = new System.Drawing.Point(346, 264);
            this.keyboardButton1.Name = "keyboardButton1";
            this.keyboardButton1.Size = new System.Drawing.Size(87, 33);
            this.keyboardButton1.TabIndex = 32;
			this.keyboardButton1.Text = "Keyboard";
            this.keyboardButton1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.keyboardButton1.UseVisualStyleBackColor = true;
            this.keyboardButton1.Click += new System.EventHandler(this.KeyboardButton1_Click);
            // 
            // mailtoContactButton
            // 
            this.mailtoContactButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mailtoContactButton.Enabled = false;
            this.mailtoContactButton.Image = Mail2Voice.properties.Resources.write;
            this.mailtoContactButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mailtoContactButton.Location = new System.Drawing.Point(368, 459);
            this.mailtoContactButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.mailtoContactButton.Name = "mailtoContactButton";
            this.mailtoContactButton.Size = new System.Drawing.Size(86, 86);
            this.mailtoContactButton.TabIndex = 31;
			this.mailtoContactButton.Text = "Write";
            this.mailtoContactButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mailtoContactButton.UseVisualStyleBackColor = true;
            this.mailtoContactButton.Click += new System.EventHandler(this.MailtoContactButton_Click);
            // 
            // contactsDetailsPanelTitleLabel
            // 
            //this.contactsDetailsPanelTitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.contactsDetailsPanelTitleLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactsDetailsPanelTitleLabel.Location = new System.Drawing.Point(0, 30);
            this.contactsDetailsPanelTitleLabel.Name = "contactsDetailsPanelTitleLabel";
            this.contactsDetailsPanelTitleLabel.Size = new System.Drawing.Size(474, 19);
            this.contactsDetailsPanelTitleLabel.TabIndex = 30;
			this.contactsDetailsPanelTitleLabel.Text = "Contact infos:";
            this.contactsDetailsPanelTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // deleteContactButton
            // 
            this.deleteContactButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteContactButton.Enabled = false;
            this.deleteContactButton.Image = Mail2Voice.properties.Resources.user_delete;
            this.deleteContactButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.deleteContactButton.Location = new System.Drawing.Point(254, 459);
            this.deleteContactButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.deleteContactButton.Name = "deleteContactButton";
            this.deleteContactButton.Size = new System.Drawing.Size(86, 86);
            this.deleteContactButton.TabIndex = 16;
			this.deleteContactButton.Text = "Delete";
            this.deleteContactButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.deleteContactButton.UseVisualStyleBackColor = true;
            this.deleteContactButton.Click += new System.EventHandler(this.DeleteContactButton_Click);
            // 
            // updateContactButton
            // 
            this.updateContactButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updateContactButton.Enabled = false;
            this.updateContactButton.Image = Mail2Voice.properties.Resources.user_edit;
            this.updateContactButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.updateContactButton.Location = new System.Drawing.Point(138, 459);
            this.updateContactButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.updateContactButton.Name = "updateContactButton";
            this.updateContactButton.Size = new System.Drawing.Size(86, 86);
            this.updateContactButton.TabIndex = 15;
			this.updateContactButton.Text = "Update";
            this.updateContactButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.updateContactButton.UseVisualStyleBackColor = true;
            this.updateContactButton.Click += new System.EventHandler(this.UpdateContactButton_Click);
            // 
            // addContactButton
            // 
            this.addContactButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addContactButton.Image = Mail2Voice.properties.Resources.user_new;
            this.addContactButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.addContactButton.Location = new System.Drawing.Point(16, 459);
            this.addContactButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.addContactButton.Name = "addContactButton";
            this.addContactButton.Size = new System.Drawing.Size(86, 86);
            this.addContactButton.TabIndex = 14;
			this.addContactButton.Text = "New";
            this.addContactButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addContactButton.UseVisualStyleBackColor = true;
            this.addContactButton.Click += new System.EventHandler(this.AddContactButton_Click);
            // 
            // changeContactPhotoButton
            // 
            this.changeContactPhotoButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeContactPhotoButton.Enabled = false;
            this.changeContactPhotoButton.Image = Mail2Voice.properties.Resources.user_new_32;
            this.changeContactPhotoButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.changeContactPhotoButton.Location = new System.Drawing.Point(222, 115);
            this.changeContactPhotoButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.changeContactPhotoButton.Name = "changeContactPhotoButton";
            this.changeContactPhotoButton.Size = new System.Drawing.Size(95, 43);
            this.changeContactPhotoButton.TabIndex = 27;
			this.changeContactPhotoButton.Text = "Change";
            this.changeContactPhotoButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.changeContactPhotoButton.UseVisualStyleBackColor = true;
            this.changeContactPhotoButton.Click += new System.EventHandler(this.ChangeContactPhotoButton_Click);
            // 
            // contactPhoto
            // 
            this.contactPhoto.BackColor = System.Drawing.Color.White;
            this.contactPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.contactPhoto.Location = new System.Drawing.Point(87, 115);
            this.contactPhoto.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.contactPhoto.Name = "contactPhoto";
            this.contactPhoto.Size = new System.Drawing.Size(117, 123);
            this.contactPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.contactPhoto.TabIndex = 26;
            this.contactPhoto.TabStop = false;
            // 
            // cancelButton
            // 
            this.cancelButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancelButton.Image = Mail2Voice.properties.Resources.no31;
            this.cancelButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cancelButton.Location = new System.Drawing.Point(72, 459);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(86, 86);
            this.cancelButton.TabIndex = 28;
			this.cancelButton.Text = "Cancel";
            this.cancelButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Visible = false;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // submitButton
            // 
            this.submitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.submitButton.Image = Mail2Voice.properties.Resources.ok3;
            this.submitButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.submitButton.Location = new System.Drawing.Point(193, 459);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(86, 86);
            this.submitButton.TabIndex = 29;
			this.submitButton.Text = "OK";
            this.submitButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Visible = false;
            this.submitButton.Click += new System.EventHandler(this.SubmitButtonButton_Click);
            // 
            // contactControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(237)))));
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.contactDetailsPanel);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "contactControl";
            this.Size = new System.Drawing.Size(1024, 606);
            this.panel5.ResumeLayout(false);
            this.contactDetailsPanel.ResumeLayout(false);
            this.contactDetailsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contactPhoto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button addContactButton;
        private System.Windows.Forms.Button updateContactButton;
        private System.Windows.Forms.Button deleteContactButton;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.Label confirmationDeleteContact;
        private System.Windows.Forms.Label contactPhotoLabel;
        private System.Windows.Forms.PictureBox contactPhoto;
        private System.Windows.Forms.Button changeContactPhotoButton;
        private System.Windows.Forms.OpenFileDialog openContactPhotoDialog;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.Panel contactDetailsPanel;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label contactsDetailsPanelTitleLabel;
        private System.Windows.Forms.ListView contactsListView;
        private System.Windows.Forms.Button mailtoContactButton;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Button keyboardButton1;
        private System.Windows.Forms.Button keyboardButton3;
        protected internal System.Windows.Forms.TextBox nameTextBox;
        protected internal System.Windows.Forms.TextBox emailTextBox;
    }
}
