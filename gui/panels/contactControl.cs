﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;
    using Mail2VoiceCore;

    public partial class ContactControl : UserControl
    {
        private enum ModeContact
        {
            NONE,
            ADD,
            UPDATE,
            DELETE
        }

        /***************************
         *      Attributes
         ***************************/
        private string oldEmail = string.Empty;
        private ModeContact mode = ModeContact.NONE;
        bool updateSendPanel = false;
        SendControl sendPanel = null;

        /***************************
         *      Constructor
         ***************************/
        public ContactControl(SendControl sendPanel)
        {
            this.InitializeComponent();

            SpeechObject.Singleton.AddVoiceString("emailAddError", "You must specify an email to add this contact.");
            SpeechObject.Singleton.AddVoiceString("existsAddError", "Cannot add the contact, email already used for another one.");
            SpeechObject.Singleton.AddVoiceString("emailUpdateError", "You must specify an email to edit this contact.");
            SpeechObject.Singleton.AddVoiceString("existsUpdateError", "Cannot edit the contact, email already used for another one.");
            SpeechObject.Singleton.AddVoiceString("emailNotValid", "The email address is not valid.");
            SpeechObject.Singleton.AddVoiceString("contactInUseWarning", "This contact is in use in Send panel, do you want to edit it anyway?");
            SpeechObject.Singleton.AddVoiceString("confirmationDeleteContact", "Delete this contact?");

            this.UpdateContent();

            this.keyboardButton1.Visible = false;
            this.keyboardButton3.Visible = false;
            
            if (Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_ACTIVATE_KEYBOARD)))
            {
                this.keyboardButton1.Visible = true;
                this.keyboardButton3.Visible = true;
            }

            this.sendPanel = sendPanel;
        }

        /***************************
         *      Public methods
         ***************************/
        public void PreFillContact(string email, string nom)
        {
            this.AddContactButton_Click(null, null);
            this.nameTextBox.Text = nom;
            this.emailTextBox.Text = email;
        }

        public void UpdateContent()
        {
            try
            {
                this.contactsListView.Items.Clear();
                this.imageList.Images.Clear();
                List<Contact> contactsList = ContactsObject.Singleton.GetContactsList();

                if (contactsList != null)
                {
                    for (int x = 0; x < contactsList.Count; x++)
                    {
                        this.imageList.Images.Add(contactsList[x].Email, contactsList[x].Image == null ? Mail2Voice.properties.Resources.user_known : contactsList[x].Image);
                        this.contactsListView.Items.Add(new ListViewItem(contactsList[x].Name, contactsList[x].Email) { Tag = contactsList[x].Email });
                    }
                }

                this.emailTextBox.Clear();
                this.nameTextBox.Clear();
                this.contactPhoto.Image = null;
                this.addContactButton.Visible = this.updateContactButton.Visible = this.deleteContactButton.Visible = true;
                this.mailtoContactButton.Visible = true;
                this.submitButton.Visible = this.cancelButton.Visible = false;
                this.upButton.Enabled = this.downButton.Enabled = this.contactsListView.Enabled = true;
                this.nameTextBox.ReadOnly = this.emailTextBox.ReadOnly = true;
                this.changeContactPhotoButton.Enabled = false;
                this.confirmationDeleteContact.Visible = false;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        /********************************
         *      Protected event handlers
         ********************************/   
        protected internal void AddContactButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.mode = ModeContact.ADD;
                this.addContactButton.Visible = this.updateContactButton.Visible = this.deleteContactButton.Visible = false;
                this.submitButton.Visible = this.cancelButton.Visible = true;
                this.mailtoContactButton.Visible = false;
                this.upButton.Enabled = this.downButton.Enabled = this.contactsListView.Enabled = false;
                this.nameTextBox.ReadOnly = this.emailTextBox.ReadOnly = false;
                this.nameTextBox.Text = this.emailTextBox.Text = string.Empty;
                this.nameTextBox.Enabled = this.emailTextBox.Enabled = true;
                this.changeContactPhotoButton.Enabled = true;
                this.contactPhoto.Image = Mail2Voice.properties.Resources.user_known;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void UpdateContactButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.updateSendPanel = false;
                if (this.sendPanel.ContactInUse(this.emailTextBox.Text))
                {
                    string warningMessage = SpeechObject.Singleton.GetVoiceString("contactInUseWarning");
                    SpeechObject.Singleton.Speak(warningMessage);
                    DialogResult result = MessageBox.Show(warningMessage, string.Empty, MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        this.updateSendPanel = true;
                    }
                    else
                    {
                        return;
                    }
                }

                this.mode = ModeContact.UPDATE;
                this.addContactButton.Visible = this.updateContactButton.Visible = this.deleteContactButton.Visible = false;
                this.mailtoContactButton.Visible = false;
                this.submitButton.Visible = this.cancelButton.Visible = true;
                this.upButton.Enabled = this.downButton.Enabled = this.contactsListView.Enabled = false;
                this.nameTextBox.ReadOnly = this.emailTextBox.ReadOnly = false;
                this.changeContactPhotoButton.Enabled = true;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void DeleteContactButton_Click(object sender, EventArgs e)
        {
            try
            {
                string warningMessage = SpeechObject.Singleton.GetVoiceString("confirmationDeleteContact");
                SpeechObject.Singleton.Speak(warningMessage);
                this.mode = ModeContact.DELETE;
                this.addContactButton.Visible = this.updateContactButton.Visible = this.deleteContactButton.Visible = false;
                this.mailtoContactButton.Visible = false;
                this.submitButton.Visible = this.cancelButton.Visible = true;
                this.upButton.Enabled = this.downButton.Enabled = this.contactsListView.Enabled = false;
                this.confirmationDeleteContact.Visible = true;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void ChangeContactPhotoButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.openContactPhotoDialog.InitialDirectory = Path.GetDirectoryName(Application.ExecutablePath);
                if (this.openContactPhotoDialog.ShowDialog() == DialogResult.OK)
                {
                    Image img = Image.FromFile(this.openContactPhotoDialog.FileName);

                    this.contactPhoto.Image = FileObject.Singleton.CropImage(img);
                    this.contactPhoto.Tag = this.openContactPhotoDialog.FileName;

                    img.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void UpButton_Click(object sender, EventArgs e)
        {
            try
            {
                int pos = this.GetSelectedIndex();
                int index = pos;
                if (pos == -1)
                {
                    index = 0;
                }
                else if (pos - 1 >= 0)
                {
                    index = pos - 1;
                }

                this.contactsListView.Items[index].Selected = true;
                this.contactsListView.Items[index].Focused = true;
                this.contactsListView.Items[index].EnsureVisible();
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void DownButton_Click(object sender, EventArgs e)
        {
            try
            {
                int pos = this.GetSelectedIndex();
                int index = pos;

                if (pos == -1)
                {
                    index = 0;
                }
                else if (pos + 1 < this.contactsListView.Items.Count)
                {
                    index = pos + 1;
                }

                this.contactsListView.Items[index].Selected = true;
                this.contactsListView.Items[index].Focused = true;
                this.contactsListView.Items[index].EnsureVisible();
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void CancelButton_Click(object sender, EventArgs e)
        {
            if (this.mode != ModeContact.UPDATE)
            {
                this.nameTextBox.Text = this.emailTextBox.Text = string.Empty;
                this.contactPhoto.Image = Mail2Voice.properties.Resources.user_unknow;
            }

            this.addContactButton.Visible = this.updateContactButton.Visible = this.deleteContactButton.Visible = true;
            this.mailtoContactButton.Visible = true;
            this.submitButton.Visible = this.cancelButton.Visible = false;
            this.upButton.Enabled = this.downButton.Enabled = this.contactsListView.Enabled = true;
            this.changeContactPhotoButton.Enabled = false;
            this.confirmationDeleteContact.Visible = false;
            this.nameTextBox.ReadOnly = this.emailTextBox.ReadOnly = true;
            this.mode = ModeContact.NONE;
            this.ContactsListView_SelectedIndexChanged(null, null);
            this.updateSendPanel = false;
        }

        protected internal void SubmitButtonButton_Click(object sender, EventArgs e)
        {
            if (this.mode == ModeContact.ADD)
            {
                if (string.IsNullOrEmpty(this.emailTextBox.Text))
                {
                    string messageError = SpeechObject.Singleton.GetVoiceString("emailAddError");
                    SpeechObject.Singleton.Speak(messageError);
                    MessageBox.Show(messageError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (!LibraryObject.Singleton.IsValidEmail(this.emailTextBox.Text))
                {
                    string messageError = SpeechObject.Singleton.GetVoiceString("emailNotValid");
                    SpeechObject.Singleton.Speak(messageError);
                    MessageBox.Show(messageError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (ContactsObject.Singleton.ContactExists(this.emailTextBox.Text))
                {
                    string messageError = SpeechObject.Singleton.GetVoiceString("existsAddError");
                    SpeechObject.Singleton.Speak(messageError);
                    MessageBox.Show(messageError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                ContactsObject.Singleton.Add(new Contact()
                {
                    Email = this.emailTextBox.Text,
                    Name = this.nameTextBox.Text,
                    Image = this.contactPhoto.Image
                });

                this.emailTextBox.Enabled = this.nameTextBox.Enabled = false;

                ((MainForm)this.Parent).UpdateContacts();
            }
            else if (this.mode == ModeContact.UPDATE)
            {
                if (string.IsNullOrEmpty(this.emailTextBox.Text))
                {
                    string messageError = SpeechObject.Singleton.GetVoiceString("emailUpdateError");
                    SpeechObject.Singleton.Speak(messageError);
                    MessageBox.Show(messageError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (this.emailTextBox.Text != this.oldEmail)
                {
                    if (ContactsObject.Singleton.ContactExists(this.emailTextBox.Text))
                    {
                        string messageError = SpeechObject.Singleton.GetVoiceString("existsUpdateError");
                        SpeechObject.Singleton.Speak(messageError);
                        MessageBox.Show(messageError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                if (!LibraryObject.Singleton.IsValidEmail(this.emailTextBox.Text))
                {
                    string messageError = SpeechObject.Singleton.GetVoiceString("emailNotValid");
                    SpeechObject.Singleton.Speak(messageError);
                    MessageBox.Show(messageError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Contact newContact = new Contact()
                {
                    Email = this.emailTextBox.Text,
                    Name = this.nameTextBox.Text,
                    Image = this.contactPhoto.Image
                };

                ContactsObject.Singleton.Update(newContact, this.oldEmail);
                ((MainForm)this.Parent).UpdateContacts();

                if (this.updateSendPanel)
                {
                    this.sendPanel.UpdateCurrentContact(newContact);
                    this.updateSendPanel = false;
                }
            }
            else if (this.mode == ModeContact.DELETE)
            {
                ContactsObject.Singleton.Delete(this.oldEmail);
                ((MainForm)this.Parent).UpdateContacts();
            }

            ContactsObject.Singleton.SaveToXml();

            this.contactPhoto.Image = null;

            this.addContactButton.Visible = this.updateContactButton.Visible = this.deleteContactButton.Visible = true;
            this.mailtoContactButton.Visible = true;
            this.submitButton.Visible = this.cancelButton.Visible = false;
            this.upButton.Enabled = this.downButton.Enabled = this.contactsListView.Enabled = true;
            this.changeContactPhotoButton.Enabled = false;
            this.confirmationDeleteContact.Visible = false;
            this.nameTextBox.ReadOnly = this.emailTextBox.ReadOnly = true;
            this.updateContactButton.Enabled = this.deleteContactButton.Enabled = false;
            this.mode = ModeContact.NONE;
        }

        protected internal void MailtoContactButton_Click(object sender, EventArgs e)
        {
            ((MainForm)this.Parent).SendEmail(this.emailTextBox.Text);
        }

        private void ContactsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.contactsListView.SelectedItems.Count > 0)
                {
                    if (this.contactsListView.SelectedItems[0].Tag.ToString() != null)
                    {
                        Contact contact = ContactsObject.Singleton.GetContact(this.contactsListView.SelectedItems[0].Tag.ToString());
                        if (contact != null)
                        {
                            // Announce selected contact
                            SpeechObject.Singleton.Speak(contact.Name);

                            this.oldEmail = contact.Email;
                            this.nameTextBox.Text = contact.Name;
                            this.emailTextBox.Text = contact.Email;
                            if (contact.Image != null)
                            {
                                this.contactPhoto.Image = contact.Image;
                            }
                            else
                            {
                                this.contactPhoto.Image = Mail2Voice.properties.Resources.user_known;
                            }

                            this.nameTextBox.Enabled = true;
                            this.emailTextBox.Enabled = true;
                        }
                    }
                }
                else
                {
                    this.nameTextBox.Text = string.Empty;
                    this.emailTextBox.Text = string.Empty;

                    this.updateContactButton.Enabled = false;
                    this.deleteContactButton.Enabled = false;
                    this.mailtoContactButton.Enabled = false;
                    this.nameTextBox.Enabled = false;
                    this.emailTextBox.Enabled = false;

                    this.contactPhoto.Image = null;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        /***************************
         *      Private methods
         ***************************/
        private int GetSelectedIndex()
        {
            // TODO: use foreach loop instead
            for (int x = 0; x < this.contactsListView.Items.Count; x++)
            {
                if (this.contactsListView.Items[x].Selected)
                {
                    return x;
                }
            }

            return -1;
        }
            
        /********************************
         *      Private event handlers
         ********************************/   
        private void EmailTextBox_TextChanged(object sender, EventArgs e)
        {
            this.mailtoContactButton.Enabled = !string.IsNullOrEmpty(this.emailTextBox.Text);
            this.updateContactButton.Enabled = !string.IsNullOrEmpty(this.emailTextBox.Text);
            this.deleteContactButton.Enabled = !string.IsNullOrEmpty(this.emailTextBox.Text);
        }

        private void ContactsListView_Click(object sender, EventArgs e)
        {
            this.ContactsListView_SelectedIndexChanged(sender, null);
        }

        private void KeyboardButton1_Click(object sender, EventArgs e)
        {
            ((MainForm)this.Parent).ShowHideKeyboard(true, this.panel5, this.nameTextBox);
        }

        private void KeyboardButton3_Click(object sender, EventArgs e)
        {
            ((MainForm)this.Parent).ShowHideKeyboard(true, this.panel5, this.emailTextBox);
        }
    }
}
