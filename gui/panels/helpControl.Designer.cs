﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using System;
    partial class HelpControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.otherHelpButtonsPanel = new System.Windows.Forms.Panel();
            this.helpLoadingImage = new System.Windows.Forms.PictureBox();
            this.goToWebsiteButton = new System.Windows.Forms.Button();
            this.loadHelpIndexButton = new System.Windows.Forms.Button();
            this.aboutButton = new System.Windows.Forms.Button();
            this.authorsLabel = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.version = new System.Windows.Forms.Label();
            this.licenseLabel = new System.Windows.Forms.Label();
            this.licenseUrl = new System.Windows.Forms.LinkLabel();
            this.websiteUrlLabel = new System.Windows.Forms.Label();
            this.aboutPanel = new System.Windows.Forms.Panel();
            this.releaseDate = new System.Windows.Forms.Label();
            this.releaseDateLabel = new System.Windows.Forms.Label();
            this.authors = new System.Windows.Forms.Label();
            this.websiteUrl = new System.Windows.Forms.LinkLabel();
            this.appLogoContainer = new System.Windows.Forms.PictureBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.otherHelpButtonsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helpLoadingImage)).BeginInit();
            this.aboutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.appLogoContainer)).BeginInit();
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 63);
            this.webBrowser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.webBrowser.MinimumSize = new System.Drawing.Size(23, 25);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(1195, 683);
            this.webBrowser.TabIndex = 0;
            this.webBrowser.AllowWebBrowserDrop = false;
            this.webBrowser.ScriptErrorsSuppressed = true;
            this.webBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.WebBrowser_DocumentCompleted);
            // 
            // otherHelpButtonsPanel
            // 
            this.otherHelpButtonsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(237)))));
            this.otherHelpButtonsPanel.Controls.Add(this.helpLoadingImage);
            this.otherHelpButtonsPanel.Controls.Add(this.goToWebsiteButton);
            this.otherHelpButtonsPanel.Controls.Add(this.loadHelpIndexButton);
            this.otherHelpButtonsPanel.Controls.Add(this.aboutButton);
            this.otherHelpButtonsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.otherHelpButtonsPanel.Location = new System.Drawing.Point(0, 0);
            this.otherHelpButtonsPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.otherHelpButtonsPanel.Name = "otherHelpButtonsPanel";
            this.otherHelpButtonsPanel.Size = new System.Drawing.Size(1195, 63);
            this.otherHelpButtonsPanel.TabIndex = 1;
            // 
            // helpLoadingImage
            // 
            this.helpLoadingImage.Image = Mail2Voice.properties.Resources.working_48_B;
            this.helpLoadingImage.Location = new System.Drawing.Point(573, 7);
            this.helpLoadingImage.Name = "helpLoadingImage";
            this.helpLoadingImage.Size = new System.Drawing.Size(48, 48);
            this.helpLoadingImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.helpLoadingImage.TabIndex = 3;
            this.helpLoadingImage.TabStop = false;
            this.helpLoadingImage.Visible = false;
            // 
            // goToWebsiteButton
            // 
            this.goToWebsiteButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.goToWebsiteButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.goToWebsiteButton.Image = Mail2Voice.properties.Resources.url2;
            this.goToWebsiteButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.goToWebsiteButton.Location = new System.Drawing.Point(923, 0);
            this.goToWebsiteButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.goToWebsiteButton.Name = "goToWebsiteButton";
            this.goToWebsiteButton.Size = new System.Drawing.Size(165, 63);
            this.goToWebsiteButton.TabIndex = 1;
			this.goToWebsiteButton.Text = "Mail2Voice website";
            this.goToWebsiteButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.goToWebsiteButton.UseVisualStyleBackColor = true;
            this.goToWebsiteButton.Click += new System.EventHandler(this.GoToWebsiteButton_Click);
            // 
            // loadHelpIndexButton
            // 
            this.loadHelpIndexButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loadHelpIndexButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.loadHelpIndexButton.Image = Mail2Voice.properties.Resources.help3;
            this.loadHelpIndexButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.loadHelpIndexButton.Location = new System.Drawing.Point(0, 0);
            this.loadHelpIndexButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.loadHelpIndexButton.Name = "loadHelpIndexButton";
            this.loadHelpIndexButton.Size = new System.Drawing.Size(169, 63);
            this.loadHelpIndexButton.TabIndex = 0;
			this.loadHelpIndexButton.Text = "Help on Mail2Voice";
            this.loadHelpIndexButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.loadHelpIndexButton.UseVisualStyleBackColor = true;
            this.loadHelpIndexButton.Click += new System.EventHandler(this.LoadHelpIndexButton_Click);
            // 
            // aboutButton
            // 
            this.aboutButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aboutButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.aboutButton.Image = Mail2Voice.properties.Resources.help4;
            this.aboutButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.aboutButton.Location = new System.Drawing.Point(1088, 0);
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Size = new System.Drawing.Size(107, 63);
            this.aboutButton.TabIndex = 2;
			this.aboutButton.Text = "About";
            this.aboutButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.aboutButton.UseVisualStyleBackColor = true;
            this.aboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // authorsLabel
            // 
            this.authorsLabel.AutoSize = true;
            this.authorsLabel.Location = new System.Drawing.Point(100, 121);
            this.authorsLabel.Name = "authorsLabel";
            this.authorsLabel.Size = new System.Drawing.Size(100, 16);
            this.authorsLabel.TabIndex = 2;
			this.authorsLabel.Text = "Authors:";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Location = new System.Drawing.Point(100, 200);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(100, 16);
            this.versionLabel.TabIndex = 5;
            this.versionLabel.Text = "Version:";
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.Location = new System.Drawing.Point(200, 200);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(15, 16);
            this.version.TabIndex = 6;
            // 
            // licenseLabel
            // 
            this.licenseLabel.AutoSize = true;
            this.licenseLabel.Location = new System.Drawing.Point(100, 240);
            this.licenseLabel.Name = "licenseLabel";
            this.licenseLabel.Size = new System.Drawing.Size(100, 16);
            this.licenseLabel.TabIndex = 7;
			this.licenseLabel.Text = "License:";
            // 
            // licenseUrl
            // 
            this.licenseUrl.AutoSize = true;
            this.licenseUrl.Location = new System.Drawing.Point(200, 240);
            this.licenseUrl.Name = "licenseUrl";
            this.licenseUrl.Size = new System.Drawing.Size(127, 16);
            this.licenseUrl.TabIndex = 8;
            this.licenseUrl.TabStop = true;
            this.licenseUrl.Tag = "http://www.gnu.org/copyleft/gpl.html";
			this.licenseUrl.Text = "GNU GPL v3 license";
            this.licenseUrl.Click += new System.EventHandler(this.WebsiteUrl_Click);

            // 
            // websiteUrlLabel
            // 
            this.websiteUrlLabel.AutoSize = true;
            this.websiteUrlLabel.Location = new System.Drawing.Point(100, 260);
            this.websiteUrlLabel.Name = "websiteUrlLabel";
            this.websiteUrlLabel.Size = new System.Drawing.Size(100, 16);
            this.websiteUrlLabel.TabIndex = 9;
			this.websiteUrlLabel.Text = "Website:";
            // 
            // aboutPanel
            // 
            this.aboutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(234)))), ((int)(((byte)(219)))));
            this.aboutPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.aboutPanel.Controls.Add(this.releaseDate);
            this.aboutPanel.Controls.Add(this.releaseDateLabel);
            this.aboutPanel.Controls.Add(this.authors);
            this.aboutPanel.Controls.Add(this.websiteUrl);
            this.aboutPanel.Controls.Add(this.websiteUrlLabel);
            this.aboutPanel.Controls.Add(this.licenseUrl);
            this.aboutPanel.Controls.Add(this.licenseLabel);
            this.aboutPanel.Controls.Add(this.version);
            this.aboutPanel.Controls.Add(this.versionLabel);
            this.aboutPanel.Controls.Add(this.authorsLabel);
            this.aboutPanel.Controls.Add(this.appLogoContainer);
            this.aboutPanel.Controls.Add(this.closeButton);
            this.aboutPanel.Location = new System.Drawing.Point(315, 111);
            this.aboutPanel.Name = "aboutPanel";
            this.aboutPanel.Size = new System.Drawing.Size(564, 408);
            this.aboutPanel.TabIndex = 2;
            this.aboutPanel.Visible = false;
            this.aboutPanel.Resize += new System.EventHandler(this.AboutPanel_Resize);
            // 
            // releaseDate
            // 
            this.releaseDate.AutoSize = true;
            this.releaseDate.Location = new System.Drawing.Point(200, 220);
            this.releaseDate.Name = "releaseDate";
            this.releaseDate.Size = new System.Drawing.Size(71, 16);
            this.releaseDate.TabIndex = 14;
            // 
            // releaseDateLabel
            // 
            this.releaseDateLabel.AutoSize = true;
            this.releaseDateLabel.Location = new System.Drawing.Point(100, 220);
            this.releaseDateLabel.Name = "releaseDateLabel";
            this.releaseDateLabel.Size = new System.Drawing.Size(100, 16);
            this.releaseDateLabel.TabIndex = 13;
			this.releaseDateLabel.Text = "Release date:";
            // 
            // authors
            // 
            this.authors.AutoSize = true;
            this.authors.Location = new System.Drawing.Point(200, 121);
            this.authors.Name = "authors";
            this.authors.Size = new System.Drawing.Size(245, 16);
            this.authors.TabIndex = 11;
            this.authors.Tag = "";

            // 
            // websiteUrl
            // 
            this.websiteUrl.AutoSize = true;
            this.websiteUrl.Location = new System.Drawing.Point(200, 260);
            this.websiteUrl.Name = "websiteUrl";
            this.websiteUrl.Size = new System.Drawing.Size(123, 16);
            this.websiteUrl.TabIndex = 10;
            this.websiteUrl.TabStop = true;
            this.websiteUrl.Tag = "http://www.mail2voice.org/en";
            this.websiteUrl.Text = "http://www.mail2voice.org/en";
            this.websiteUrl.Click += new System.EventHandler(this.WebsiteUrl_Click);
            // 
            // appLogoContainer
            // 
            this.appLogoContainer.BackColor = System.Drawing.Color.White;
            this.appLogoContainer.Image = Mail2Voice.properties.Resources.logomail2voice_322_100;
            this.appLogoContainer.Location = new System.Drawing.Point(127, 3);
            this.appLogoContainer.Name = "appLogoContainer";
            this.appLogoContainer.Size = new System.Drawing.Size(322, 100);
            this.appLogoContainer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.appLogoContainer.TabIndex = 1;
            this.appLogoContainer.TabStop = false;
            // 
            // closeButton
            // 
            this.closeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeButton.Image = Mail2Voice.properties.Resources.ok1;
            this.closeButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.closeButton.Location = new System.Drawing.Point(229, 348);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(101, 42);
            this.closeButton.TabIndex = 0;
			this.closeButton.Text = "Close";
            this.closeButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // helpControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.aboutPanel);
            this.Controls.Add(this.webBrowser);
            this.Controls.Add(this.otherHelpButtonsPanel);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "helpControl";
            this.Size = new System.Drawing.Size(1195, 746);
            this.otherHelpButtonsPanel.ResumeLayout(false);
            this.otherHelpButtonsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helpLoadingImage)).EndInit();
            this.aboutPanel.ResumeLayout(false);
            this.aboutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.appLogoContainer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Panel otherHelpButtonsPanel;
        private System.Windows.Forms.Button loadHelpIndexButton;
        private System.Windows.Forms.Button goToWebsiteButton;
        private System.Windows.Forms.Button aboutButton;
        private System.Windows.Forms.PictureBox helpLoadingImage;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.PictureBox appLogoContainer;
        private System.Windows.Forms.Label authorsLabel;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Label version;
        private System.Windows.Forms.Label licenseLabel;
        private System.Windows.Forms.LinkLabel licenseUrl;
        private System.Windows.Forms.Label websiteUrlLabel;
        private System.Windows.Forms.Panel aboutPanel;
        private System.Windows.Forms.LinkLabel websiteUrl;
        private System.Windows.Forms.Label authors;
        private System.Windows.Forms.Label releaseDate;
        private System.Windows.Forms.Label releaseDateLabel;
    }
}
