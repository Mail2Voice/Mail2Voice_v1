﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using System;
    using System.Diagnostics;
    using System.Windows.Forms;
    using Mail2VoiceCore;

    public partial class HelpControl : UserControl
    {
        /***************************
         *      Constructor
         ***************************/
        public HelpControl()
        {
            this.InitializeComponent();

            this.version.Text = Application.ProductVersion;
            
            // TODO: is this reliable?
            this.releaseDate.Text = FileObject.Singleton.GetFileCreationTime(Application.ExecutablePath);

            string[] authors = { "Olivier \"oliversleep\" Villedieu", "Laurent \"lclaude\" Claude", "Matthieu \"mhatz\" Hazon", "Diana \"idiana\" Ibanescu" };
            this.authors.Text = string.Join("\n", authors);
        }

        /***************************
         *      Public methods
         ***************************/
        public void OpenHelpPage()
        {
            this.webBrowser.Navigate(AppDomain.CurrentDomain.BaseDirectory + "docs/1.1/aide-v1-1.html");
        }

        /********************************
         *      Protected event handlers
         ********************************/   
        protected internal void LoadHelpIndexButton_Click(object sender, System.EventArgs e)
        {
            this.ShowLoading();
            this.OpenHelpPage();
        }

        protected internal void GoToWebsiteButton_Click(object sender, System.EventArgs e)
        {
            this.ShowLoading();
            this.webBrowser.Navigate(@"http://www.mail2voice.org");
        }

        protected internal void AboutButton_Click(object sender, System.EventArgs e)
        {
            this.loadHelpIndexButton.Enabled = this.goToWebsiteButton.Enabled = this.aboutButton.Enabled = false;
            this.aboutPanel.Left = (this.Width / 2) - (this.aboutPanel.Width / 2);
            this.aboutPanel.Top = (this.Height / 2) - (this.aboutPanel.Height / 2);
            this.aboutPanel.Show();
        }

        protected internal void CloseButton_Click(object sender, System.EventArgs e)
        {
            this.loadHelpIndexButton.Enabled = this.goToWebsiteButton.Enabled = this.aboutButton.Enabled = true;
            this.aboutPanel.Hide();
        }

        /***************************
         *      Private methods
         ***************************/
        private void ShowLoading()
        {
            this.helpLoadingImage.Left = (this.otherHelpButtonsPanel.Width / 2) - (this.helpLoadingImage.Width / 2);
            this.helpLoadingImage.Top = (this.otherHelpButtonsPanel.Height / 2) - (this.helpLoadingImage.Height / 2);
            this.helpLoadingImage.Show();
        }

        /********************************
         *      Private event handlers
         ********************************/   
        private void AboutPanel_Resize(object sender, System.EventArgs e)
        {
            this.helpLoadingImage.Left = (this.otherHelpButtonsPanel.Width / 2) - (this.helpLoadingImage.Width / 2);
            this.helpLoadingImage.Top = (this.otherHelpButtonsPanel.Height / 2) - (this.helpLoadingImage.Height / 2);
            this.aboutPanel.Left = (this.Width / 2) - (this.aboutPanel.Width / 2);
            this.aboutPanel.Top = (this.Height / 2) - (this.aboutPanel.Height / 2);
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this.helpLoadingImage.Hide();
        }

        private void WebsiteUrl_Click(object sender, System.EventArgs e)
        {
            Process.Start(((LinkLabel)sender).Tag.ToString());
        }
    }
}
