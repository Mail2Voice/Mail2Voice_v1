﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml.Linq;
    using Mail2VoiceCore;

    public partial class KeyboardControl : UserControl
    {
        /***************************
         *      Attributes
         ***************************/
        private Control sentText = null;

        /***************************
         *      Constructor
         ***************************/
        public KeyboardControl()
        {
            this.InitializeComponent();
            
            // TODO: rename panels
            this.panel1.Top = 0;
            this.panel1.Left = 0;
            this.panel1.Width = this.Width;
            this.panel2.Top = this.panel1.Height;
            this.panel2.Left = 0;
            this.panel2.Width = this.Width;

            this.LoadKeyboard();
        }

        /***************************
         *      Public methods
         ***************************/
        public void SetSentText(Control text)
        {
            this.sentText = text;
            this.textBoxPreview.Text = ((TextBox)text).Text;
        }

        /********************************
         *      Public event handlers
         ********************************/
         
        // TODO: rename to KeyboardButton_Click 
        public void KeyboardBouton_Click(object sender, EventArgs e)
        {
            string keyboard = ((Button)sender).Tag.ToString();

            // We iterate through panels
            foreach (Control p in this.panel2.Controls)
            {
                if (p is Panel)
                {
                    if (((Panel)p).Tag != null)
                    {
                        string text = ((Panel)p).Tag.ToString();
                        if (text == keyboard)
                        {
                            p.Show();
                        }
                        else
                        {
                            p.Hide();
                        }
                    }
                }
            }
        }

        public void CharacterButton_Click(object sender, EventArgs e)
        {
            string text = ((Button)sender).Tag.ToString().Split('#').ToList()[1];
            if (text == "SPACE")
            {
                text = " ";
            }

            if (text == "ENTER") 
            {
                text = Environment.NewLine;
            }

            if (text == "ERASE") 
            {
                if (this.textBoxPreview.Text.Length > 0)
                {
                    text = this.textBoxPreview.Text.Substring(0, this.textBoxPreview.Text.Length - 1);
                    this.textBoxPreview.Text = string.Empty;
                }
            }

            this.textBoxPreview.Text = this.textBoxPreview.Text + text;
        }

        /***************************
         *      Private methods
         ***************************/

        // Load keyboard from kb file
        private void LoadKeyboard()
        {
            string keyboardName = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_KEYBOARD);
            if (!string.IsNullOrEmpty(keyboardName))
            {
                string keyboardFile = Constants.CONFIG_LANGS_FOLDER + keyboardName;
                if (File.Exists(keyboardFile))
                {
                    // Read keyboard file
                    XElement keyboard = XElement.Load(keyboardFile);

                    int indexLigne = 0;
                    int kbdHeight = 0;
                    int kbdWidth = 0;
                    int kbdLeft = 0;
                    int kbdDefaultPanelHeight = 40;
                    int kbdDefaultButtonHeight = 40;
                    int kbdDefaultButtonWidth = 40;
                    int kbdDefaultPanelLeft = 0;
                    int kbdNewLeft = 0;
                    int keyboardIndex = 0;

                    foreach (object b in keyboard.Nodes())
                    {
                        if (((XElement)b).Name.ToString().StartsWith("Keyboard"))
                        {
                            keyboardIndex++;

                            // Add the keyboard button
                            Button kbdBouton = new Button();
                            kbdBouton.Text = keyboardIndex.ToString();
                            kbdBouton.Tag = keyboardIndex.ToString();
                            kbdBouton.Font = new Font(kbdBouton.Font, FontStyle.Bold);
                            kbdBouton.Dock = DockStyle.Left;
                            kbdBouton.Click += new EventHandler(this.KeyboardBouton_Click);
                            this.panel3.Controls.Add(kbdBouton);
                            kbdBouton.BringToFront();

                            if (((XElement)b).HasElements)
                            {
                                foreach (object o in ((XElement)b).Nodes())
                                {
                                    if (((XElement)o).HasElements)
                                    {
                                        int linesCount = ((XElement)o).Nodes().Count();
                                        foreach (object ob in ((XElement)o).Nodes())
                                        {
                                            if (((XElement)ob).Name.ToString().StartsWith("Line"))
                                            {
                                                Panel panel = new Panel();
                                                panel.Width = this.panel2.Width;
                                                panel.Top = (this.panel2.Height / linesCount) * indexLigne;
                                                panel.Height = kbdDefaultPanelHeight;
                                                if (((XElement)ob).Attributes().FirstOrDefault(p => p.Name == "height") != null && int.TryParse(((XElement)ob).Attribute("height").Value, out kbdHeight))
                                                {
                                                    panel.Height = kbdHeight;
                                                }

                                                kbdLeft = kbdDefaultPanelLeft;
                                                int tmp = 0;

                                                if (((XElement)ob).Attributes().FirstOrDefault(p => p.Name == "left") != null && int.TryParse(((XElement)ob).Attribute("left").Value, out tmp))
                                                {
                                                    kbdLeft = tmp;
                                                }

                                                panel.Dock = DockStyle.Top;
                                                panel.Tag = keyboardIndex.ToString();
                                                this.panel2.Controls.Add(panel);
                                                panel.BringToFront();

                                                int buttonsCount = ((XElement)ob).Nodes().Count();
                                                int indexButton = 0;

                                                foreach (object character in ((XElement)ob).Nodes())
                                                {
                                                    Button characterButton = new Button();
                                                    if (((XElement)character).Attributes().FirstOrDefault(p => p.Name == "text") != null) 
                                                    {
                                                        characterButton.Text = ((XElement)character).Attribute("text").Value;
                                                    }

                                                    if (((XElement)character).Attributes().FirstOrDefault(p => p.Name == "value") != null)
                                                    {
                                                        characterButton.Tag = indexLigne.ToString() + "#" + ((XElement)character).Attribute("value").Value;
                                                    }

                                                    characterButton.Height = kbdDefaultButtonHeight;

                                                    if (((XElement)character).Attributes().FirstOrDefault(p => p.Name == "height") != null && int.TryParse(((XElement)character).Attribute("height").Value, out kbdHeight))
                                                    {
                                                        characterButton.Height = kbdHeight;
                                                    }

                                                    characterButton.Width = kbdDefaultButtonWidth;

                                                    if (((XElement)character).Attributes().FirstOrDefault(p => p.Name == "width") != null && int.TryParse(((XElement)character).Attribute("width").Value, out kbdWidth))
                                                    {
                                                        characterButton.Width = kbdWidth;
                                                    }

                                                    characterButton.Left = kbdLeft + (characterButton.Width * indexButton);

                                                    if (((XElement)character).Attributes().FirstOrDefault(p => p.Name == "left") != null && int.TryParse(((XElement)character).Attribute("left").Value, out kbdNewLeft))
                                                    {
                                                        characterButton.Left = kbdNewLeft;
                                                    }

                                                    characterButton.Font = new Font(characterButton.Font, FontStyle.Bold);
                                                    characterButton.Click += new EventHandler(this.CharacterButton_Click);
                                                    panel.Controls.Add(characterButton);
                                                    indexButton++;
                                                }

                                                if (keyboardIndex > 1)
                                                {
                                                    panel.Visible = false;
                                                }

                                                indexLigne++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /********************************
         *      Private event handlers
         ********************************/  
        private void Button1_Click(object sender, EventArgs e)
        {
            this.textBoxPreview.Text = string.Empty;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (this.sentText != null)
            {
                TextBox tb = (TextBox)this.sentText;
                tb.Text = this.textBoxPreview.Text;
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
