﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    partial class QuitControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuitControl));
            this.yesButton = new System.Windows.Forms.Button();
            this.noButton = new System.Windows.Forms.Button();
            this.messageLabel = new System.Windows.Forms.Label();
            this.editingEmailWarningMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // yesButton
            // 
            this.yesButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.yesButton.Image = ((System.Drawing.Image)(resources.GetObject("yesButton.Image")));
            this.yesButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.yesButton.Location = new System.Drawing.Point(598, 319);
            this.yesButton.Name = "yesButton";
            this.yesButton.Size = new System.Drawing.Size(86, 86);
            this.yesButton.TabIndex = 0;
			this.yesButton.Text = "YES";
            this.yesButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.yesButton.UseVisualStyleBackColor = true;
            this.yesButton.Click += new System.EventHandler(this.YesButton_Click);
            // 
            // noButton
            // 
            this.noButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.noButton.Image = ((System.Drawing.Image)(resources.GetObject("noButton.Image")));
            this.noButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.noButton.Location = new System.Drawing.Point(370, 319);
            this.noButton.Name = "noButton";
            this.noButton.Size = new System.Drawing.Size(86, 86);
            this.noButton.TabIndex = 1;
            this.noButton.Text = "NO";
            this.noButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.noButton.UseVisualStyleBackColor = true;
            this.noButton.Click += new System.EventHandler(this.NoButton_Click);
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.Location = new System.Drawing.Point(425, 201);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(174, 44);
            this.messageLabel.TabIndex = 2;
			this.messageLabel.Text = "Exit?";
            // 
            // editingEmailWarningMessage
            // 
            this.editingEmailWarningMessage.AutoSize = true;
            this.editingEmailWarningMessage.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editingEmailWarningMessage.Location = new System.Drawing.Point(150, 201);
            this.editingEmailWarningMessage.Name = "editingEmailWarningMessage";
            this.editingEmailWarningMessage.Size = new System.Drawing.Size(174, 44);
            this.editingEmailWarningMessage.TabIndex = 3;
			this.editingEmailWarningMessage.Text = "A message is being edited, do you want to quit anyway?";
            // 
            // quitControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(234)))));
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.editingEmailWarningMessage);
            this.Controls.Add(this.noButton);
            this.Controls.Add(this.yesButton);
            this.Name = "quitControl";
            this.Size = new System.Drawing.Size(1024, 606);
            this.Resize += new System.EventHandler(this.QuitControl_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button yesButton;
        private System.Windows.Forms.Button noButton;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label editingEmailWarningMessage;
    }
}
