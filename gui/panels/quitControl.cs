/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using System;
    using System.Windows.Forms;
    using Mail2VoiceCore;

    public partial class QuitControl : UserControl
    {
        /***************************
         *      Constructor
         ***************************/
        public QuitControl()
        {
            this.InitializeComponent();
        }

        /***************************
         *      Public methods
         ***************************/
        public void ShowEditingEmailWarning()
        {
            this.messageLabel.Visible = false;
            this.editingEmailWarningMessage.Visible = true;

            // TODO: a speech buffer may be implemented
            // SpeechObject.Singleton.Speak(this.editingEmailWarningMessage.Text);
        }

        public void HideEditingEmailWarning()
        {
            this.messageLabel.Visible = true;
            this.editingEmailWarningMessage.Visible = false;
        }

        /********************************
         *      Protected event handlers
         ********************************/   
        protected internal void YesButton_Click(object sender, EventArgs e)
        {
            if (this.editingEmailWarningMessage.Visible == true)
            {
                ((MainForm)this.Parent).EditingEmail = false; // Force forget about email being edited
            }

            ((MainForm)this.Parent).Close();
        }

        protected internal void NoButton_Click(object sender, EventArgs e)
        {
            if (this.editingEmailWarningMessage.Visible == true)
            {
                ((MainForm)this.Parent).ForceDisplaySendPanel();
            }
            else
            {
                ((MainForm)this.Parent).ForceDisplayReceptionPanel(false);
            }
        }

        /********************************
         *      Private event handlers
         ********************************/   
        private void QuitControl_Resize(object sender, EventArgs e)
        {
            try
            {
                this.yesButton.Top  = (this.Height / 2) - (this.yesButton.Height / 2);
                this.noButton.Top   = this.yesButton.Top;
                this.noButton.Left  = (this.Width / 2) - (this.noButton.Width * 2);
                this.yesButton.Left = (this.Width / 2) + this.noButton.Width;

                this.messageLabel.Left = (this.Width / 2) - (this.messageLabel.Width / 2);
                this.messageLabel.Top = this.Height / 4;

                this.editingEmailWarningMessage.Left = (this.Width / 2) - (this.editingEmailWarningMessage.Width / 2);
                this.editingEmailWarningMessage.Top = this.Height / 4;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }
    }
}
