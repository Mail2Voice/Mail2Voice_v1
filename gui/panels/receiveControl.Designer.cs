/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using Mail2Voice.properties;
    using Mail2Voice.Widgets;
    
    partial class ReceiveControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReceiveControl));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.selectedEmailPanel = new System.Windows.Forms.Panel();
            this.emailReadingPanel = new System.Windows.Forms.Panel();
            this.emailPlainViewer = new System.Windows.Forms.RichTextBox();
            this.emailHtmlViewer = new CustomWebBrowser();
            this.panelUrlsEmails = new System.Windows.Forms.Panel();
            this.emailInfosPanel = new System.Windows.Forms.Panel();
            this.emailTitle = new System.Windows.Forms.Label();
            this.emailTitleLabel = new System.Windows.Forms.Label();
            this.addContactButton = new System.Windows.Forms.Button();
            this.addContactButtonPopup = new System.Windows.Forms.Button();
            this.writeToContactButtonPopup = new System.Windows.Forms.Button();
            this.exitButtonPopup = new System.Windows.Forms.Button();
            this.senderNameLabel = new System.Windows.Forms.Label();
            this.emailDate = new System.Windows.Forms.Label();
            this.senderEmailAddressLabel = new System.Windows.Forms.Label();
            this.senderEmailAddress = new System.Windows.Forms.Label();
            this.emailDateLabel = new System.Windows.Forms.Label();
            this.senderName = new System.Windows.Forms.Label();
            this.senderPhoto = new System.Windows.Forms.PictureBox();
            this.emailSpeechSynthControlsPanel = new System.Windows.Forms.Panel();
            this.speechSynthStopButton = new System.Windows.Forms.Button();
            this.speechSynthPauseButton = new System.Windows.Forms.Button();
            this.speechSynthPlayButton = new System.Windows.Forms.Button();
            this.emailAttachmentsPanel = new System.Windows.Forms.Panel();
            this.attachmentsListView = new System.Windows.Forms.ListView();
            this.attachmentsActionsPanel = new System.Windows.Forms.Panel();
            this.openAttachmentButton = new System.Windows.Forms.Button();
            this.previousAttachmentButton = new System.Windows.Forms.Button();
            this.nextAttachmentButton = new System.Windows.Forms.Button();
            this.emailActionsPanel = new System.Windows.Forms.Panel();
            this.deleteEmailButton = new System.Windows.Forms.Button();
            this.replyToEmailButton = new System.Windows.Forms.Button();
            this.fileSystemWatcher = new System.IO.FileSystemWatcher();
            this.emailsListPanel = new System.Windows.Forms.Panel();
            this.mailtoPopup = new System.Windows.Forms.Panel();
            this.emailsListflowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.inboxEmailsList = new System.Windows.Forms.DataGridView();
            this.warningInboxLabel = new System.Windows.Forms.Label();
            this.attachmentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusPictoColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.senderPhotoColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.senderNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.attachmentPictoColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.receptionDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receptionColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.downButton = new System.Windows.Forms.Button();
            this.upButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.selectedEmailPanel.SuspendLayout();
            this.emailReadingPanel.SuspendLayout();
            this.emailInfosPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.senderPhoto)).BeginInit();
            this.emailSpeechSynthControlsPanel.SuspendLayout();
            this.emailAttachmentsPanel.SuspendLayout();
            this.attachmentsActionsPanel.SuspendLayout();
            this.emailActionsPanel.SuspendLayout();
            this.mailtoPopup.SuspendLayout();

            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).BeginInit();
            this.emailsListPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inboxEmailsList)).BeginInit();
            this.SuspendLayout();

            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            
            // 
            // selectedEmailPanel
            // 
            this.selectedEmailPanel.Controls.Add(this.emailReadingPanel);
            this.selectedEmailPanel.Controls.Add(this.emailSpeechSynthControlsPanel);
            this.selectedEmailPanel.Controls.Add(this.emailAttachmentsPanel);
            this.selectedEmailPanel.Controls.Add(this.emailActionsPanel);
            this.selectedEmailPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectedEmailPanel.Location = new System.Drawing.Point(672, 0);
            this.selectedEmailPanel.Name = "selectedEmailPanel";
            this.selectedEmailPanel.Size = new System.Drawing.Size(352, 606);
            this.selectedEmailPanel.TabIndex = 14;
            // 
            // emailReadingPanel
            // 
            this.emailReadingPanel.Controls.Add(this.emailPlainViewer);
            this.emailReadingPanel.Controls.Add(this.emailHtmlViewer);
            this.emailReadingPanel.Controls.Add(this.panelUrlsEmails);
            this.emailReadingPanel.Controls.Add(this.emailInfosPanel);
            this.emailReadingPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emailReadingPanel.Location = new System.Drawing.Point(0, 0);
            this.emailReadingPanel.Name = "emailReadingPanel";
            this.emailReadingPanel.Size = new System.Drawing.Size(352, 327);
            this.emailReadingPanel.TabIndex = 14;
            // 
            // emailPlainViewer
            // 
            this.emailPlainViewer.BackColor = System.Drawing.Color.White;
            this.emailPlainViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emailPlainViewer.Location = new System.Drawing.Point(0, 152);
            this.emailPlainViewer.Multiline = true;
            this.emailPlainViewer.Name = "emailContent";
            this.emailPlainViewer.ReadOnly = true;
            this.emailPlainViewer.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.emailPlainViewer.Size = new System.Drawing.Size(352, 175);
            this.emailPlainViewer.TabIndex = 5;
            // 
            // emailHtmlViewer
            // 
            this.emailHtmlViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emailHtmlViewer.Location = new System.Drawing.Point(0, 152);
            this.emailHtmlViewer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.emailHtmlViewer.MinimumSize = new System.Drawing.Size(23, 25);
            this.emailHtmlViewer.Name = "emailHtmlContent";
            this.emailHtmlViewer.Size = new System.Drawing.Size(352, 175);
            this.emailHtmlViewer.TabIndex = 0;
            this.emailHtmlViewer.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.EmailHtmlViewer_DocumentNavigatingToExternalBrowser);
            this.emailHtmlViewer.Visible = false;
            this.emailHtmlViewer.AllowWebBrowserDrop = false;
            this.emailHtmlViewer.IsWebBrowserContextMenuEnabled = false;
            this.emailHtmlViewer.ScriptErrorsSuppressed = true;
            // 
            // panelUrlsEmails
            // 
            this.panelUrlsEmails.AutoScroll = true;
            this.panelUrlsEmails.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelUrlsEmails.Location = new System.Drawing.Point(352, 152);
            this.panelUrlsEmails.Name = "panelUrlsEmails";
            this.panelUrlsEmails.Size = new System.Drawing.Size(0, 175);
            this.panelUrlsEmails.TabIndex = 11;
            // 
            // emailInfosPanel
            // 
            this.emailInfosPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(237)))));
            this.emailInfosPanel.Controls.Add(this.emailTitleLabel);
            this.emailInfosPanel.Controls.Add(this.emailTitle);
            this.emailInfosPanel.Controls.Add(this.addContactButton);
            this.emailInfosPanel.Controls.Add(this.senderNameLabel);
            this.emailInfosPanel.Controls.Add(this.emailDate);
            this.emailInfosPanel.Controls.Add(this.senderEmailAddressLabel);
            this.emailInfosPanel.Controls.Add(this.senderEmailAddress);
            this.emailInfosPanel.Controls.Add(this.emailDateLabel);
            this.emailInfosPanel.Controls.Add(this.senderName);
            this.emailInfosPanel.Controls.Add(this.senderPhoto);
            this.emailInfosPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.emailInfosPanel.Location = new System.Drawing.Point(0, 0);
            this.emailInfosPanel.Name = "emailInfosPanel";
            this.emailInfosPanel.Size = new System.Drawing.Size(352, 130);
            this.emailInfosPanel.TabIndex = 10;
            // 
            // emailTitleLabel
            // 
            this.emailTitleLabel.Location = new System.Drawing.Point(120, 95);
            this.emailTitleLabel.Name = "emailTitleLabel";
            this.emailTitleLabel.Size = new System.Drawing.Size(85, 20);
            this.emailTitleLabel.TabIndex = 3;
            this.emailTitleLabel.Text = "Subject:";
            this.emailTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.emailTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // emailTitle
            // 
            this.emailTitle.BackColor = System.Drawing.Color.Transparent;
            this.emailTitle.Location = new System.Drawing.Point(205, 95);
            this.emailTitle.Name = "emailTitle";
            this.emailTitle.Size = new System.Drawing.Size(1024, 20);
            this.emailTitle.TabIndex = 4;
            this.emailTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // addContactButton
            // 
            this.addContactButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(234)))), ((int)(((byte)(219)))));
            this.addContactButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addContactButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addContactButton.Image = Mail2Voice.properties.Resources.user_new_32;
            this.addContactButton.Location = new System.Drawing.Point(80, 86);
            this.addContactButton.Name = "addContactButton";
            this.addContactButton.Size = new System.Drawing.Size(40, 40);
            this.addContactButton.TabIndex = 9;
            this.addContactButton.UseVisualStyleBackColor = false;
            this.addContactButton.Visible = false;
            this.addContactButton.Click += new System.EventHandler(this.AddContactButton_Click);
            // 
            // senderNameLabel
            // 
            this.senderNameLabel.Location = new System.Drawing.Point(120, 15);
            this.senderNameLabel.Name = "senderNameLabel";
            this.senderNameLabel.Size = new System.Drawing.Size(85, 18);
            this.senderNameLabel.TabIndex = 1;
            this.senderNameLabel.Text = "Sender:";
            this.senderNameLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // emailDate
            // 
            this.emailDate.AutoSize = true;
            this.emailDate.Location = new System.Drawing.Point(205, 65);
            this.emailDate.Name = "emailDate";
            this.emailDate.Size = new System.Drawing.Size(0, 13);
            this.emailDate.TabIndex = 8;
            // 
            // senderEmailAddressLabel
            // 
            this.senderEmailAddressLabel.Location = new System.Drawing.Point(120, 40);
            this.senderEmailAddressLabel.Name = "senderEmailAddressLabel";
            this.senderEmailAddressLabel.Size = new System.Drawing.Size(85, 18);
            this.senderEmailAddressLabel.TabIndex = 2;
            this.senderEmailAddressLabel.Text = "Email:";
            this.senderEmailAddressLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // senderEmailAddress
            // 
            this.senderEmailAddress.AutoSize = true;
            this.senderEmailAddress.Location = new System.Drawing.Point(205, 40);
            this.senderEmailAddress.Name = "senderEmailAddress";
            this.senderEmailAddress.Size = new System.Drawing.Size(300, 18);
            this.senderEmailAddress.TabIndex = 7;
            // 
            // emailDateLabel
            // 
            this.emailDateLabel.Location = new System.Drawing.Point(120, 65);
            this.emailDateLabel.Name = "emailDateLabel";
            this.emailDateLabel.Size = new System.Drawing.Size(85, 18);
            this.emailDateLabel.TabIndex = 3;
            this.emailDateLabel.Text = "Date:";
            this.emailDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // senderName
            // 
            this.senderName.AutoSize = true;
            this.senderName.Location = new System.Drawing.Point(205, 15);
            this.senderName.Name = "senderName";
            this.senderName.Size = new System.Drawing.Size(300, 18);
            this.senderName.TabIndex = 6;
            // 
            // senderPhoto
            // 
            this.senderPhoto.BackColor = System.Drawing.Color.White;
            this.senderPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.senderPhoto.ErrorImage = Mail2Voice.properties.Resources.user;
            this.senderPhoto.InitialImage = Mail2Voice.properties.Resources.user;
            this.senderPhoto.Location = new System.Drawing.Point(3, 3);
            this.senderPhoto.Name = "senderPhoto";
            this.senderPhoto.Size = new System.Drawing.Size(117, 123);
            this.senderPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.senderPhoto.TabIndex = 0;
            this.senderPhoto.TabStop = false;
            // 
            // emailSpeechSynthControlsPanel
            // 
            this.emailSpeechSynthControlsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(237)))));
            this.emailSpeechSynthControlsPanel.Controls.Add(this.speechSynthStopButton);
            this.emailSpeechSynthControlsPanel.Controls.Add(this.speechSynthPauseButton);
            this.emailSpeechSynthControlsPanel.Controls.Add(this.speechSynthPlayButton);
            this.emailSpeechSynthControlsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.emailSpeechSynthControlsPanel.Location = new System.Drawing.Point(0, 327);
            this.emailSpeechSynthControlsPanel.Name = "emailSpeechSynthControlsPanel";
            this.emailSpeechSynthControlsPanel.Size = new System.Drawing.Size(352, 69);
            this.emailSpeechSynthControlsPanel.TabIndex = 12;
            this.emailSpeechSynthControlsPanel.Resize += new System.EventHandler(this.EmailSpeechSynthControlsPanel_Resize);
            // 
            // addContactButtonPopup
            // 
            this.addContactButtonPopup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addContactButtonPopup.Image = Mail2Voice.properties.Resources.user_new;
            this.addContactButtonPopup.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.addContactButtonPopup.Location = new System.Drawing.Point(4, 4);
            this.addContactButtonPopup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.addContactButtonPopup.Name = "addContactButton";
            this.addContactButtonPopup.Size = new System.Drawing.Size(70, 70);
            this.addContactButtonPopup.TabIndex = 14;
            this.addContactButtonPopup.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addContactButtonPopup.UseVisualStyleBackColor = true;
            this.addContactButtonPopup.Click += new System.EventHandler(this.MailtoAddButton_Click);
            // 
            // writeToContactButtonPopup
            // 
            this.writeToContactButtonPopup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.writeToContactButtonPopup.Image = Mail2Voice.properties.Resources.write;
            this.writeToContactButtonPopup.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.writeToContactButtonPopup.Location = new System.Drawing.Point(80, 4);
            this.writeToContactButtonPopup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.writeToContactButtonPopup.Name = "writeToContactButtonPopup";
            this.writeToContactButtonPopup.Size = new System.Drawing.Size(70, 70);
            this.writeToContactButtonPopup.TabIndex = 31;
            this.writeToContactButtonPopup.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.writeToContactButtonPopup.UseVisualStyleBackColor = true;
            this.writeToContactButtonPopup.Click += new System.EventHandler(this.MailtoWriteButton_Click);
            // 
            // exitButtonPopup
            // 
            this.exitButtonPopup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitButtonPopup.Image = Mail2Voice.properties.Resources.no31;
            this.exitButtonPopup.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.exitButtonPopup.Location = new System.Drawing.Point(156, 4);
            this.exitButtonPopup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.exitButtonPopup.Name = "writeToContactButtonPopup";
            this.exitButtonPopup.Size = new System.Drawing.Size(70, 70);
            this.exitButtonPopup.TabIndex = 31;
            this.exitButtonPopup.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.exitButtonPopup.UseVisualStyleBackColor = true;
            this.exitButtonPopup.Click += new System.EventHandler(this.MailtoExitButton_Click);
            // 
            // mailtoPopup
            // 
            this.mailtoPopup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(234)))), ((int)(((byte)(219)))));
            this.mailtoPopup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mailtoPopup.Location = new System.Drawing.Point(315, 111);
            this.mailtoPopup.Name = "mailtoPopup";
            this.mailtoPopup.Size = new System.Drawing.Size(234, 80);
            this.mailtoPopup.TabIndex = 2;
            this.mailtoPopup.Visible = false;
            this.mailtoPopup.Controls.Add(this.addContactButtonPopup);
            this.mailtoPopup.Controls.Add(this.writeToContactButtonPopup);
            this.mailtoPopup.Controls.Add(this.exitButtonPopup);
            // 
            // speechSynthStopButton
            // 
            this.speechSynthStopButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.speechSynthStopButton.Enabled = false;
            this.speechSynthStopButton.Image = ((System.Drawing.Image)(resources.GetObject("stopButton.Image")));
            this.speechSynthStopButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.speechSynthStopButton.Location = new System.Drawing.Point(235, 3);
            this.speechSynthStopButton.Name = "speechSynthStopButton";
            this.speechSynthStopButton.Size = new System.Drawing.Size(64, 64);
            this.speechSynthStopButton.TabIndex = 8;
            this.speechSynthStopButton.Text = "Stop";
            this.speechSynthStopButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.speechSynthStopButton.UseVisualStyleBackColor = true;
            this.speechSynthStopButton.Click += new System.EventHandler(this.SpeechSynthStopButton_Click);
            // 
            // speechSynthPauseButton
            // 
            this.speechSynthPauseButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.speechSynthPauseButton.Enabled = false;
            this.speechSynthPauseButton.Image = ((System.Drawing.Image)(resources.GetObject("pauseButton.Image")));
            this.speechSynthPauseButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.speechSynthPauseButton.Location = new System.Drawing.Point(144, 3);
            this.speechSynthPauseButton.Name = "speechSynthPauseButton";
            this.speechSynthPauseButton.Size = new System.Drawing.Size(64, 64);
            this.speechSynthPauseButton.TabIndex = 7;
            this.speechSynthPauseButton.Text = "Pause";
            this.speechSynthPauseButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.speechSynthPauseButton.UseVisualStyleBackColor = true;
            this.speechSynthPauseButton.Click += new System.EventHandler(this.SpeechSynthPauseButton_Click);
            // 
            // speechSynthPlayButton
            // 
            this.speechSynthPlayButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.speechSynthPlayButton.Image = ((System.Drawing.Image)(resources.GetObject("playButton.Image")));
            this.speechSynthPlayButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.speechSynthPlayButton.Location = new System.Drawing.Point(51, 3);
            this.speechSynthPlayButton.Name = "speechSynthPlayButton";
            this.speechSynthPlayButton.Size = new System.Drawing.Size(64, 64);
            this.speechSynthPlayButton.TabIndex = 6;
            this.speechSynthPlayButton.Text = "Play";
            this.speechSynthPlayButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.speechSynthPlayButton.UseVisualStyleBackColor = true;
            this.speechSynthPlayButton.Enabled = false;
            this.speechSynthPlayButton.Click += new System.EventHandler(this.SpeechSynthPlayButton_Click);
            // 
            // emailAttachmentsPanel
            // 
            this.emailAttachmentsPanel.Controls.Add(this.attachmentsListView);
            this.emailAttachmentsPanel.Controls.Add(this.attachmentsActionsPanel);
            this.emailAttachmentsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.emailAttachmentsPanel.Location = new System.Drawing.Point(0, 396);
            this.emailAttachmentsPanel.Name = "emailAttachmentsPanel";
            this.emailAttachmentsPanel.Size = new System.Drawing.Size(352, 126);
            this.emailAttachmentsPanel.TabIndex = 13;
            this.emailAttachmentsPanel.Enabled = false;
            // 
            // attachmentsListView
            // 
            this.attachmentsListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.attachmentsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attachmentsListView.Enabled = false;
            this.attachmentsListView.Location = new System.Drawing.Point(74, 0);
            this.attachmentsListView.MultiSelect = false;
            this.attachmentsListView.Name = "attachmentsListView";
            this.attachmentsListView.Size = new System.Drawing.Size(278, 126);
            this.attachmentsListView.TabIndex = 0;
            this.attachmentsListView.UseCompatibleStateImageBehavior = false;
            this.attachmentsListView.Click += new System.EventHandler(this.AttachmentsListView_Click);
            // 
            // attachmentsActionsPanel
            // 
            this.attachmentsActionsPanel.Controls.Add(this.openAttachmentButton);
            this.attachmentsActionsPanel.Controls.Add(this.previousAttachmentButton);
            this.attachmentsActionsPanel.Controls.Add(this.nextAttachmentButton);
            this.attachmentsActionsPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.attachmentsActionsPanel.Location = new System.Drawing.Point(0, 0);
            this.attachmentsActionsPanel.Name = "attachmentsActionsPanel";
            this.attachmentsActionsPanel.Size = new System.Drawing.Size(74, 126);
            this.attachmentsActionsPanel.TabIndex = 5;
            // 
            // openAttachmentButton
            // 
            this.openAttachmentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openAttachmentButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.openAttachmentButton.Image = Mail2Voice.properties.Resources.folder;
            this.openAttachmentButton.Location = new System.Drawing.Point(0, 41);
            this.openAttachmentButton.Name = "openAttachmentButton";
            this.openAttachmentButton.Size = new System.Drawing.Size(74, 44);
            this.openAttachmentButton.TabIndex = 5;
            this.openAttachmentButton.UseVisualStyleBackColor = true;
            this.openAttachmentButton.Click += new System.EventHandler(this.OpenAttachmentButton_Click);
            // 
            // previousAttachmentButton
            // 
            this.previousAttachmentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.previousAttachmentButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.previousAttachmentButton.Image = ((System.Drawing.Image)(resources.GetObject("leftButton.Image")));
            this.previousAttachmentButton.Location = new System.Drawing.Point(0, 0);
            this.previousAttachmentButton.Name = "previousAttachmentButton";
            this.previousAttachmentButton.Size = new System.Drawing.Size(74, 41);
            this.previousAttachmentButton.TabIndex = 3;
            this.previousAttachmentButton.UseVisualStyleBackColor = true;
            this.previousAttachmentButton.Click += new System.EventHandler(this.PreviousAttachmentButton_Click);
            // 
            // nextAttachmentButton
            // 
            this.nextAttachmentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nextAttachmentButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nextAttachmentButton.Image = ((System.Drawing.Image)(resources.GetObject("rightButton.Image")));
            this.nextAttachmentButton.Location = new System.Drawing.Point(0, 85);
            this.nextAttachmentButton.Name = "nextAttachmentButton";
            this.nextAttachmentButton.Size = new System.Drawing.Size(74, 41);
            this.nextAttachmentButton.TabIndex = 4;
            this.nextAttachmentButton.UseVisualStyleBackColor = true;
            this.nextAttachmentButton.Click += new System.EventHandler(this.NextAttachmentButton_Click);
            // 
            // emailActionsPanel
            // 
            this.emailActionsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(237)))));
            this.emailActionsPanel.Controls.Add(this.deleteEmailButton);
            this.emailActionsPanel.Controls.Add(this.replyToEmailButton);
            this.emailActionsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.emailActionsPanel.Location = new System.Drawing.Point(0, 522);
            this.emailActionsPanel.Name = "emailActionsPanel";
            this.emailActionsPanel.Size = new System.Drawing.Size(352, 84);
            this.emailActionsPanel.TabIndex = 11;
            this.emailActionsPanel.Enabled = false;
            // 
            // deleteEmailButton
            // 
            this.deleteEmailButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteEmailButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.deleteEmailButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteButton.Image")));
            this.deleteEmailButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.deleteEmailButton.Location = new System.Drawing.Point(0, 0);
            this.deleteEmailButton.Name = "deleteEmailButton";
            this.deleteEmailButton.Size = new System.Drawing.Size(172, 84);
            this.deleteEmailButton.TabIndex = 9;
            this.deleteEmailButton.Text = "Delete";
            this.deleteEmailButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.deleteEmailButton.UseVisualStyleBackColor = true;
            this.deleteEmailButton.Click += new System.EventHandler(this.DeleteEmailButton_Click);
            // 
            // replyToEmailButton
            // 
            this.replyToEmailButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.replyToEmailButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.replyToEmailButton.Image = Mail2Voice.properties.Resources.write;
            this.replyToEmailButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.replyToEmailButton.Location = new System.Drawing.Point(178, 0);
            this.replyToEmailButton.Name = "replyToEmailButton";
            this.replyToEmailButton.Size = new System.Drawing.Size(174, 84);
            this.replyToEmailButton.TabIndex = 10;
            this.replyToEmailButton.Text = "Reply";
            this.replyToEmailButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.replyToEmailButton.UseVisualStyleBackColor = true;
            this.replyToEmailButton.Click += new System.EventHandler(this.ReplyToEmailButton_Click);
            // 
            // fileSystemWatcher
            // 
            this.fileSystemWatcher.EnableRaisingEvents = true;
            this.fileSystemWatcher.IncludeSubdirectories = true;
            this.fileSystemWatcher.NotifyFilter = System.IO.NotifyFilters.FileName;
            this.fileSystemWatcher.SynchronizingObject = this;
            //this.fileSystemWatcher.Deleted += new System.IO.FileSystemEventHandler(this.FileSystemWatcher_Deleted);
            //this.fileSystemWatcher.Created += new System.IO.FileSystemEventHandler(this.FileSystemWatcher_Created);
            // 
            // emailsListPanel
            // 
            this.emailsListPanel.Controls.Add(this.emailsListflowLayoutPanel);
            this.emailsListPanel.Controls.Add(this.inboxEmailsList);
            this.emailsListPanel.Controls.Add(this.downButton);
            this.emailsListPanel.Controls.Add(this.upButton);
            this.emailsListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.emailsListPanel.Location = new System.Drawing.Point(0, 0);
            this.emailsListPanel.Name = "emailsListPanel";
            this.emailsListPanel.Size = new System.Drawing.Size(672, 606);
            this.emailsListPanel.TabIndex = 15;
            // 
            // emailsListflowLayoutPanel
            // 
            this.emailsListflowLayoutPanel.AutoScroll = true;
            this.emailsListflowLayoutPanel.Location = new System.Drawing.Point(77, 144);
            this.emailsListflowLayoutPanel.Name = "emailsListflowLayoutPanel";
            this.emailsListflowLayoutPanel.Size = new System.Drawing.Size(316, 61);
            this.emailsListflowLayoutPanel.TabIndex = 15;
            this.emailsListflowLayoutPanel.Visible = false;
            //
            // warningInboxLabel
            //
            this.warningInboxLabel.Location = new System.Drawing.Point(100,300);
            this.warningInboxLabel.Name = "warningInboxLabel";
            this.warningInboxLabel.Size = new System.Drawing.Size(400, 100);
            this.warningInboxLabel.Text = "Connexion failed, please check your settings.";
            this.warningInboxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.warningInboxLabel.Visible = false;
            this.warningInboxLabel.ForeColor = System.Drawing.Color.Red;
            this.warningInboxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // inboxEmailsList
            // 
            this.inboxEmailsList.AllowUserToAddRows = false;
            this.inboxEmailsList.AllowUserToDeleteRows = false;
            this.inboxEmailsList.AllowUserToResizeColumns = false;
            this.inboxEmailsList.AllowUserToResizeRows = false;
            this.inboxEmailsList.BackgroundColor = System.Drawing.Color.White;
            this.inboxEmailsList.CausesValidation = false;
            this.inboxEmailsList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.inboxEmailsList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.inboxEmailsList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.inboxEmailsList.ColumnHeadersHeight = 30;
            this.inboxEmailsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.inboxEmailsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.attachmentColumn,
            this.statusColumn,
            this.idColumn,
            this.emailColumn,
            this.statusPictoColumn,
            this.senderPhotoColumn,
            this.senderNameColumn,
            this.attachmentPictoColumn,
            this.receptionDateColumn,
            this.titleColumn,
            this.contentColumn,
            this.typeColumn,
            this.receptionColumn});
            this.inboxEmailsList.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.inboxEmailsList.DefaultCellStyle = dataGridViewCellStyle11;
            this.inboxEmailsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inboxEmailsList.Location = new System.Drawing.Point(0, 54);
            this.inboxEmailsList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.inboxEmailsList.MultiSelect = false;
            this.inboxEmailsList.Name = "inboxEmailsList";
            this.inboxEmailsList.ReadOnly = true;
            this.inboxEmailsList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.inboxEmailsList.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.inboxEmailsList.RowHeadersVisible = false;
            this.inboxEmailsList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.inboxEmailsList.RowTemplate.Height = 70;
            this.inboxEmailsList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.inboxEmailsList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.inboxEmailsList.ShowCellErrors = false;
            this.inboxEmailsList.ShowCellToolTips = false;
            this.inboxEmailsList.ShowEditingIcon = false;
            this.inboxEmailsList.ShowRowErrors = false;
            this.inboxEmailsList.Size = new System.Drawing.Size(672, 498);
            this.inboxEmailsList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.inboxEmailsList.TabIndex = 14;
            this.inboxEmailsList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.InboxEmailsList_CellClick);
            this.inboxEmailsList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.InboxEmailsList_CellEnter);
            this.inboxEmailsList.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.InboxEmailsList_RowsRemoved);
            this.inboxEmailsList.SelectionChanged += new System.EventHandler(this.InboxEmailsList_SelectionChanged);
            this.inboxEmailsList.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.InboxEmailsList_SortCompare);
            // 
            // attachmentColumn
            // 
            this.attachmentColumn.Frozen = true;
            this.attachmentColumn.HeaderText = "Attachment";
            this.attachmentColumn.Name = "attachmentColumn";
            this.attachmentColumn.ReadOnly = true;
            this.attachmentColumn.Visible = false;
            // 
            // statusColumn
            // 
            this.statusColumn.Frozen = true;
            this.statusColumn.HeaderText = "Status";
            this.statusColumn.Name = "statusColumn";
            this.statusColumn.ReadOnly = true;
            this.statusColumn.Visible = false;
            // 
            // idColumn
            // 
            this.idColumn.Frozen = true;
            this.idColumn.HeaderText = "Id";
            this.idColumn.Name = "idColumn";
            this.idColumn.ReadOnly = true;
            this.idColumn.Visible = false;
            this.idColumn.Width = 75;
            // 
            // emailColumn
            // 
            this.emailColumn.Frozen = true;
            this.emailColumn.HeaderText = "Email";
            this.emailColumn.Name = "emailColumn";
            this.emailColumn.ReadOnly = true;
            this.emailColumn.Visible = false;
            // 
            // statusPictoColumn
            // 
            this.statusPictoColumn.Frozen = true;
            this.statusPictoColumn.HeaderText = "Status";
            this.statusPictoColumn.Name = "statusPictoColumn";
            this.statusPictoColumn.ReadOnly = true;
            this.statusPictoColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.statusPictoColumn.Width = 70;
            // 
            // senderPhotoColumn
            // 
            this.senderPhotoColumn.Frozen = true;
            this.senderPhotoColumn.HeaderText = "Portrait";
            this.senderPhotoColumn.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.senderPhotoColumn.Name = "senderPhotoColumn";
            this.senderPhotoColumn.ReadOnly = true;
            this.senderPhotoColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.senderPhotoColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.senderPhotoColumn.Width = 70;
            // 
            // senderNameColumn
            //
            this.senderNameColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this.senderNameColumn.Frozen = true;
            this.senderNameColumn.HeaderText = "Sender name";
            this.senderNameColumn.Name = "senderNameColumn";
            this.senderNameColumn.ReadOnly = true;
            this.senderNameColumn.Width = 125;
            // 
            // attachmentPictoColumn
            // 
            this.attachmentPictoColumn.Frozen = true;
            this.attachmentPictoColumn.HeaderText = "Attch";
            this.attachmentPictoColumn.Name = "attachmentPictoColumn";
            this.attachmentPictoColumn.ReadOnly = true;
            this.attachmentPictoColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.attachmentPictoColumn.Width = 50;
            // 
            // receptionDateColumn
            // 
            this.receptionDateColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this.receptionDateColumn.Frozen = true;
            this.receptionDateColumn.HeaderText = "Date";
            this.receptionDateColumn.Name = "receptionDateColumn";
            this.receptionDateColumn.ReadOnly = true;
            this.receptionDateColumn.Width = 110;
            //this.receptionDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.receptionDateColumn.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.receptionDateColumn.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            // 
            // titleColumn
            // 
            this.titleColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this.titleColumn.Frozen = true;
            this.titleColumn.HeaderText = "Title";
            this.titleColumn.Name = "titleColumn";
            this.titleColumn.ReadOnly = true;
            //this.titleColumn.Width = 200;
            // 
            // contentColumn
            // 
            this.contentColumn.Frozen = true;
            this.contentColumn.HeaderText = "Message";
            this.contentColumn.Name = "contentColumn";
            this.contentColumn.ReadOnly = true;
            this.contentColumn.Visible = false;
            // 
            // typeColumn
            // 
            this.typeColumn.Frozen = true;
            this.typeColumn.HeaderText = "Type";
            this.typeColumn.Name = "typeColumn";
            this.typeColumn.ReadOnly = true;
            this.typeColumn.Visible = false;
            // 
            // receptionColumn
            // 
            this.receptionColumn.Frozen = true;
            this.receptionColumn.HeaderText = "Restore";
            this.receptionColumn.Name = "receptionColumn";
            this.receptionColumn.ReadOnly = true;
            this.receptionColumn.Visible = false;
            this.receptionColumn.Width = 70;
            // 
            // downButton
            // 
            this.downButton.AutoSize = true;
            this.downButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.downButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.downButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downButton.Image = ((System.Drawing.Image)(resources.GetObject("downButton.Image")));
            this.downButton.Location = new System.Drawing.Point(0, 552);
            this.downButton.Margin = new System.Windows.Forms.Padding(0);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(672, 54);
            this.downButton.TabIndex = 2;
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.DownButton_Click);
            // 
            // upButton
            // 
            this.upButton.AutoSize = true;
            this.upButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.upButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.upButton.Image = ((System.Drawing.Image)(resources.GetObject("upButton.Image")));
            this.upButton.Location = new System.Drawing.Point(0, 0);
            this.upButton.Margin = new System.Windows.Forms.Padding(0);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(672, 54);
            this.upButton.TabIndex = 1;
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.UpButton_Click);
            // 
            // timer1
            // 
            //this.timer1.Enabled = true;
            //this.timer1.Interval = 30000;
            //this.timer1.Tick += new System.EventHandler(this.TimerToCheckEmailFiles);
            // 
            // receiveControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.warningInboxLabel);
            this.Controls.Add(this.mailtoPopup);
            this.Controls.Add(this.selectedEmailPanel);
            this.Controls.Add(this.emailsListPanel);


            this.Name = "receiveControl";
            this.Size = new System.Drawing.Size(1024, 606);
            this.Resize += new System.EventHandler(this.ReceiveControl_Resize);
            this.selectedEmailPanel.ResumeLayout(false);
            this.emailReadingPanel.ResumeLayout(false);
            this.emailReadingPanel.PerformLayout();
            this.emailInfosPanel.ResumeLayout(false);
            this.emailInfosPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.senderPhoto)).EndInit();
            this.emailSpeechSynthControlsPanel.ResumeLayout(false);
            this.emailAttachmentsPanel.ResumeLayout(false);
            this.attachmentsActionsPanel.ResumeLayout(false);
            this.emailActionsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).EndInit();
            this.emailsListPanel.ResumeLayout(false);
            this.emailsListPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inboxEmailsList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel selectedEmailPanel;
        private System.Windows.Forms.PictureBox senderPhoto;
        private System.Windows.Forms.RichTextBox emailPlainViewer;
        private CustomWebBrowser emailHtmlViewer;
        private System.Windows.Forms.Label emailTitle;
        private System.Windows.Forms.Label emailTitleLabel;
        private System.Windows.Forms.Label emailDateLabel;
        private System.Windows.Forms.Label senderEmailAddressLabel;
        private System.Windows.Forms.Label senderNameLabel;
        private System.Windows.Forms.Button replyToEmailButton;
        private System.Windows.Forms.Button deleteEmailButton;
        private System.Windows.Forms.Button speechSynthStopButton;
        private System.Windows.Forms.Button speechSynthPauseButton;
        private System.Windows.Forms.Button speechSynthPlayButton;
        private System.Windows.Forms.Panel emailSpeechSynthControlsPanel;
        private System.Windows.Forms.Panel emailAttachmentsPanel;
        private System.Windows.Forms.ListView attachmentsListView;
        private System.Windows.Forms.Panel emailActionsPanel;
        private System.Windows.Forms.Panel emailReadingPanel;
        private System.Windows.Forms.Label emailDate;
        private System.Windows.Forms.Label senderEmailAddress;
        private System.Windows.Forms.Label senderName;
        private System.Windows.Forms.Button nextAttachmentButton;
        private System.Windows.Forms.Button previousAttachmentButton;
        private System.Windows.Forms.Panel attachmentsActionsPanel;
        private System.IO.FileSystemWatcher fileSystemWatcher;
        private System.Windows.Forms.Button openAttachmentButton;
        private System.Windows.Forms.Button addContactButton;
        private System.Windows.Forms.Button addContactButtonPopup;
        private System.Windows.Forms.Button writeToContactButtonPopup;
        private System.Windows.Forms.Button exitButtonPopup;
        private System.Windows.Forms.Panel emailsListPanel;
        private System.Windows.Forms.DataGridView inboxEmailsList;
        private System.Windows.Forms.Label warningInboxLabel;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.Panel emailInfosPanel;
        private System.Windows.Forms.Panel mailtoPopup;
        private System.Windows.Forms.DataGridViewTextBoxColumn attachmentColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailColumn;
        private System.Windows.Forms.DataGridViewImageColumn statusPictoColumn;
        private System.Windows.Forms.DataGridViewImageColumn senderPhotoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn senderNameColumn;
        private System.Windows.Forms.DataGridViewImageColumn attachmentPictoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn receptionDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeColumn;
        private System.Windows.Forms.DataGridViewImageColumn receptionColumn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panelUrlsEmails; // TODO: identify this panel
        private System.Windows.Forms.FlowLayoutPanel emailsListflowLayoutPanel;
        private System.Windows.Forms.Button upButton;
    }
}
