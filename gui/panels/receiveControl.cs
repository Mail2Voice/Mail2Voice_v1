/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using Mail2VoiceCore;

    public partial class ReceiveControl : UserControl
    {
        /***************************
         *      Attributes
         ***************************/
        private int selectedIndex = 0;
        private string currentMailId = string.Empty;
        private string currentReadMailId = string.Empty;
        private List<ReceiveMessage> storedEmails = null;
        private string windowsMode = "RECEPTION";
        private string tmpMailToAddress = string.Empty;
        private Timer mailtoPopupTimer;

        private List<char> listSeparator = new List<char>() { ' ', ',', ';', '?', '!', '\n', '\r' };
        private Dictionary<string, string> specialChars;

        private MailSyncEventHandler myMailSyncEventHandler = null;

        /***************************
         *      Constructor
         ***************************/
        public ReceiveControl(string mode)
        {
            this.InitializeComponent();

            SpeechObject.Singleton.AddVoiceString("dateReception", "On {0}, ");
            SpeechObject.Singleton.AddVoiceString("hourReception", "at {0} hours {1}.");
            SpeechObject.Singleton.AddVoiceString("titleReception", "Subject: {0}.");
            SpeechObject.Singleton.AddVoiceString("messageReception", "Message: {0}.");
            SpeechObject.Singleton.AddVoiceString("endOfMessage", "End of message.");
            SpeechObject.Singleton.AddVoiceString("noAttach", "This email does not include any attachment.");
            SpeechObject.Singleton.AddVoiceString("multiAttach", "This email includes {0} attachments.");

            this.windowsMode = mode;
            if (this.windowsMode == Constants.InboxView)
            {
                this.receptionColumn.Visible = false;
            }
            else
            {
                this.receptionColumn.Visible = true;
            }

            // Prepare delegate that will be used by the MailSyncEventHandler object
            this.ReadStoredEmailsDelegate = new ReadStoredEmailsDelegateT(this.ReadStoredEmails);
            this.myMailSyncEventHandler = new MailSyncEventHandler(this);

            this.InitHtmlSpecialCharsDictionary();

            SpeechObject.Singleton.SpeakCompleted += this.Singleton_SpeakCompleted;

            this.emailPlainViewer.LinkClicked += this.LinkClickedEventHandler;

            this.attachmentsListView.HideSelection = false;

            this.mailtoPopupTimer = new System.Windows.Forms.Timer(this.components);
            this.mailtoPopupTimer.Interval = 5000;
            this.mailtoPopupTimer.Tick += new System.EventHandler(this.OnMailtoPopupTimeout);
        }

        /***************************
         *      Public methods
         ***************************/
        public delegate void ReadStoredEmailsDelegateT();

        public ReadStoredEmailsDelegateT ReadStoredEmailsDelegate;

        public void ReloadConfiguration()
        {
            this.ReadStoredEmails();

            this.fileSystemWatcher.EnableRaisingEvents = false;
            if (this.windowsMode == Constants.InboxView)
            {
                if (Directory.Exists(Constants.EMAIL_USER_FOLDER_INBOX))
                {
                    this.fileSystemWatcher.Path = Constants.EMAIL_USER_FOLDER_INBOX;
                }
            }
            else
            {
                if (Directory.Exists(Constants.EMAIL_USER_FOLDER_TRASH))
                {
                    this.fileSystemWatcher.Path = Constants.EMAIL_USER_FOLDER_TRASH;
                }
            }

            if (!string.IsNullOrEmpty(this.fileSystemWatcher.Path))
            {
                this.fileSystemWatcher.EnableRaisingEvents = true;
            }
        }

        public void ClearInboxGrid()
        {
          this.inboxEmailsList.Rows.Clear();
        }

        public void InitGrid()
        {
            this.ClearInboxGrid();
            this.ReadStoredEmails();
        }

        public void SetImageTypeMime(ImageList listImage)
        {
            this.attachmentsListView.LargeImageList = listImage;
            this.attachmentsListView.SmallImageList = listImage;
        }

        public void ReadStoredEmails()
        {
            if (ConfigSettings.Singleton.Valid == false) 
            {
              this.inboxEmailsList.Rows.Clear();
              return;
            }

            try
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, "Read email files");
                this.storedEmails = EmailObject.Singleton.LoadEmails(this.windowsMode);

                if (this.storedEmails != null && this.storedEmails.Count > 0)
                {
                    int sel = 0;
                    if (this.inboxEmailsList.Rows.Count > 0)
                    {
                        sel = this.inboxEmailsList.FirstDisplayedCell.RowIndex;
                    }
                        
                    int selectedIndex = -1;

                    if (this.inboxEmailsList.SelectedRows.Count > 0)
                    {
                        selectedIndex = this.inboxEmailsList.SelectedRows[0].Index;
                    }

                    string selectedEmailId = null;

                    if (selectedIndex > -1)
                    {
                        selectedEmailId = this.inboxEmailsList.SelectedRows[0].Cells[2].Value.ToString();
                    }

                    this.inboxEmailsList.Rows.Clear();

                    foreach (ReceiveMessage email in this.storedEmails)
                    {
                        Image senderPhoto = null;
                        Image attachmentPicture = Mail2Voice.properties.Resources.transparent;
                        Image emailStatusPicture = Mail2Voice.properties.Resources.mail_unread;
                        Contact emailSender = ContactsObject.Singleton.GetContact(email.FromEmail);

                        if (emailSender != null)
                        {
                            senderPhoto = emailSender.Image;
                        }
                        else
                        {
                            senderPhoto = Mail2Voice.properties.Resources.user_unknow;
                        }

                        if (email.HasAttachment)
                        {
                            attachmentPicture = Mail2Voice.properties.Resources.mail_attach_32;
                        }
                            
                        if (email.IsRead)
                        {
                            emailStatusPicture = Mail2Voice.properties.Resources.mail_read;
                        }

                        string contactName = email.FromName;

                        if (ContactsObject.Singleton.ContactExists(email.FromEmail))
                        {
                            Contact contact = ContactsObject.Singleton.GetContact(email.FromEmail);
                            contactName = contact.Name;
                        }

                        CultureInfo culture = LanguageObject.Singleton.CurrentCulture;
                        string strDate = email.Date.ToString(culture.DateTimeFormat.ShortDatePattern) + System.Environment.NewLine + email.Date.ToString(culture.DateTimeFormat.ShortTimePattern, culture);
                        
                        int index = this.inboxEmailsList.Rows.Add(
                            email.HasAttachment.ToString(),
                            email.IsRead.ToString(),
                            email.Id,
                            email.FromEmail,
                            emailStatusPicture,
                            senderPhoto,
                            contactName,
                            attachmentPicture,
                            strDate,
                            email.Subject,
                            email.Content,
                            email.Type,
                            senderPhoto = Mail2Voice.properties.Resources.inbox);

                        if (email.Id == selectedEmailId)
                        {
                            selectedIndex = index;
                        }

                        System.Windows.Forms.DataGridViewCellStyle rowStyle = new System.Windows.Forms.DataGridViewCellStyle();
                        rowStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular);

                        if (email.IsRead)
                        {
                            this.inboxEmailsList.Rows[index].DefaultCellStyle = rowStyle;
                        }
                    }

                    if (this.inboxEmailsList.Rows.Count > 0)
                    {
                        if (selectedIndex < 0)
                        {
                            selectedIndex = 0;
                        }
                        else if (selectedIndex >= this.inboxEmailsList.Rows.Count)
                        {
                            selectedIndex = this.inboxEmailsList.Rows.Count - 1;
                        }

                        this.inboxEmailsList.Rows[selectedIndex].Selected = true;
                        this.inboxEmailsList.FirstDisplayedCell = this.inboxEmailsList.Rows[sel].Cells[this.senderPhotoColumn.Index];

                        this.DisplayEmailDetails();
                    }
                }
                else
                {
                    this.inboxEmailsList.Rows.Clear();
                    this.ResetEmailDetails();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void ResetEmailDetails()
        {
            this.senderPhoto.Image = null;
            this.senderName.Text = string.Empty;
            this.senderEmailAddress.Text = string.Empty;
            this.emailDate.Text = string.Empty;
            this.emailTitle.Text = string.Empty;
            this.emailPlainViewer.Text = string.Empty;
            this.emailHtmlViewer.Navigate("about:");
            this.attachmentsListView.Items.Clear();
        }

        public void SetWarningInboxLabel(bool val)
        {
          this.warningInboxLabel.Visible = val;
        }

        /********************************
         *      Protected event handlers
         ********************************/   

        #region "Moving in the emails list"
        protected internal void UpButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.inboxEmailsList.SelectedRows.Count > 0)
                {
                    this.selectedIndex = this.inboxEmailsList.SelectedRows[0].Index;
                }
                    
                this.UnselectAll();
                this.selectedIndex--;

                if (this.selectedIndex <= 0)
                {
                    this.selectedIndex = 0;
                }
                else if (this.selectedIndex >= this.inboxEmailsList.Rows.Count)
                {
                    this.selectedIndex = this.inboxEmailsList.Rows.Count - 1;
                }

                this.inboxEmailsList.Rows[this.selectedIndex].Selected = true;

                if (this.inboxEmailsList.FirstDisplayedScrollingRowIndex > 0)
                {
                    this.inboxEmailsList.FirstDisplayedScrollingRowIndex--;
                }
                    
                this.DisplayEmailDetails();
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void DownButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.inboxEmailsList.SelectedRows.Count > 0)
                {
                    this.selectedIndex = this.inboxEmailsList.SelectedRows[0].Index;
                }

                this.UnselectAll();
                this.selectedIndex++;

                if (this.selectedIndex < 0)
                {
                    this.selectedIndex = 0;
                }
                else if (this.selectedIndex >= this.inboxEmailsList.Rows.Count)
                {
                    this.selectedIndex = this.inboxEmailsList.Rows.Count - 1;
                }
                    
                this.inboxEmailsList.Rows[this.selectedIndex].Selected = true;

                int totalHeight = this.inboxEmailsList.Height - this.inboxEmailsList.ColumnHeadersHeight;
                int lineHeight = this.inboxEmailsList.RowTemplate.Height;
                int positionHeight = lineHeight * (this.selectedIndex + 1);

                if (positionHeight > totalHeight)
                {
                    this.inboxEmailsList.FirstDisplayedScrollingRowIndex++;
                }

                this.DisplayEmailDetails();
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }
        #endregion "Moving in the emails list"

        #region "Events in emails details view"
        protected internal void SpeechSynthPlayButton_Click(object sender, EventArgs e)
        {
            this.currentReadMailId = this.currentMailId;

            // Pre-translate tokens
            string receptionDateFormat = SpeechObject.Singleton.GetVoiceString("dateReception");
            string receptionTimeFormat = SpeechObject.Singleton.GetVoiceString("hourReception");
            string receptionTitleFormat = SpeechObject.Singleton.GetVoiceString("titleReception");
            string receptionMessageFormat = SpeechObject.Singleton.GetVoiceString("messageReception");
            string mailEnd = SpeechObject.Singleton.GetVoiceString("endOfMessage");

            if (this.attachmentsListView.Items.Count == 0)
            {
                mailEnd += SpeechObject.Singleton.GetVoiceString("noAttach");
            }
            else
            {
                mailEnd += string.Format(SpeechObject.Singleton.GetVoiceString("multiAttach"), this.attachmentsListView.Items.Count.ToString());
            }

            // Create speech fragments
            int cutPos = this.emailDate.Text.LastIndexOf(',');
            string tmpDate = this.emailDate.Text.Substring(0, cutPos);
            string tmpTime = this.emailDate.Text.Substring(cutPos + 1).Trim();
            string[] hoursMinutes = tmpTime.Split(':');

            string date = string.Format(receptionDateFormat, tmpDate);
            string time = string.Format(receptionTimeFormat, hoursMinutes[0], hoursMinutes[1]);
            string mailHeader = this.senderName.Text + " " + date + " " + time + ". " + string.Format(receptionTitleFormat, this.emailTitle.Text) + ". ";

            string mailContent = string.Empty;

            if (this.emailPlainViewer.Visible)
            {
                mailContent = string.Format(receptionMessageFormat, this.emailPlainViewer.Text);
            }
            else
            {
                mailContent = this.emailHtmlViewer.Document.Body.InnerText;
            }

            mailContent += ". " + mailEnd + ".";

            string finalTextToRead = mailHeader + mailContent;

            // Start speech
            SpeechObject.Singleton.Speak(finalTextToRead);

            if (this.windowsMode == Constants.InboxView)
            {
                FileObject.Singleton.MoveEmail(Constants.EMAIL_USER_FOLDER_INBOX_UNREAD, Constants.EMAIL_USER_FOLDER_INBOX_READ, this.currentMailId);
                ReceiveObject.Singleton.SetEmailSeen(Constants.EMAIL_INBOX, this.currentMailId);
            }
            else
            {
                FileObject.Singleton.MoveEmail(Constants.EMAIL_USER_FOLDER_TRASH_UNREAD, Constants.EMAIL_USER_FOLDER_TRASH_READ, this.currentMailId);
                ReceiveObject.Singleton.SetEmailSeen(Constants.EMAIL_TRASH, this.currentMailId);
            }

            this.SetReadEmailRow(this.inboxEmailsList.CurrentRow);

            this.speechSynthPauseButton.Enabled = true;
            this.speechSynthStopButton.Enabled = true;
        }

        protected internal void SpeechSynthPauseButton_Click(object sender, EventArgs e)
        {
            SpeechObject.Singleton.Pause();
        }

        protected internal void SpeechSynthStopButton_Click(object sender, EventArgs e)
        {
            SpeechObject.Singleton.Stop();
            this.speechSynthPauseButton.Enabled = false;
            this.speechSynthStopButton.Enabled = false;
            this.currentReadMailId = string.Empty;
        }

        protected internal void DeleteEmailButton_Click(object sender, EventArgs e)
        {
            SpeechObject.Singleton.Stop();
            this.ResetEmailDetails();

            if (this.windowsMode == Constants.InboxView)
            {
                if (ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_EMAIL_ACTIVATE_TRASH).Equals(Constants.TRUE))
                {
                    ((MainForm)this.Parent).OperationInProgress(true);
                    if (FileObject.Singleton.EmailExists(Constants.EMAIL_USER_FOLDER_INBOX_READ, this.currentMailId))
                    {
                        FileObject.Singleton.MoveEmail(
                            Constants.EMAIL_USER_FOLDER_INBOX_READ,
                            Constants.EMAIL_USER_FOLDER_TRASH_READ,
                            this.currentMailId);

                        // Move the mail on the server
                        ReceiveObject.Singleton.MoveEmail(Constants.EMAIL_INBOX, Constants.EMAIL_TRASH, Constants.EMAIL_READ, this.currentMailId);
                    }
                    else
                    {
                        FileObject.Singleton.MoveEmail(
                            Constants.EMAIL_USER_FOLDER_INBOX_UNREAD,
                            Constants.EMAIL_USER_FOLDER_TRASH_UNREAD,
                            this.currentMailId);

                        // Move the mail on the server
                        ReceiveObject.Singleton.MoveEmail(Constants.EMAIL_INBOX, Constants.EMAIL_TRASH, Constants.EMAIL_UNREAD, this.currentMailId);
                    }

                    ((MainForm)this.Parent).OperationInProgress(false);
                }
                else
                {
                    // Delete email on the server
                    if (FileObject.Singleton.EmailExists(Constants.EMAIL_USER_FOLDER_TRASH_READ, this.currentMailId))
                    {
                        FileObject.Singleton.DeleteEmailInFolders(Constants.EMAIL_USER_FOLDER_TRASH_READ, this.currentMailId);

                        // Delete email on the server
                        ReceiveObject.Singleton.DeleteEmail(Constants.EMAIL_TRASH, Constants.EMAIL_READ, this.currentMailId);
                    }
                    else
                    {
                        FileObject.Singleton.DeleteEmailInFolders(Constants.EMAIL_USER_FOLDER_TRASH_UNREAD, this.currentMailId);

                        // Delete email on the server
                        ReceiveObject.Singleton.DeleteEmail(Constants.EMAIL_TRASH, Constants.EMAIL_UNREAD, this.currentMailId);
                    }
                }

                ((MainForm)this.Parent).RefreshEmails(Constants.TrashView);
            }
            else
            {
                // Delete email on the server
                if (FileObject.Singleton.EmailExists(Constants.EMAIL_USER_FOLDER_TRASH_READ, this.currentMailId))
                {
                    FileObject.Singleton.DeleteEmailInFolders(Constants.EMAIL_USER_FOLDER_TRASH_READ, this.currentMailId);

                    // Delete email on the server
                    ReceiveObject.Singleton.DeleteEmail(Constants.EMAIL_TRASH, Constants.EMAIL_READ, this.currentMailId);
                }
                else
                {
                    FileObject.Singleton.DeleteEmailInFolders(Constants.EMAIL_USER_FOLDER_TRASH_UNREAD, this.currentMailId);

                    // Delete email on the server
                    ReceiveObject.Singleton.DeleteEmail(Constants.EMAIL_TRASH, Constants.EMAIL_UNREAD, this.currentMailId);
                }
            }

            this.ReadStoredEmails();
        }

        protected internal void ReplyToEmailButton_Click(object sender, EventArgs e)
        {
            ((MainForm)this.Parent).SendEmail(this.senderEmailAddress.Text);
        }

        protected internal void PreviousAttachmentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.attachmentsListView.Items.Count > 0)
                {
                    int index = this.GetSelectedIndex();

                    if (index > 0)
                    {
                        index--;
                    }
                    else
                    {
                        index = 0;
                    }

                    this.attachmentsListView.Items[index].Selected = true;
                    this.attachmentsListView.Items[index].Focused = true;
                    this.attachmentsListView.EnsureVisible(index);
                    this.attachmentsListView.Select();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void NextAttachmentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.attachmentsListView.Items.Count > 0)
                {
                    int index = this.GetSelectedIndex();

                    if (index + 1 < this.attachmentsListView.Items.Count)
                    {
                        index++;
                    }

                    this.attachmentsListView.Items[index].Selected = true;
                    this.attachmentsListView.Items[index].Focused = true;
                    this.attachmentsListView.EnsureVisible(index);
                    this.attachmentsListView.Select();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void OpenAttachmentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.attachmentsListView.SelectedItems.Count > 0)
                {
                    if (this.attachmentsListView.SelectedItems[0].Tag.ToString() != null)
                    {
                        System.Diagnostics.Process.Start(this.attachmentsListView.SelectedItems[0].Tag.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected void Singleton_SpeakCompleted(object sender, System.Speech.Synthesis.SpeakCompletedEventArgs e)
        {
            this.speechSynthPauseButton.Enabled = false;
            this.speechSynthStopButton.Enabled = false;
        }

        protected void LinkClickedEventHandler(object sender, System.Windows.Forms.LinkClickedEventArgs e)
        {
            string link = e.LinkText.ToLower();

            if (link.StartsWith("http"))
            {
                System.Diagnostics.Process.Start(e.LinkText);
            }
            else if (link.StartsWith("mailto:"))
            {
                string[] mailtoParts = link.Split(':');

                if (mailtoParts.Length == 2 && LibraryObject.Singleton.IsValidEmail(mailtoParts[1]))
                {
                    if (ContactsObject.Singleton.ContactExists(mailtoParts[1]))
                    {
                        ((MainForm)this.Parent).HandleMailTo(mailtoParts[1]);
                    }
                    else
                    {
                        this.tmpMailToAddress = mailtoParts[1];
                        this.DisplayMailtoPopup();
                    }
                }
            }
        }

        protected void MailtoAddButton_Click(object sender, EventArgs args)
        {
            this.mailtoPopup.Visible = false;
            ((MainForm)this.Parent).AddContact(this.tmpMailToAddress, this.tmpMailToAddress);
        }

        protected void MailtoWriteButton_Click(object sender, EventArgs args)
        {
            this.mailtoPopup.Visible = false;
            ((MainForm)this.Parent).HandleMailTo(this.tmpMailToAddress);
        }

        protected void MailtoExitButton_Click(object sender, EventArgs args)
        {
            this.mailtoPopup.Visible = false;
        }

        /***************************
         *      Private methods
         ***************************/
        private void UnselectAll()
        {
            try
            {
                for (int x = 0; x < this.inboxEmailsList.Rows.Count; x++)
                {
                    this.inboxEmailsList.Rows[x].Selected = false;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        // TODO: it should be another way to do so...
        private int GetSelectedIndex()
        {
            // TODO: use foreach loop
            for (int x = 0; x < this.attachmentsListView.Items.Count; x++)
            {
                if (this.attachmentsListView.Items[x].Selected)
                {
                    return x;
                }
            }

            return -1;
        }

        #region "Events on emails list"
        private void DisplayEmailDetails()
        {
            // Display email details in the panels on the right.
            try
            {
                if (this.inboxEmailsList.SelectedRows.Count > 0)
                {
                    int selectedRow = this.inboxEmailsList.SelectedRows[0].Index;

                    this.ResetEmailDetails();

                    Contact emailSender = ContactsObject.Singleton.GetContact(this.inboxEmailsList[this.emailColumn.Index, selectedRow].Value.ToString());

                    if (emailSender != null)
                    {
                        this.senderName.Text = emailSender.Name;
                        this.addContactButton.Hide();
                        if (emailSender.Image != null)
                        {
                            this.senderPhoto.Image = emailSender.Image;
                        }
                        else
                        {
                            this.senderPhoto.Image = Mail2Voice.properties.Resources.user_known;
                        }
                    }
                    else
                    {
                        this.addContactButton.Show();
                        this.senderName.Text = this.inboxEmailsList[this.senderNameColumn.Index, selectedRow].Value.ToString();
                        this.senderPhoto.Image = Mail2Voice.properties.Resources.user_unknow;
                    }

                    this.senderEmailAddress.Text = this.inboxEmailsList[this.emailColumn.Index, selectedRow].Value.ToString();
                    DateTime date; 
                    string tmpDate = string.Join(" ", Regex.Split(this.inboxEmailsList[this.receptionDateColumn.Index, selectedRow].Value.ToString(), @"(?:\r\n|\n|\r)"));
                    CultureInfo culture = LanguageObject.Singleton.CurrentCulture;
                    date = DateTime.Parse(tmpDate, culture);
                    this.emailDate.Text = date.ToString(culture.DateTimeFormat.LongDatePattern, culture) + ", " + date.ToString(culture.DateTimeFormat.ShortTimePattern, culture);
                    this.emailTitle.Text = this.inboxEmailsList[this.titleColumn.Index, selectedRow].Value.ToString();
                    this.currentMailId = this.inboxEmailsList[this.idColumn.Index, selectedRow].Value.ToString();
                    string mailContent = this.inboxEmailsList[this.contentColumn.Index, selectedRow].Value.ToString();

                    if (this.inboxEmailsList[this.typeColumn.Index, selectedRow].Value.ToString() == "html")
                    {
                        this.SwitchToMailHtmlViewer();
                        this.emailHtmlViewer.Navigating -= this.EmailHtmlViewer_DocumentNavigatingToExternalBrowser;
                        this.emailHtmlViewer.DocumentCompleted -= this.EmailHtmlViewer_DocumentCompletedFromEmail;
                        this.emailHtmlViewer.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.EmailHtmlViewer_DocumentCompletedFromEmail);

                        this.emailHtmlViewer.DocumentText = mailContent;
                    }
                    else
                    {
                        this.SwitchToMailPlainViewer();
                        this.emailPlainViewer.Text = mailContent;
                    }

                    // Manage attachments.
                    string directory;
                    if (this.windowsMode == Constants.InboxView)
                    {
                        if (this.inboxEmailsList[this.statusColumn.Index, selectedRow].Value.ToString() == Constants.TRUE)
                        {
                            directory = Constants.EMAIL_USER_FOLDER_INBOX_READ;
                        }
                        else
                        {
                            directory = Constants.EMAIL_USER_FOLDER_INBOX_UNREAD;
                        }
                    }
                    else
                    {
                        if (this.inboxEmailsList[this.statusColumn.Index, selectedRow].Value.ToString() == Constants.TRUE)
                        {
                            directory = Constants.EMAIL_USER_FOLDER_TRASH_READ;
                        }
                        else
                        {
                            directory = Constants.EMAIL_USER_FOLDER_TRASH_UNREAD;
                        }
                    }

                    this.attachmentsListView.Items.Clear();
                    this.previousAttachmentButton.Enabled = this.nextAttachmentButton.Enabled = this.openAttachmentButton.Enabled = false;

                    if (Directory.Exists(directory + this.inboxEmailsList[this.idColumn.Index, selectedRow].Value.ToString()))
                    {
                        List<string> attachmentsList = FileObject.Singleton.ListFilesFromDirectory(directory + this.inboxEmailsList[this.idColumn.Index, selectedRow].Value.ToString());
                        if (attachmentsList.Count > 0)
                        {
                            for (int x = 0; x < attachmentsList.Count; x++)
                            {
                                this.attachmentsListView.Items.Add(new ListViewItem(Path.GetFileName(attachmentsList[x]), FileObject.Singleton.FileType(attachmentsList[x])) { Tag = attachmentsList[x] });
                            }

                            this.attachmentsListView.Items[0].Selected = true;
                            this.attachmentsListView.Items[0].Focused = true;
                            this.attachmentsListView.EnsureVisible(0);
                            this.attachmentsListView.Select();
                        }

                        if (this.attachmentsListView.Items.Count > 0)
                        {
                            this.attachmentsListView.Enabled = true;
                            this.previousAttachmentButton.Enabled = this.nextAttachmentButton.Enabled = this.openAttachmentButton.Enabled = true;
                        }
                    }
                    else
                    {
                        this.attachmentsListView.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        private void SetReadEmailRow(DataGridViewRow row)
        {
            System.Windows.Forms.DataGridViewCellStyle rowStyle = new System.Windows.Forms.DataGridViewCellStyle();
            rowStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular);
            Image emailStatusPicture = Mail2Voice.properties.Resources.mail_read;

            foreach (DataGridViewCell cell in row.Cells)
            {
                if (cell.ColumnIndex == 4)
                {
                    cell.Value = emailStatusPicture;
                }

                cell.Style = rowStyle;
            }
        }

        /********************************
         *      Private event handlers
         ********************************/   
        private void InboxEmailsList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.speechSynthPlayButton.Enabled = true;

            try
            {
                this.DisplayEmailDetails();
                if (e.ColumnIndex == this.inboxEmailsList.Columns[this.receptionColumn.Index].Index)
                {
                    if (this.windowsMode == Constants.TrashView)
                    {
                        ((MainForm)this.Parent).OperationInProgress(true);
                        this.ResetEmailDetails();

                        if (this.inboxEmailsList[this.statusColumn.Index, e.RowIndex].Value.ToString() == Constants.TRUE)
                        {
                            FileObject.Singleton.MoveEmail(
                                Constants.EMAIL_USER_FOLDER_TRASH_READ,
                                Constants.EMAIL_USER_FOLDER_INBOX_READ,
                                this.inboxEmailsList[this.idColumn.Index, e.RowIndex].Value.ToString());

                            // Put back the mail in inbox folder on the server.
                            ReceiveObject.Singleton.MoveEmail(Constants.EMAIL_TRASH, Constants.EMAIL_INBOX, Constants.EMAIL_READ, this.currentMailId);
                        }
                        else
                        {
                            FileObject.Singleton.MoveEmail(
                                Constants.EMAIL_USER_FOLDER_TRASH_UNREAD,
                                Constants.EMAIL_USER_FOLDER_INBOX_UNREAD,
                                this.inboxEmailsList[this.idColumn.Index, e.RowIndex].Value.ToString());
                            this.inboxEmailsList.Rows.RemoveAt(e.RowIndex);

                            // Put back the mail in inbox folder on the server.
                            ReceiveObject.Singleton.MoveEmail(Constants.EMAIL_TRASH, Constants.EMAIL_INBOX, Constants.EMAIL_UNREAD, this.currentMailId);
                        }

                        ((MainForm)this.Parent).OperationInProgress(false);
                        this.ReadStoredEmails();
                        ((MainForm)this.Parent).RefreshEmails(Constants.InboxView);
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        private void InboxEmailsList_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.DisplayEmailDetails();
        }

        private void InboxEmailsList_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (this.inboxEmailsList.Rows.Count == 0)
            {
                this.speechSynthPlayButton.Enabled = false;
                this.emailActionsPanel.Enabled = false;
                this.emailAttachmentsPanel.Enabled = false;
            }
        }

        private void InboxEmailsList_SelectionChanged(object sender, System.EventArgs e)
        {
            this.speechSynthPlayButton.Enabled = true;
            this.emailActionsPanel.Enabled = true;
            this.emailAttachmentsPanel.Enabled = true;

            // Check if the current displayed mail is the one that is currently read (if any).
            // Also need to check if the currentReadMailId is empty because this event can be called
            // by the other panel (when no email is currently read).
            if (this.currentReadMailId != string.Empty && this.currentReadMailId != this.currentMailId)
            {
                this.SpeechSynthStopButton_Click(null, null);
            }
        }

        #endregion "Events on emails list"

        private void AttachmentsListView_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.attachmentsListView.SelectedItems.Count > 0)
                {
                    if (this.attachmentsListView.SelectedItems[0].Tag.ToString() != null)
                    {
                        System.Diagnostics.Process.Start(this.attachmentsListView.SelectedItems[0].Tag.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        private void AddContactButton_Click(object sender, EventArgs e)
        {
            ((MainForm)this.Parent).AddContact(this.senderEmailAddress.Text, this.senderName.Text);
        }

        private void EmailSpeechSynthControlsPanel_Resize(object sender, EventArgs e)
        {
            this.speechSynthPauseButton.Left = (this.emailSpeechSynthControlsPanel.Width / 2) - (this.speechSynthPauseButton.Width / 2);
            this.speechSynthPlayButton.Left = this.speechSynthPauseButton.Left - this.speechSynthPauseButton.Width - 30;
            this.speechSynthStopButton.Left = this.speechSynthPauseButton.Left + this.speechSynthPauseButton.Width + 30;
        }

        private void InboxEmailsList_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.Index == 8)
            {
                e.SortResult = System.DateTime.Compare(DateTime.Parse(e.CellValue1.ToString(), LanguageObject.Singleton.CurrentCulture), DateTime.Parse(e.CellValue2.ToString(), LanguageObject.Singleton.CurrentCulture));
                e.Handled = true;
            }
        }

        #endregion "Events in emails details view"

        private void ReceiveControl_Resize(object sender, EventArgs e)
        {
            try
            {
                int width = 0;
                if (this.windowsMode == Constants.InboxView)
                {
                    width = this.inboxEmailsList.Columns[this.statusPictoColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.senderPhotoColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.senderNameColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.attachmentPictoColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.receptionDateColumn.Index].Width +
                            20;
                }
                else
                {
                    width = this.inboxEmailsList.Columns[this.statusPictoColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.senderPhotoColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.senderNameColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.attachmentPictoColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.receptionDateColumn.Index].Width +
                            this.inboxEmailsList.Columns[this.receptionColumn.Index].Width +
                            20;
                }

                this.inboxEmailsList.Columns[this.titleColumn.Index].Width = this.inboxEmailsList.Width - width;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        private void SwitchToMailHtmlViewer()
        {
            if (!this.emailHtmlViewer.Visible)
            {
                this.emailPlainViewer.Hide();
                this.emailHtmlViewer.Show();
            }
        }

        private void SwitchToMailPlainViewer()
        {
            if (!this.emailPlainViewer.Visible)
            {
                this.emailHtmlViewer.Hide();
                this.emailPlainViewer.Show();
            }
        }

        private void InitHtmlSpecialCharsDictionary()
        {
            this.specialChars = new Dictionary<string, string>();
            this.specialChars.Add("&quot;", "\"");
            this.specialChars.Add("&amp;", "&");
            this.specialChars.Add("&euro;", "€");
            this.specialChars.Add("&lt;", "<");
            this.specialChars.Add("&gt;", ">");
            this.specialChars.Add("&oelig;", "œ");
            this.specialChars.Add("&Yuml;", "Y");
            this.specialChars.Add("&nbsp;", " ");
            this.specialChars.Add("&iexcl;", "¡");
            this.specialChars.Add("&cent;", "¢");
            this.specialChars.Add("&pound;", "£");
            this.specialChars.Add("&curren;", "¤");
            this.specialChars.Add("&yen;", "¥");
            this.specialChars.Add("&brvbar;", "¦");
            this.specialChars.Add("&sect;", "§");
            this.specialChars.Add("&uml;", "¨");
            this.specialChars.Add("&copy;", "©");
            this.specialChars.Add("&ordf;", "ª");
            this.specialChars.Add("&laquo;", "«");
            this.specialChars.Add("&not;", "¬");
            this.specialChars.Add("&reg;", "®");
            this.specialChars.Add("&masr;", "¯");
            this.specialChars.Add("&deg;", "°");
            this.specialChars.Add("&plusmn;", "±");
            this.specialChars.Add("&sup2;", "²");
            this.specialChars.Add("&sup3;", "³");
            this.specialChars.Add("&acute;", "'");
            this.specialChars.Add("&micro;", "µ");
            this.specialChars.Add("&para;", "¶");
            this.specialChars.Add("&middot;", "·");
            this.specialChars.Add("&cedil;", "¸");
            this.specialChars.Add("&sup1;", "¹");
            this.specialChars.Add("&ordm;", "º");
            this.specialChars.Add("&raquo;", "»");
            this.specialChars.Add("&frac14;", "¼");
            this.specialChars.Add("&frac12;", "½");
            this.specialChars.Add("&frac34;", "¾");
            this.specialChars.Add("&iquest;", "¿");
            this.specialChars.Add("&Agrave;", "À");
            this.specialChars.Add("&Aacute;", "Á");
            this.specialChars.Add("&Acirc;", "Â");
            this.specialChars.Add("&Atilde;", "Ã");
            this.specialChars.Add("&Auml;", "Ä");
            this.specialChars.Add("&Aring;", "Å");
            this.specialChars.Add("&Aelig;", "Æ");
            this.specialChars.Add("&Ccedil;", "Ç");
            this.specialChars.Add("&Egrave;", "È");
            this.specialChars.Add("&Eacute;", "É");
            this.specialChars.Add("&Ecirc;", "Ê");
            this.specialChars.Add("&Euml;", "Ë");
            this.specialChars.Add("&Igrave;", "Ì");
            this.specialChars.Add("&Iacute;", "Í");
            this.specialChars.Add("&Icirc;", "Î");
            this.specialChars.Add("&Iuml;", "Ï");
            this.specialChars.Add("&eth;", "Ð");
            this.specialChars.Add("&Ntilde;", "Ñ");
            this.specialChars.Add("&Ograve;", "Ò");
            this.specialChars.Add("&Oacute;", "Ó");
            this.specialChars.Add("&Ocirc;", "Ô");
            this.specialChars.Add("&Otilde;", "Õ");
            this.specialChars.Add("&Ouml;", "Ö");
            this.specialChars.Add("&times;", "×");
            this.specialChars.Add("&Oslash;", "Ø");
            this.specialChars.Add("&Ugrave;", "Ù");
            this.specialChars.Add("&Uacute;", "Ú");
            this.specialChars.Add("&Ucirc;", "Û");            
            this.specialChars.Add("&Uuml;", "Ü");
            this.specialChars.Add("&Yacute;", "Ý");
            this.specialChars.Add("&thorn;", "Þ");
            this.specialChars.Add("&szlig;", "ß");            
            this.specialChars.Add("&agrave;", "à");
            this.specialChars.Add("&aacute;", "á");
            this.specialChars.Add("&acirc;", "â");
            this.specialChars.Add("&atilde;", "ã");            
            this.specialChars.Add("&auml;", "ä");
            this.specialChars.Add("&aring;", "å");
            this.specialChars.Add("&aelig;", "æ");
            this.specialChars.Add("&ccedil;", "ç");            
            this.specialChars.Add("&egrave;", "è");
            this.specialChars.Add("&eacute;", "é");
            this.specialChars.Add("&ecirc;", "ê");
            this.specialChars.Add("&euml;", "ë");            
            this.specialChars.Add("&igrave;", "ì");
            this.specialChars.Add("&iacute;", "í");
            this.specialChars.Add("&icirc;", "î");
            this.specialChars.Add("&iuml;", "ï");
            /*specialChars.Add("&eth;", "ð"); Throw exception */
            this.specialChars.Add("&ntilde;", "ñ");
            this.specialChars.Add("&ograve;", "ò");
            this.specialChars.Add("&oacute;", "ó");
            this.specialChars.Add("&ocirc;", "ô");
            this.specialChars.Add("&otilde;", "õ");
            this.specialChars.Add("&ouml;", "ö");
            this.specialChars.Add("&divide;", "÷");
            this.specialChars.Add("&oslash;", "ø");
            this.specialChars.Add("&ugrave;", "ù");
            this.specialChars.Add("&uacute;", "ú");
            this.specialChars.Add("&ucirc;", "û");
            this.specialChars.Add("&uuml;", "ü");
            this.specialChars.Add("&yacute;", "ý");
            /* specialChars.Add("&thorn;", "þ"); Throw exception */
            this.specialChars.Add("&yuml;", "ÿ");
        }

        private void EmailHtmlViewer_DocumentCompletedFromEmail(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this.emailHtmlViewer.DocumentCompleted -= this.EmailHtmlViewer_DocumentCompletedFromEmail;
            this.emailHtmlViewer.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.EmailHtmlViewer_DocumentNavigatingToExternalBrowser);
        }

        private void EmailHtmlViewer_DocumentNavigatingToExternalBrowser(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.OriginalString.ToLower().StartsWith("mailto:"))
            {
                string[] mailtoParts = e.Url.OriginalString.Split(':');

                if (mailtoParts.Length == 2 && LibraryObject.Singleton.IsValidEmail(mailtoParts[1]))
                {
                    if (ContactsObject.Singleton.ContactExists(mailtoParts[1]))
                    {
                        ((MainForm)this.Parent).HandleMailTo(mailtoParts[1]);
                    }
                    else
                    {
                        this.tmpMailToAddress = mailtoParts[1];
                        this.DisplayMailtoPopup();
                    }
                }

                e.Cancel = true;
            }
            else if (e.Url.OriginalString != "about:" && e.Url.OriginalString != "about:blank")
            {
                e.Cancel = true;
                Process.Start(e.Url.ToString());
            }
        }

        private void DisplayMailtoPopup()
        {
            this.mailtoPopup.Location = new System.Drawing.Point(Cursor.Position.X - 20, Cursor.Position.Y - 80);
            this.mailtoPopup.Visible = true;
            this.mailtoPopupTimer.Start();
        }

        private void OnMailtoPopupTimeout(object source, EventArgs e)
        {
            this.mailtoPopup.Visible = false;
            this.mailtoPopupTimer.Stop();
        }
    }

    public partial class MailSyncEventHandler
    {
        ReceiveControl receiveCtrl = null;

        public MailSyncEventHandler(ReceiveControl ctrl)
        {
            this.receiveCtrl = ctrl;
            ReceiveObject.EmailsListChanged += new EventHandler(this.ListEmailsChanged);
        }

        private void ListEmailsChanged(object sender, EventArgs e)
        {
            this.receiveCtrl.BeginInvoke(this.receiveCtrl.ReadStoredEmailsDelegate);
        }
    }
}
