﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    partial class SendControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.sendEmailControlsTitle = new System.Windows.Forms.Label();
            this.recordDuration = new System.Windows.Forms.Label();
            this.ongoingRecordDuration = new System.Windows.Forms.Label();
            this.recordDurationLabel = new System.Windows.Forms.Label();
            this.ongoingRecordLabel = new System.Windows.Forms.Label();
            this.mp3Capture = new Istrib.Sound.Mp3SoundCapture(this.components);
            this.durationProgressBar = new System.Windows.Forms.ProgressBar();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.recipientNameLabel = new System.Windows.Forms.Label();
            this.recipientEmailLabel = new System.Windows.Forms.Label();
            this.recipientName = new System.Windows.Forms.Label();
            this.contactsPanel = new System.Windows.Forms.Panel();
            this.contactsListView = new System.Windows.Forms.ListView();
            this.upButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.sendEmailControlsPanel = new System.Windows.Forms.Panel();
            this.emailContentPanel = new System.Windows.Forms.Panel();
            this.keyboardButton = new System.Windows.Forms.Button();
            this.emailContent = new System.Windows.Forms.TextBox();
            this.emailTitle = new System.Windows.Forms.TextBox();
            this.sendPendingPicture = new System.Windows.Forms.PictureBox();
            this.removeAttachmentButton = new System.Windows.Forms.Button();
            this.attachmentsPanel = new System.Windows.Forms.Panel();
            this.attachmentsListView = new System.Windows.Forms.ListView();
            this.attachmentActionsPanel = new System.Windows.Forms.Panel();
            this.openAttachmentButton = new System.Windows.Forms.Button();
            this.previousAttachmentButton = new System.Windows.Forms.Button();
            this.nextAttachmentButton = new System.Windows.Forms.Button();
            this.addAttachmentButton = new System.Windows.Forms.Button();
            this.recipientEmailAddress = new System.Windows.Forms.TextBox();
            this.recipientPhoto = new System.Windows.Forms.PictureBox();
            this.cancelEmailButton = new System.Windows.Forms.Button();
            this.audioPlaybackStopButton = new System.Windows.Forms.Button();
            this.audioPlaybackPauseButton = new System.Windows.Forms.Button();
            this.audioPlaybackPlayButton = new System.Windows.Forms.Button();
            this.sendEmailButton = new System.Windows.Forms.Button();
            this.audioRecordStartButton = new System.Windows.Forms.Button();
            this.audioRecordStopButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.contactsPanel.SuspendLayout();
            this.sendEmailControlsPanel.SuspendLayout();
            this.emailContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sendPendingPicture)).BeginInit();
            this.attachmentsPanel.SuspendLayout();
            this.attachmentActionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recipientPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(100, 100);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // sendEmailControlsTitle
            // 
            this.sendEmailControlsTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.sendEmailControlsTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendEmailControlsTitle.Location = new System.Drawing.Point(0, 0);
            this.sendEmailControlsTitle.Name = "sendEmailControlsTitle";
            this.sendEmailControlsTitle.Size = new System.Drawing.Size(480, 19);
            this.sendEmailControlsTitle.TabIndex = 13;
			this.sendEmailControlsTitle.Text = "Send a message:";
            this.sendEmailControlsTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // recordDuration
            // 
            this.recordDuration.AutoSize = true;
            this.recordDuration.Location = new System.Drawing.Point(255, 324);
            this.recordDuration.Name = "recordDuration";
            this.recordDuration.Size = new System.Drawing.Size(58, 16);
            this.recordDuration.TabIndex = 15;
            this.recordDuration.Text = "00:00:00";
            this.recordDuration.Visible = false;
            // 
            // ongoingRecordDuration
            // 
            this.ongoingRecordDuration.Location = new System.Drawing.Point(10, 344);
            this.ongoingRecordDuration.Name = "ongoingRecordDuration";
            this.ongoingRecordDuration.Size = new System.Drawing.Size(460, 16);
            this.ongoingRecordDuration.TabIndex = 15;
            this.ongoingRecordDuration.Text = "00:00:00";
            this.ongoingRecordDuration.Visible = false;
            this.ongoingRecordDuration.Font = new System.Drawing.Font("Arial", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ongoingRecordDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ongoingRecordDuration.ForeColor = System.Drawing.Color.Red;
            // 
            // recordDurationLabel
            // 
            this.recordDurationLabel.AutoSize = true;
            this.recordDurationLabel.Location = new System.Drawing.Point(199, 324);
            this.recordDurationLabel.Name = "recordDurationLabel";
            this.recordDurationLabel.Size = new System.Drawing.Size(50, 16);
            this.recordDurationLabel.TabIndex = 14;
			this.recordDurationLabel.Text = "Length:";
            this.recordDurationLabel.Visible = false;
            // 
            // ongoingRecordLabel
            // 
            this.ongoingRecordLabel.Location = new System.Drawing.Point(10, 324);
            this.ongoingRecordLabel.Name = "ongoingRecordLabel";
            this.ongoingRecordLabel.Size = new System.Drawing.Size(460, 16);
            this.ongoingRecordLabel.TabIndex = 14;
            this.ongoingRecordLabel.Text = "Recording...";
            this.ongoingRecordLabel.Visible = false;
            this.ongoingRecordLabel.Font = new System.Drawing.Font("Arial", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ongoingRecordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ongoingRecordLabel.ForeColor = System.Drawing.Color.Red;
            // 
            // mp3Capture
            // 
            this.mp3Capture.NormalizeVolume = false;
            this.mp3Capture.OutputType = Istrib.Sound.Mp3SoundCapture.Outputs.Mp3;
            this.mp3Capture.UseSynchronizationContext = true;
            this.mp3Capture.WaitOnStop = true;
            this.mp3Capture.Started += new System.EventHandler(this.Mp3Capture_Started);
            this.mp3Capture.Starting += new System.EventHandler(this.Mp3Capture_Starting);
            this.mp3Capture.Stopped += new System.EventHandler<Istrib.Sound.Mp3SoundCapture.StoppedEventArgs>(this.Mp3Capture_Stopped);
            this.mp3Capture.Stopping += new System.EventHandler(this.Mp3Capture_Stopping);
            // 
            // durationProgressBar
            // 
            this.durationProgressBar.Location = new System.Drawing.Point(4, 344);
            this.durationProgressBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.durationProgressBar.Name = "durationProgressBar";
            this.durationProgressBar.Size = new System.Drawing.Size(467, 19);
            this.durationProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.durationProgressBar.TabIndex = 20;
            this.durationProgressBar.Visible = false;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // recipientNameLabel
            // 
            this.recipientNameLabel.Location = new System.Drawing.Point(144, 34);
            this.recipientNameLabel.Name = "recipientNameLabel";
            this.recipientNameLabel.Size = new System.Drawing.Size(49, 16);
            this.recipientNameLabel.TabIndex = 24;
			this.recipientNameLabel.Text = "Name:";
            this.recipientNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // emailLabel
            // 
            this.recipientEmailLabel.Location = new System.Drawing.Point(144, 73);
            this.recipientEmailLabel.Name = "recipientEmailLabel";
            this.recipientEmailLabel.Size = new System.Drawing.Size(49, 16);
            this.recipientEmailLabel.TabIndex = 25;
            this.recipientEmailLabel.Text = "Email:";
            this.recipientEmailLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // recipientName
            // 
            this.recipientName.AutoSize = true;
            this.recipientName.Location = new System.Drawing.Point(193, 34);
            this.recipientName.Name = "recipientName";
            this.recipientName.Size = new System.Drawing.Size(0, 16);
            this.recipientName.TabIndex = 26;
            // 
            // contactsPanel
            // 
            this.contactsPanel.Controls.Add(this.contactsListView);
            this.contactsPanel.Controls.Add(this.upButton);
            this.contactsPanel.Controls.Add(this.downButton);
            this.contactsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contactsPanel.Location = new System.Drawing.Point(0, 0);
            this.contactsPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.contactsPanel.Name = "contactsPanel";
            this.contactsPanel.Size = new System.Drawing.Size(544, 606);
            this.contactsPanel.TabIndex = 30;
            // 
            // contactsList
            // 
            this.contactsListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.contactsListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.contactsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contactsListView.ForeColor = System.Drawing.SystemColors.WindowText;
            this.contactsListView.GridLines = true;
            this.contactsListView.HideSelection = false;
            this.contactsListView.LargeImageList = this.imageList;
            this.contactsListView.Location = new System.Drawing.Point(0, 47);
            this.contactsListView.MultiSelect = false;
            this.contactsListView.Name = "contactsListView";
            this.contactsListView.Size = new System.Drawing.Size(544, 512);
            this.contactsListView.SmallImageList = this.imageList;
            this.contactsListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.contactsListView.TabIndex = 5;
            this.contactsListView.TileSize = new System.Drawing.Size(101, 101);
            this.contactsListView.UseCompatibleStateImageBehavior = false;
            //this.contactsListView.SelectedIndexChanged += new System.EventHandler(this.ContactsListView_SelectedIndexChanged);
            this.contactsListView.Click += new System.EventHandler(this.ContactsListView_Click);
            // 
            // upButton
            // 
            this.upButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.upButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.upButton.Image = Mail2Voice.properties.Resources.arrow_up;
            this.upButton.Location = new System.Drawing.Point(0, 0);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(544, 47);
            this.upButton.TabIndex = 1;
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.UpButton_Click);
            // 
            // downButton
            // 
            this.downButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.downButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.downButton.Image = Mail2Voice.properties.Resources.arrow_dw;
            this.downButton.Location = new System.Drawing.Point(0, 559);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(544, 47);
            this.downButton.TabIndex = 0;
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.DownButton_Click);
            // 
            // sendEmailControlsPanel
            // 
            this.sendEmailControlsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(251)))), ((int)(((byte)(237)))));
            this.sendEmailControlsPanel.Controls.Add(this.emailContentPanel);
            this.sendEmailControlsPanel.Controls.Add(this.emailTitle);
            this.sendEmailControlsPanel.Controls.Add(this.sendPendingPicture);
            this.sendEmailControlsPanel.Controls.Add(this.removeAttachmentButton);
            this.sendEmailControlsPanel.Controls.Add(this.attachmentsPanel);
            this.sendEmailControlsPanel.Controls.Add(this.addAttachmentButton);
            this.sendEmailControlsPanel.Controls.Add(this.recipientEmailAddress);
            this.sendEmailControlsPanel.Controls.Add(this.sendEmailControlsTitle);
            this.sendEmailControlsPanel.Controls.Add(this.recipientPhoto);
            this.sendEmailControlsPanel.Controls.Add(this.cancelEmailButton);
            this.sendEmailControlsPanel.Controls.Add(this.recipientNameLabel);
            this.sendEmailControlsPanel.Controls.Add(this.durationProgressBar);
            this.sendEmailControlsPanel.Controls.Add(this.recipientName);
            this.sendEmailControlsPanel.Controls.Add(this.audioPlaybackStopButton);
            this.sendEmailControlsPanel.Controls.Add(this.recipientEmailLabel);
            this.sendEmailControlsPanel.Controls.Add(this.audioPlaybackPauseButton);
            this.sendEmailControlsPanel.Controls.Add(this.audioPlaybackPlayButton);
            this.sendEmailControlsPanel.Controls.Add(this.recordDurationLabel);
            this.sendEmailControlsPanel.Controls.Add(this.ongoingRecordLabel);
            this.sendEmailControlsPanel.Controls.Add(this.recordDuration);
            this.sendEmailControlsPanel.Controls.Add(this.ongoingRecordDuration);
            this.sendEmailControlsPanel.Controls.Add(this.sendEmailButton);
            this.sendEmailControlsPanel.Controls.Add(this.audioRecordStartButton);
            this.sendEmailControlsPanel.Controls.Add(this.audioRecordStopButton);
            this.sendEmailControlsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.sendEmailControlsPanel.Location = new System.Drawing.Point(544, 0);
            this.sendEmailControlsPanel.Name = "sendEmailControlsPanel";
            this.sendEmailControlsPanel.Size = new System.Drawing.Size(480, 606);
            this.sendEmailControlsPanel.TabIndex = 31;
            // 
            // emailContentPanel
            // 
            this.emailContentPanel.Controls.Add(this.keyboardButton);
            this.emailContentPanel.Controls.Add(this.emailContent);
            this.emailContentPanel.Location = new System.Drawing.Point(9, 155);
            this.emailContentPanel.Name = "emailContentPanel";
            this.emailContentPanel.Size = new System.Drawing.Size(464, 92);
            this.emailContentPanel.TabIndex = 36;
            // 
            // keyboardButton
            // 
            this.keyboardButton.Image = Mail2Voice.properties.Resources.keyboard_32;
            this.keyboardButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.keyboardButton.Location = new System.Drawing.Point(377, 56);
            this.keyboardButton.Name = "button1";
            this.keyboardButton.Size = new System.Drawing.Size(87, 33);
            this.keyboardButton.TabIndex = 33;
			this.keyboardButton.Text = "Keyboard";
            this.keyboardButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.keyboardButton.UseVisualStyleBackColor = true;
            this.keyboardButton.Click += new System.EventHandler(this.KeyboardButton_Click);
            // 
            // emailContent
            // 
            this.emailContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emailContent.Location = new System.Drawing.Point(0, 0);
            this.emailContent.Multiline = true;
            this.emailContent.Name = "emailContent";
            this.emailContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.emailContent.Size = new System.Drawing.Size(464, 92);
            this.emailContent.TabIndex = 35;
            this.emailContent.TextChanged += new System.EventHandler(this.ContentText_Changed);
            // 
            // emailTitle
            // 
            this.emailTitle.Location = new System.Drawing.Point(146, 126);
            this.emailTitle.Name = "emailTitle";
            this.emailTitle.Size = new System.Drawing.Size(325, 22);
            this.emailTitle.TabIndex = 34;
            this.emailTitle.TextChanged += new System.EventHandler(this.ContentText_Changed);
            // 
            // sendPendingPicture
            // 
            this.sendPendingPicture.Image = Mail2Voice.properties.Resources.working_48_B;
            this.sendPendingPicture.Location = new System.Drawing.Point(302, 511);
            this.sendPendingPicture.Name = "sendPendingPicture";
            this.sendPendingPicture.Size = new System.Drawing.Size(48, 48);
            this.sendPendingPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.sendPendingPicture.TabIndex = 32;
            this.sendPendingPicture.TabStop = false;
            this.sendPendingPicture.Visible = false;
            // 
            // removeAttachmentButton
            // 
            this.removeAttachmentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.removeAttachmentButton.Image = Mail2Voice.properties.Resources.no3;
            this.removeAttachmentButton.Location = new System.Drawing.Point(4, 440);
            this.removeAttachmentButton.Name = "removeAttachmentButton";
            this.removeAttachmentButton.Size = new System.Drawing.Size(64, 64);
            this.removeAttachmentButton.TabIndex = 31;
			this.removeAttachmentButton.Text = "Remove";
            this.removeAttachmentButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.removeAttachmentButton.UseVisualStyleBackColor = true;
            this.removeAttachmentButton.Click += new System.EventHandler(this.RemoveAttachmentButton_Click);
            // 
            // attachmentsPanel
            // 
            this.attachmentsPanel.Controls.Add(this.attachmentsListView);
            this.attachmentsPanel.Controls.Add(this.attachmentActionsPanel);
            this.attachmentsPanel.Location = new System.Drawing.Point(74, 370);
            this.attachmentsPanel.Name = "attachmentsPanel";
            this.attachmentsPanel.Size = new System.Drawing.Size(399, 134);
            this.attachmentsPanel.TabIndex = 30;
            // 
            // attachmentsListView
            // 
            this.attachmentsListView.Enabled = false;
            this.attachmentsListView.Location = new System.Drawing.Point(0, 0);
            this.attachmentsListView.MultiSelect = false;
            this.attachmentsListView.Name = "attachmentsListView";
            this.attachmentsListView.Size = new System.Drawing.Size(319, 134);
            this.attachmentsListView.TabIndex = 0;
            this.attachmentsListView.UseCompatibleStateImageBehavior = false;
            // 
            // attachmentActionsPanel
            // 
            this.attachmentActionsPanel.Controls.Add(this.openAttachmentButton);
            this.attachmentActionsPanel.Controls.Add(this.previousAttachmentButton);
            this.attachmentActionsPanel.Controls.Add(this.nextAttachmentButton);
            this.attachmentActionsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.attachmentActionsPanel.Location = new System.Drawing.Point(325, 0);
            this.attachmentActionsPanel.Name = "panel7";
            this.attachmentActionsPanel.Size = new System.Drawing.Size(74, 134);
            this.attachmentActionsPanel.TabIndex = 5;
            // 
            // openAttachmentButton
            // 
            this.openAttachmentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openAttachmentButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.openAttachmentButton.Enabled = false;
            this.openAttachmentButton.Image = Mail2Voice.properties.Resources.folder;
            this.openAttachmentButton.Location = new System.Drawing.Point(0, 41);
            this.openAttachmentButton.Name = "openAttachmentButton";
            this.openAttachmentButton.Size = new System.Drawing.Size(74, 52);
            this.openAttachmentButton.TabIndex = 5;
            this.openAttachmentButton.UseVisualStyleBackColor = true;
            this.openAttachmentButton.Click += new System.EventHandler(this.OpenAttachmentButton_Click);
            // 
            // previousAttachmentButton
            // 
            this.previousAttachmentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.previousAttachmentButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.previousAttachmentButton.Enabled = false;
            this.previousAttachmentButton.Image = Mail2Voice.properties.Resources.arrow_up;
            this.previousAttachmentButton.Location = new System.Drawing.Point(0, 0);
            this.previousAttachmentButton.Name = "previousAttachmentButton";
            this.previousAttachmentButton.Size = new System.Drawing.Size(74, 41);
            this.previousAttachmentButton.TabIndex = 3;
            this.previousAttachmentButton.UseVisualStyleBackColor = true;
            this.previousAttachmentButton.Click += new System.EventHandler(this.PreviousAttachmentButton_Click);
            // 
            // nextAttachmentButton
            // 
            this.nextAttachmentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nextAttachmentButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nextAttachmentButton.Enabled = false;
            this.nextAttachmentButton.Image = Mail2Voice.properties.Resources.arrow_dw;
            this.nextAttachmentButton.Location = new System.Drawing.Point(0, 93);
            this.nextAttachmentButton.Name = "nextAttachmentButton";
            this.nextAttachmentButton.Size = new System.Drawing.Size(74, 41);
            this.nextAttachmentButton.TabIndex = 4;
            this.nextAttachmentButton.UseVisualStyleBackColor = true;
            this.nextAttachmentButton.Click += new System.EventHandler(this.NextAttachmentButton_Click);
            // 
            // addAttachmentButton
            // 
            this.addAttachmentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addAttachmentButton.Image = Mail2Voice.properties.Resources.mail_attach2;
            this.addAttachmentButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.addAttachmentButton.Location = new System.Drawing.Point(4, 370);
            this.addAttachmentButton.Name = "addAttachmentButton";
            this.addAttachmentButton.Size = new System.Drawing.Size(64, 64);
            this.addAttachmentButton.TabIndex = 29;
			this.addAttachmentButton.Text = "Attach";
            this.addAttachmentButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addAttachmentButton.UseVisualStyleBackColor = true;
            this.addAttachmentButton.Click += new System.EventHandler(this.AddAttachmentButton_Click);
            // 
            // recipientEmailAddress
            // 
            this.recipientEmailAddress.Location = new System.Drawing.Point(196, 70);
            this.recipientEmailAddress.Name = "recipientEmailAddress";
            this.recipientEmailAddress.Size = new System.Drawing.Size(275, 22);
            this.recipientEmailAddress.TabIndex = 28;
            this.recipientEmailAddress.TextChanged += new System.EventHandler(this.ContentText_Changed);
            // 
            // recipientPhoto
            // 
            this.recipientPhoto.BackColor = System.Drawing.Color.White;
            this.recipientPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.recipientPhoto.Location = new System.Drawing.Point(10, 25);
            this.recipientPhoto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.recipientPhoto.Name = "recipientPhoto";
            this.recipientPhoto.Size = new System.Drawing.Size(117, 123);
            this.recipientPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.recipientPhoto.TabIndex = 21;
            this.recipientPhoto.TabStop = false;
            // 
            // cancelEmailButton
            // 
            this.cancelEmailButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancelEmailButton.Image = Mail2Voice.properties.Resources.no31;
            this.cancelEmailButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cancelEmailButton.Location = new System.Drawing.Point(4, 511);
            this.cancelEmailButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cancelEmailButton.Name = "cancelEmailButton";
            this.cancelEmailButton.Size = new System.Drawing.Size(250, 86);
            this.cancelEmailButton.TabIndex = 22;
			this.cancelEmailButton.Text = "Cancel";
            this.cancelEmailButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cancelEmailButton.UseVisualStyleBackColor = true;
            this.cancelEmailButton.Click += new System.EventHandler(this.CancelEmailButton_Click);
            // 
            // audioPlaybackStopButton
            // 
            this.audioPlaybackStopButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.audioPlaybackStopButton.Enabled = false;
            this.audioPlaybackStopButton.Image = Mail2Voice.properties.Resources.media_stop;
            this.audioPlaybackStopButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.audioPlaybackStopButton.Location = new System.Drawing.Point(409, 254);
            this.audioPlaybackStopButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.audioPlaybackStopButton.Name = "audioPlaybackStopButton";
            this.audioPlaybackStopButton.Size = new System.Drawing.Size(64, 64);
            this.audioPlaybackStopButton.TabIndex = 19;
            this.audioPlaybackStopButton.Text = "Stop";
            this.audioPlaybackStopButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.audioPlaybackStopButton.UseVisualStyleBackColor = true;
            this.audioPlaybackStopButton.Click += new System.EventHandler(this.AudioPlaybackStopButton_Click);
            // 
            // audioPlaybackPauseButton
            // 
            this.audioPlaybackPauseButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.audioPlaybackPauseButton.Enabled = false;
            this.audioPlaybackPauseButton.Image = Mail2Voice.properties.Resources.media_pause;
            this.audioPlaybackPauseButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.audioPlaybackPauseButton.Location = new System.Drawing.Point(335, 254);
            this.audioPlaybackPauseButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.audioPlaybackPauseButton.Name = "audioPlaybackPauseButton";
            this.audioPlaybackPauseButton.Size = new System.Drawing.Size(64, 64);
            this.audioPlaybackPauseButton.TabIndex = 18;
            this.audioPlaybackPauseButton.Text = "Pause";
            this.audioPlaybackPauseButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.audioPlaybackPauseButton.UseVisualStyleBackColor = true;
            this.audioPlaybackPauseButton.Click += new System.EventHandler(this.AudioPlaybackPauseButton_Click);
            // 
            // audioPlaybackPlayButton
            // 
            this.audioPlaybackPlayButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.audioPlaybackPlayButton.Enabled = false;
            this.audioPlaybackPlayButton.Image = Mail2Voice.properties.Resources.media_play1;
            this.audioPlaybackPlayButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.audioPlaybackPlayButton.Location = new System.Drawing.Point(264, 254);
            this.audioPlaybackPlayButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.audioPlaybackPlayButton.Name = "audioPlaybackPlayButton";
            this.audioPlaybackPlayButton.Size = new System.Drawing.Size(64, 64);
            this.audioPlaybackPlayButton.TabIndex = 17;
			this.audioPlaybackPlayButton.Text = "Play";
            this.audioPlaybackPlayButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.audioPlaybackPlayButton.UseVisualStyleBackColor = true;
            this.audioPlaybackPlayButton.Click += new System.EventHandler(this.AudioPlaybackPlayButton_Click);
            // 
            // sendEmailButton
            // 
            this.sendEmailButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendEmailButton.Image = Mail2Voice.properties.Resources.ok3;
            this.sendEmailButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.sendEmailButton.Location = new System.Drawing.Point(264, 511);
            this.sendEmailButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sendEmailButton.Name = "sendEmailButton";
            this.sendEmailButton.Size = new System.Drawing.Size(209, 86);
            this.sendEmailButton.TabIndex = 23;
			this.sendEmailButton.Text = "Send";
            this.sendEmailButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.sendEmailButton.UseVisualStyleBackColor = true;
            this.sendEmailButton.Click += new System.EventHandler(this.SendEmailButton_Click);
            // 
            // audioRecordStartButton
            // 
            this.audioRecordStartButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.audioRecordStartButton.Image = Mail2Voice.properties.Resources.micro;
            this.audioRecordStartButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.audioRecordStartButton.Location = new System.Drawing.Point(5, 254);
            this.audioRecordStartButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.audioRecordStartButton.Name = "audioRecordStartButton";
            this.audioRecordStartButton.Size = new System.Drawing.Size(250, 64);
            this.audioRecordStartButton.TabIndex = 16;
			this.audioRecordStartButton.Text = "Record";
            this.audioRecordStartButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.audioRecordStartButton.UseVisualStyleBackColor = true;
            this.audioRecordStartButton.Click += new System.EventHandler(this.AudioRecordStartButton_Click);
            // 
            // audioRecordStopButton
            // 
            this.audioRecordStopButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.audioRecordStopButton.Image = Mail2Voice.properties.Resources.media_stop_32;
            this.audioRecordStopButton.Location = new System.Drawing.Point(4, 254);
            this.audioRecordStopButton.Name = "audioRecordStopButton";
            this.audioRecordStopButton.Size = new System.Drawing.Size(250, 64);
            this.audioRecordStopButton.TabIndex = 37;
			this.audioRecordStopButton.Text = "Recording...";
            this.audioRecordStopButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.audioRecordStopButton.UseVisualStyleBackColor = true;
            this.audioRecordStopButton.Visible = false;
            this.audioRecordStopButton.Click += new System.EventHandler(this.AudioRecordStopButton_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "*.*";
			this.openFileDialog.Filter = "All files|*.*";
			this.openFileDialog.Title = "File to open";
            // 
            // sendControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.contactsPanel);
            this.Controls.Add(this.sendEmailControlsPanel);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "sendControl";
            this.Size = new System.Drawing.Size(1024, 606);
            this.contactsPanel.ResumeLayout(false);
            this.sendEmailControlsPanel.ResumeLayout(false);
            this.sendEmailControlsPanel.PerformLayout();
            this.emailContentPanel.ResumeLayout(false);
            this.emailContentPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sendPendingPicture)).EndInit();
            this.attachmentsPanel.ResumeLayout(false);
            this.attachmentActionsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recipientPhoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label sendEmailControlsTitle;
        private System.Windows.Forms.Button audioPlaybackPauseButton;
        private System.Windows.Forms.Button audioPlaybackPlayButton;
        private System.Windows.Forms.Button audioRecordStartButton;
        private System.Windows.Forms.Label recordDuration;
        private System.Windows.Forms.Label ongoingRecordDuration;
        private System.Windows.Forms.Label recordDurationLabel;
        private System.Windows.Forms.Label ongoingRecordLabel;
        private System.Windows.Forms.ImageList imageList;
        private Istrib.Sound.Mp3SoundCapture mp3Capture;
        private System.Windows.Forms.Button audioPlaybackStopButton;
        private System.Windows.Forms.ProgressBar durationProgressBar;
        private System.Windows.Forms.PictureBox recipientPhoto;
        private System.Windows.Forms.Button cancelEmailButton;
        private System.Windows.Forms.Button sendEmailButton;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label recipientNameLabel;
        private System.Windows.Forms.Label recipientEmailLabel;
        private System.Windows.Forms.Label recipientName;
        private System.Windows.Forms.Panel contactsPanel;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.ListView contactsListView;
        private System.Windows.Forms.Panel sendEmailControlsPanel;
        private System.Windows.Forms.Button addAttachmentButton;
        private System.Windows.Forms.Button removeAttachmentButton;
        private System.Windows.Forms.Panel attachmentsPanel;
        private System.Windows.Forms.ListView attachmentsListView;
        private System.Windows.Forms.Panel attachmentActionsPanel;
        private System.Windows.Forms.Button openAttachmentButton;
        private System.Windows.Forms.Button previousAttachmentButton;
        private System.Windows.Forms.Button nextAttachmentButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.PictureBox sendPendingPicture;
        private System.Windows.Forms.Panel emailContentPanel;
        private System.Windows.Forms.Button audioRecordStopButton;
        private System.Windows.Forms.Button keyboardButton;
        protected internal System.Windows.Forms.TextBox recipientEmailAddress;
        protected internal System.Windows.Forms.TextBox emailTitle;
        protected internal System.Windows.Forms.TextBox emailContent;

    }
}
