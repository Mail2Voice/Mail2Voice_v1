﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Panels
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using Istrib.Sound;
    using Istrib.Sound.Formats;
    using Mail2VoiceCore;
    using Microsoft.DirectX.AudioVideoPlayback;

    public partial class SendControl : UserControl
    {
        /***************************
         *      Attributes
         ***************************/
        private long timeDelay = 0;
        private string tmpVoiceMessageFilename = string.Empty;
        private Audio music = null;
        private string musicDuration = string.Empty;

        private SentMailEventHandler mySentMailEventHandler = null;

        /***************************
         *      Constructor
         ***************************/
        public SendControl()
        {
            this.InitializeComponent();

            SpeechObject.Singleton.AddVoiceString("errorCancelEmail", "Error email cancel.");
            SpeechObject.Singleton.AddVoiceString("errorSendEmail", "Email sending failed.");
            SpeechObject.Singleton.AddVoiceString("endSendEmail", "Email sent.");
            SpeechObject.Singleton.AddVoiceString("send", "Send email.");
            SpeechObject.Singleton.AddVoiceString("specifyEmail", "Please specify the recipient email address.");

            this.UpdateContent();
            
            this.keyboardButton.Visible = false;
            if (Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_ACTIVATE_KEYBOARD)))
            {
                this.keyboardButton.Visible = true;
            }

            this.sendEmailButton.Enabled = false;
            this.attachmentsListView.HideSelection = false;

            this.mySentMailEventHandler = new SentMailEventHandler(this);

            this.SendEmailCompletedDelegate = new SendEmailCompletedDelegateT(this.SendEmailCompletedHandler);
            this.SendEmailErrorDelegate = new SendEmailErrorDelegateT(this.SendEmailErrorHandler);
        }

        /***************************
         *      Private delegates
         ***************************/
        private delegate void SetSendingStateVisible(bool visible);

        private delegate void ChangeButtons(bool state);

        /***************************
         *      Public methods
         ***************************/
        public delegate void SendEmailCompletedDelegateT();
        public delegate void SendEmailErrorDelegateT();

        public SendEmailCompletedDelegateT SendEmailCompletedDelegate;
        public SendEmailErrorDelegateT SendEmailErrorDelegate;

        public void SetImageTypeMime(ImageList listImage)
        {
            this.attachmentsListView.LargeImageList = listImage;
            this.attachmentsListView.SmallImageList = listImage;
        }

        public void SetEmail(string email)
        {
            this.EmptySelection();
            Contact recipient = ContactsObject.Singleton.GetContact(email);

            if (recipient != null)
            {
                for (int x = 0; x < this.contactsListView.Items.Count; x++)
                {
                    if (this.contactsListView.Items[x].Tag.ToString().Equals(email))
                    {
                        this.contactsListView.Items[x].Selected = true;
                        this.ContactsListView_SelectedIndexChanged(null, null);
                        return;
                    }
                }

                this.recipientEmailAddress.Text = recipient.Email;
                this.recipientName.Text = recipient.Name;
                this.recipientPhoto.Image = recipient.Image != null ? recipient.Image : Mail2Voice.properties.Resources.user_known;
            }
            else
            {
                this.recipientEmailAddress.Text = email;
                this.recipientPhoto.Image = Mail2Voice.properties.Resources.user_unknow;
            }
        }

        public void UpdateContent()
        {
            if (this.Parent != null && this.Parent.Created
                && ((MainForm)this.Parent).EditingEmail)
            {
                return;
            }

            try
            {
                this.EmptySelection();
                this.UpdateContactsList();
                    
                this.recipientPhoto.Image = null;
                this.emailTitle.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_SUBJECT);
                this.emailContent.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_CONTENT);

                if (this.Parent != null && this.Parent.Created)
                {
                    ((MainForm)this.Parent).EditingEmail = false;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        public void UpdateDefaultMessage()
        {
            this.emailTitle.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_SUBJECT);
            this.emailContent.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_CONTENT);
        }

        public bool ContactInUse(string emailAddress)
        {
            return this.recipientEmailAddress.Text == emailAddress;
        }

        public void UpdateContactsList()
        {
            this.contactsListView.Items.Clear();
            this.imageList.Images.Clear();
            List<Contact> contactsList = ContactsObject.Singleton.GetContactsList();

            if (contactsList != null)
            {
                for (int x = 0; x < contactsList.Count; x++)
                {
                    this.imageList.Images.Add(contactsList[x].Email, contactsList[x].Image == null ? Mail2Voice.properties.Resources.user_known : contactsList[x].Image);
                    this.contactsListView.Items.Add(new ListViewItem(contactsList[x].Name, contactsList[x].Email) { Tag = contactsList[x].Email });
                }
            }
        }

        public void UpdateCurrentContact(Contact contact)
        {
            this.recipientName.Text = contact.Name;
            this.recipientEmailAddress.Text = contact.Email;
            this.recipientPhoto.Image = contact.Image != null ? contact.Image : Mail2Voice.properties.Resources.user_known;
        }

        public void StopRecording()
        {
            if (this.mp3Capture.Capturing)
            {
                this.mp3Capture.Stop();
            }
        }

        public void HandleMailTo(string address)
        {
            this.SetEmail(address);
        }

        /********************************
         *      Public event handlers
         ********************************/    
        public void SendEmailErrorHandler()
        {
            this.SetPendingPictureVisible(false);
            this.ChangeButtonsState(true);

            SpeechObject.Singleton.Speak(SpeechObject.Singleton.GetVoiceString("errorSendEmail"));
        }

        public void SendEmailCompletedHandler()
        {
            this.SetPendingPictureVisible(false);
            this.ChangeButtonsState(true);

            // Clear previous email
            this.CancelEmailButton_Click(null, null);
            this.emailTitle.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_SUBJECT);
            this.emailContent.Text = ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_CONTENT);
            this.attachmentsListView.Clear();
            this.recipientName.Text = string.Empty;
            SpeechObject.Singleton.Speak(SpeechObject.Singleton.GetVoiceString("endSendEmail"));
            FileObject.Singleton.DeleteMp3Files();
            this.sendEmailButton.Enabled = false;
        }

        /********************************
         *      Protected event handlers
         ********************************/   
        protected internal void AudioRecordStartButton_Click(object sender, EventArgs e)
        {
            try
            {
                ((MainForm)this.Parent).SetRecordMode(true);

                this.mp3Capture.CaptureDevice = SoundCaptureDevice.AllAvailable.ElementAt(1);
                this.mp3Capture.NormalizeVolume = true;
                this.mp3Capture.OutputType = Mp3SoundCapture.Outputs.Mp3;

                this.mp3Capture.WaveFormat = Mp3SoundFormat.AllSourceFormats.ElementAt(0);
                this.mp3Capture.Mp3BitRate = Mp3BitRate.BitRate32;

                if (this.music != null)
                {
                    this.music.Stop();
                    this.music.Dispose();
                    this.music = null;
                }

                this.tmpVoiceMessageFilename = "Message_" + DateTime.Now.Year.ToString() + "-" +
                    DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00") +
                    "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00") +
                               ".mp3";
                this.mp3Capture.WaitOnStop = true;
                this.mp3Capture.Start(this.tmpVoiceMessageFilename);

                this.audioRecordStartButton.Hide();
                this.audioRecordStopButton.Show();
                this.recordDuration.Visible = false;
                this.recordDurationLabel.Visible = false;
                this.durationProgressBar.Visible = false;
                this.ongoingRecordLabel.Visible = true;
                this.ongoingRecordDuration.Visible = true;
                this.timer.Interval = 1000;
                this.timer.Enabled = true;
                this.durationProgressBar.Value = 0;
                this.durationProgressBar.Maximum = 100;
                this.timeDelay = 0;
                this.EnableButtons(false);
                this.ContentText_Changed(null, null);
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void AudioPlaybackPlayButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.music == null && File.Exists(this.tmpVoiceMessageFilename))
                {
                    this.music = new Audio(this.tmpVoiceMessageFilename);
                    this.music.Ending += new System.EventHandler(this.AudioPlaybackEnding);
                    this.durationProgressBar.Value = 0;
                    this.timer.Interval = 100;

                    this.durationProgressBar.Maximum = Convert.ToInt32(this.music.Duration * 1000);
                }

                this.timer.Start();
                this.music.Play();
                this.audioPlaybackPlayButton.Enabled = false;
                this.audioPlaybackPauseButton.Enabled = true;
                this.audioPlaybackStopButton.Enabled = true;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void AudioPlaybackPauseButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.music != null && this.music.State == StateFlags.Running)
                {
                    this.music.Pause();
                    this.audioPlaybackPlayButton.Enabled = true;
                    this.audioPlaybackPauseButton.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void AudioPlaybackStopButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.music != null && this.music.State != StateFlags.Stopped)
                {
                    this.timer.Stop();
                    this.music.Stop();
                    this.audioPlaybackPlayButton.Enabled = true;
                    this.audioPlaybackPauseButton.Enabled = false;
                    this.audioPlaybackStopButton.Enabled = false;
                    this.durationProgressBar.Value = 0;
                    this.recordDuration.Text = this.musicDuration;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void CancelEmailButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.mp3Capture.Capturing)
                {
                    this.mp3Capture.Stop();
                    this.timer.Enabled = false;
                    this.audioRecordStopButton.Hide();
                    this.audioRecordStartButton.Show();
                }

                if (this.music != null)
                {
                    this.music.Stop();
                }

                this.timer.Stop();
                this.ongoingRecordLabel.Visible = false;
                this.ongoingRecordDuration.Visible = false;
                this.recordDuration.Visible = false;
                this.recordDurationLabel.Visible = false;
                this.durationProgressBar.Visible = false;
                ((MainForm)this.Parent).SetRecordMode(false);

                FileObject.Singleton.DeleteMp3Files();
                this.recordDuration.Text = "00:00:00";
                this.ongoingRecordDuration.Text = "00:00:00";
                this.EnableButtons(false);
                this.sendEmailButton.Show();
                this.sendPendingPicture.Hide();

                if (this.Parent != null && this.Parent.Created
                    && ((MainForm)this.Parent).EditingEmail)
                {
                    ((MainForm)this.Parent).EditingEmail = false;
                }

                this.attachmentsListView.Clear();
                this.UpdateContent();
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void SendEmailButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Send email
                if (!string.IsNullOrEmpty(this.recipientEmailAddress.Text))
                {
                    SpeechObject.Singleton.Speak(SpeechObject.Singleton.GetVoiceString("send"));
                    this.sendPendingPicture.Visible = true;
                    this.Invoke(new ChangeButtons(this.ChangeButtonsState), false);
                    ((MainForm)this.Parent).EditingEmail = false;

                    if (this.attachmentsListView.Items.Count == 0)
                    {
                        SendObject.Singleton.SendMail(this.recipientName.Text, this.recipientEmailAddress.Text, this.emailTitle.Text, this.emailContent.Text, Path.GetFileName(this.tmpVoiceMessageFilename), Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_SSL)));
                    }
                    else
                    {
                        List<string> fileList = new List<string>();
                        fileList.Add(Path.GetFileName(this.tmpVoiceMessageFilename));

                        for (int x = 0; x < this.attachmentsListView.Items.Count; x++)
                        {
                            fileList.Add(this.attachmentsListView.Items[x].Tag.ToString());
                        }
                            
                        SendObject.Singleton.SendMailMultiAttach(this.recipientName.Text, this.recipientEmailAddress.Text, this.emailTitle.Text, this.emailContent.Text, fileList, Convert.ToBoolean(ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SMTP_SSL)));

                        this.tmpVoiceMessageFilename = null;
                    }
                }
                else
                {
                    SpeechObject.Singleton.Speak(SpeechObject.Singleton.GetVoiceString("specifyEmail"));
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void UpButton_Click(object sender, EventArgs e)
        {
            try
            {
                int pos = this.GetSelectedIndex();
                int index = pos;

                if (pos == -1)
                {
                    index = 0;
                }
                else if (pos - 1 >= 0)
                {
                    index = pos - 1;
                }

                this.contactsListView.Items[index].Selected = true;
                this.contactsListView.Items[index].Focused = true;
                this.contactsListView.Items[index].EnsureVisible();
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void DownButton_Click(object sender, EventArgs e)
        {
            try
            {
                int pos = this.GetSelectedIndex();
                int index = pos;

                if (pos == -1)
                {
                    index = 0;
                }
                else if (pos + 1 < this.contactsListView.Items.Count)
                {
                    index = pos + 1;
                }

                this.contactsListView.Items[index].Selected = true;
                this.contactsListView.Items[index].Focused = true;
                this.contactsListView.Items[index].EnsureVisible();
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void PreviousAttachmentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.attachmentsListView.Items.Count > 0)
                {
                    int index = this.GetSelectedIndexPiece();

                    if (index > 0)
                    {
                        index--;
                    }
                    else
                    {
                        index = 0;
                    }

                    this.attachmentsListView.Items[index].Focused = true;
                    this.attachmentsListView.Items[index].Selected = true;
                    this.attachmentsListView.EnsureVisible(index);
                    this.attachmentsListView.Select();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void NextAttachmentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.attachmentsListView.Items.Count > 0)
                {
                    int index = this.GetSelectedIndexPiece();

                    if (index + 1 < this.attachmentsListView.Items.Count)
                    {
                        index++;
                    }

                    this.attachmentsListView.Items[index].Focused = true;
                    this.attachmentsListView.Items[index].Selected = true;
                    this.attachmentsListView.EnsureVisible(index);
                    this.attachmentsListView.Select();
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void OpenAttachmentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.attachmentsListView.SelectedItems.Count > 0)
                {
                    if (this.attachmentsListView.SelectedItems[0].Tag.ToString() != null)
                    {
                        System.Diagnostics.Process.Start(this.attachmentsListView.SelectedItems[0].Tag.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        protected internal void AddAttachmentButton_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(this.openFileDialog.FileName))
                {
                    this.attachmentsListView.Items.Add(new ListViewItem(
                        Path.GetFileNameWithoutExtension(this.openFileDialog.FileName),
                        FileObject.Singleton.FileType(this.openFileDialog.FileName)) 
                        { 
                            Tag = this.openFileDialog.FileName 
                        });
                }

                if (this.attachmentsListView.Items.Count > 0)
                {
                    this.attachmentsListView.Enabled = true;
                    this.previousAttachmentButton.Enabled = this.nextAttachmentButton.Enabled = this.openAttachmentButton.Enabled = true;

                    if (this.GetSelectedIndex() == -1)
                    {
                        this.attachmentsListView.Items[0].Selected = true;
                        this.attachmentsListView.Items[0].Focused = true;
                        this.attachmentsListView.EnsureVisible(0);
                        this.attachmentsListView.Select();
                    }
                }

                this.ContentText_Changed(null, null);
            }
        }

        protected internal void RemoveAttachmentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.attachmentsListView.SelectedItems.Count > 0)
                {
                    this.attachmentsListView.Items.Remove(this.attachmentsListView.SelectedItems[0]);
                }
                else
                {
                    this.attachmentsListView.Enabled = false;
                    this.previousAttachmentButton.Enabled = this.nextAttachmentButton.Enabled = this.openAttachmentButton.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        /***************************
         *      Private methods
         ***************************/
        private void SetPendingPictureVisible(bool visible)
        {
            this.sendPendingPicture.Visible = visible;
        }

        private void ChangeButtonsState(bool state)
        {
            this.audioRecordStartButton.Enabled = state;
            this.audioPlaybackPlayButton.Enabled = state;
            this.audioPlaybackPauseButton.Enabled = state;
            this.audioPlaybackStopButton.Enabled = state;
            this.addAttachmentButton.Enabled = state;
            this.previousAttachmentButton.Enabled = state;
            this.openAttachmentButton.Enabled = state;
            this.nextAttachmentButton.Enabled = state;
            this.removeAttachmentButton.Enabled = state;
            this.cancelEmailButton.Enabled = state;
            this.sendEmailButton.Visible = state;
        }

        private void EnableButtons(bool active)
        {
            try
            {
                bool soundAvailable = File.Exists(Constants.CONFIG_MP3_TEMP_FILE);
                this.audioPlaybackPlayButton.Enabled = active;
                this.audioPlaybackPauseButton.Enabled = active;
                this.audioPlaybackStopButton.Enabled = active;
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        private int GetSelectedIndex()
        {
            for (int x = 0; x < this.contactsListView.Items.Count; x++)
            {
                if (this.contactsListView.Items[x].Selected)
                {
                    return x;
                }
            }

            return -1;
        }

        private int GetSelectedIndexPiece()
        {
            for (int x = 0; x < this.attachmentsListView.Items.Count; x++)
            {
                if (this.attachmentsListView.Items[x].Selected)
                {
                    return x;
                }
            }

            return -1;
        }

        private void EmptySelection()
        {
            this.recipientName.Text = this.recipientEmailAddress.Text = string.Empty;
            this.recipientPhoto.Image = null;
        }

        /********************************
         *      Private event handlers
         ********************************/   
        private void ContactsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.contactsListView.SelectedItems.Count > 0)
                {
                    this.EmptySelection();
                    Contact contact = ContactsObject.Singleton.GetContact(this.contactsListView.SelectedItems[0].Tag.ToString());
                    if (contact != null)
                    {
                        // Workaround to enable contact announcement only when this event handler is called by the list view click event
                        if (sender != null && e == null)
                        {
                            // Announce selected contact
                            SpeechObject.Singleton.Speak(contact.Name);
                        }

                        this.recipientName.Text = contact.Name;
                        this.recipientEmailAddress.Text = contact.Email;

                        this.recipientPhoto.Image = contact.Image != null ? contact.Image : Mail2Voice.properties.Resources.user_known;
                    }
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        private void AudioPlaybackEnding(object sender, EventArgs e)
        {
            this.music.Stop();
            this.timer.Stop();
            this.durationProgressBar.Value = 0;
            this.audioPlaybackPlayButton.Enabled = true;
            this.audioPlaybackPauseButton.Enabled = false;
            this.audioPlaybackStopButton.Enabled = false;
            this.recordDuration.Text = this.musicDuration;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (this.mp3Capture.Capturing)
                {
                    this.timeDelay++;
                    DateTime delay = new DateTime().AddSeconds(this.timeDelay);
                    this.recordDuration.Text = delay.Hour.ToString("00") + ":" + delay.Minute.ToString("00") + ":" + delay.Second.ToString("00");
                    this.ongoingRecordDuration.Text = this.recordDuration.Text;
                    this.ongoingRecordLabel.Visible = this.timeDelay % 2 == 0;
                }
                else
                {
                    if (this.music.State == StateFlags.Stopped)
                    {
                        this.timer.Enabled = false;
                        return;
                    }

                    this.durationProgressBar.Value = Convert.ToInt32(this.music.CurrentPosition * 1000);
                    DateTime delay = new DateTime().AddSeconds(this.music.CurrentPosition);
                    this.recordDuration.Text = delay.Hour.ToString("00") + ":" + delay.Minute.ToString("00") + ":" + delay.Second.ToString("00");
                }
            }
            catch (Exception ex)
            {
                LogObject.Singleton.Write(new System.Diagnostics.StackFrame().GetMethod().Name, ex);
            }
        }

        private void AudioRecordStopButton_Click(object sender, EventArgs e)
        {
            ((MainForm)this.Parent).SetRecordMode(false);
            this.mp3Capture.Stop();
            this.timer.Enabled = false;
            this.audioRecordStopButton.Hide();
            this.audioRecordStartButton.Show();
            this.audioPlaybackPlayButton.Enabled = true;
            this.ContentText_Changed(null, null);
            this.recordDuration.Visible = true;
            this.recordDurationLabel.Visible = true;
            this.durationProgressBar.Visible = true;
            this.ongoingRecordLabel.Visible = false;
            this.ongoingRecordDuration.Visible = false;
            this.ongoingRecordDuration.Text = "00:00:00";
            this.musicDuration = this.recordDuration.Text;
        }

        private void ContactsListView_Click(object sender, EventArgs e)
        {
            this.ContactsListView_SelectedIndexChanged(sender, null);
        }

        private void Mp3Capture_Stopped(object sender, Mp3SoundCapture.StoppedEventArgs e)
        {
            this.audioRecordStartButton.Enabled = true;
        }

        private void Mp3Capture_Stopping(object sender, EventArgs e)
        {
            this.audioRecordStartButton.Enabled = false;
        }

        private void Mp3Capture_Started(object sender, EventArgs e)
        {
            this.audioRecordStopButton.Enabled = true;
        }

        private void Mp3Capture_Starting(object sender, EventArgs e)
        {
            this.audioRecordStopButton.Enabled = false;
        }

        private void ContentText_Changed(object sender, EventArgs e)
        {
            if (this.Parent != null)
            {
                if (e == null || (this.emailTitle.Text != ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_SUBJECT) ||
                    this.emailContent.Text != ConfigSettings.Singleton.GetSettingValue(Constants.CONFIG_SEND_CONTENT))
                    || (this.recipientEmailAddress.Text != null && !string.IsNullOrEmpty(this.recipientEmailAddress.Text))
                    || (this.tmpVoiceMessageFilename != null && File.Exists(this.tmpVoiceMessageFilename)))
                {
                    ((MainForm)this.Parent).EditingEmail = true;
                }
                else
                {
                    ((MainForm)this.Parent).EditingEmail = false;
                }

                if (this.recipientEmailAddress.Text != null && !string.IsNullOrEmpty(this.recipientEmailAddress.Text) && !this.mp3Capture.Capturing)
                {
                    this.sendEmailButton.Enabled = true;
                }
                else
                {
                    this.sendEmailButton.Enabled = false;
                }
            }
        }

        private void KeyboardButton_Click(object sender, EventArgs e)
        {
            ((MainForm)this.Parent).ShowHideKeyboard(true, this.contactsPanel, this.emailContent);
        }
    }

    public partial class SentMailEventHandler
    {
        SendControl sendCtrl = null;

        public SentMailEventHandler(SendControl ctrl)
        {
            this.sendCtrl = ctrl;
            SendObject.SendEmailCompleted += new EventHandler(this.SendEmailCompleted);
            SendObject.SendEmailError += new EventHandler(this.SendEmailError);
        }

        private void SendEmailCompleted(object sender, EventArgs e)
        {
            this.sendCtrl.BeginInvoke(this.sendCtrl.SendEmailCompletedDelegate);
        }

        private void SendEmailError(object sender, EventArgs e)
        {
            this.sendCtrl.BeginInvoke(this.sendCtrl.SendEmailErrorDelegate);
        }
    }
}
