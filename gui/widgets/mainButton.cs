﻿/*
 *   Copyright: 2011-2015, Mail2Voice
 *   Authors: Olivier "oliversleep" Villedieu, Laurent "lclaude" Claude, Matthieu "mhatz" Hazon, Diana "idiana" Ibanescu
 *   
 *   This file is part of Mail2Voice.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2Voice.Widgets
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Mail2VoiceCore;

    public class MainButton : Button
    {
        /***************************
         *      Attributes
         ***************************/
        private static Color backColorInactive = Color.FromArgb(216, 234, 219);
        private static Color backColorActive = Color.White;

        private bool active;
        private bool cursorFocus;
        private Bitmap activeIcon;
        private Bitmap inactiveIcon;

        /***************************
         *      Constructor
         ***************************/
        public MainButton(Bitmap activeIcon, Bitmap inactiveIcon)
        {
            this.activeIcon = activeIcon;
            this.inactiveIcon = inactiveIcon;
            this.cursorFocus = false;
            this.MouseEnter += this.HandleMouseEnter;
            this.MouseLeave += this.HandleMouseLeave;
            this.MouseUp += this.AnnounceButton;
        }
            
        /***************************
         *      Public methods
         ***************************/
        public bool IsActive()
        {
            return this.active;
        }

        public void Activate()
        {
            this.active = true;
            this.BackColor = backColorActive;
            this.Image = this.activeIcon;
        }

        public void Deactivate()
        {
            this.active = false;
            this.BackColor = backColorInactive;
            this.Image = this.inactiveIcon;
        }

        public bool HasCursorFocus()
        {
            return this.cursorFocus;
        }

        /********************************
         *      Private event handlers
         ********************************/   
        private void HandleMouseEnter(object sender, EventArgs e)
        {
            this.cursorFocus = true;
        }

        private void HandleMouseLeave(object sender, EventArgs e)
        {
            this.cursorFocus = false;
        }

        private void AnnounceButton(object sender, EventArgs e)
        {
            SpeechObject.Singleton.Speak(this.Text);
        }
    }
}
