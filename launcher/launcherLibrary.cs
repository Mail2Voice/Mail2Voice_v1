﻿/*
 *   Copyright: 2011-2013, Mail2Voice
 *   Authors: Olivier VILLEDIEU, Laurent CLAUDE, Matthieu HAZON
 *   
 *   This file is part of Mail2Voice Launcher.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceLauncher
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Runtime.InteropServices;
    using ICSharpCode.SharpZipLib.Zip;

    public class LauncherLibrary
    {
        public enum Level
        {
            classic,
            debug
        }

        /***************************
         *      Attributes
         ***************************/
        private const int WM_CLOSE = 0x0010;
        private const int HTCAPTION = 2;

        #region "Singleton"
        private static LauncherLibrary instance = null;

        public static LauncherLibrary Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new LauncherLibrary();
                }

                return instance;
            }
        }

        /***************************
         *      Constructor
         ***************************/
        static LauncherLibrary()
        {
            instance = new LauncherLibrary();
        }

        #endregion "Singleton"

        /***************************
         *      Public methods
         ***************************/

        // Déclaration de l'API
        [DllImport("wininet.dll")]
        public static extern bool InternetGetConnectedState(out int description, int reservedValue);
       
        public bool IsConnected()
        {
            int desc;
            return InternetGetConnectedState(out desc, 0);
        }

        public void CloseWindow(string titre)
        {
            IntPtr hdlrWnd = FindWindow(null, titre);
            if (hdlrWnd != IntPtr.Zero)
            {
                SendMessage(hdlrWnd, WM_CLOSE, (UIntPtr)HTCAPTION, IntPtr.Zero);
            }
        }

        public bool IsWindowsVisible(string titre)
        {
            IntPtr hdlrWnd = FindWindow(null, titre);
            return hdlrWnd != IntPtr.Zero;
        }

        public string GetFileVersion(string file)
        {
            string version = "0.0.0";
            if (File.Exists(file))
            {
                FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(file);
                version = versionInfo.FileMajorPart + "." + versionInfo.FileMinorPart + "." + versionInfo.FileBuildPart;
            }

            return version;
        }

        public string GetMail2VoiceVersionMax(string file)
        {
            string version = "0.0.0.0";
            if (File.Exists(file))
            {
                FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(file);
                version = versionInfo.FileMajorPart + "." + versionInfo.FileMinorPart + "." + versionInfo.FileBuildPart + "." + versionInfo.FilePrivatePart;
            }

            return version;
        }

        public bool IsNewVersion(string oldVersion, string newVersion)
        {
            try
            {
                string oldV = oldVersion.Replace(".", string.Empty);
                string newV = newVersion.Replace(".", string.Empty);

                if (Convert.ToInt32(oldV) < Convert.ToInt32(newV))
                {
                    return true;
                }
            }
            catch
            {
            }

            return false;
        }

        public bool DownloadFile(string url, string outFile)
        {
            try
            {
                WebClient client = new WebClient();
                client.DownloadFile(url, outFile);
                client.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UnZipFile(string inputPathOfZipFile, string finalDirectory)
        {
            bool ret = true;
            try
            {
                if (File.Exists(inputPathOfZipFile))
                {
                    string baseDirectory = finalDirectory;

                    using (ZipInputStream zipStream = new ZipInputStream(File.OpenRead(inputPathOfZipFile)))
                    {
                        ZipEntry theEntry;
                        while ((theEntry = zipStream.GetNextEntry()) != null)
                        {
                            if (theEntry.IsFile)
                            {
                                if (theEntry.Name != string.Empty)
                                {
                                    string strNewFile = string.Empty + baseDirectory + @"\" + theEntry.Name;
                                    if (File.Exists(strNewFile))
                                    {
                                        // Tentative de suppression du fichier
                                        try
                                        {
                                            File.Delete(strNewFile);
                                        }
                                        catch
                                        {
                                            continue;
                                        }
                                    }

                                    using (FileStream streamWriter = File.Create(strNewFile))
                                    {
                                        int size = 2048;
                                        byte[] data = new byte[2048];
                                        while (true)
                                        {
                                            size = zipStream.Read(data, 0, data.Length);
                                            if (size > 0)
                                            {
                                                streamWriter.Write(data, 0, size);
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }

                                        streamWriter.Close();
                                    }
                                }
                            }
                            else if (theEntry.IsDirectory)
                            {
                                string strNewDirectory = string.Empty + baseDirectory + @"\" + theEntry.Name;
                                if (!Directory.Exists(strNewDirectory))
                                {
                                    Directory.CreateDirectory(strNewDirectory);
                                }
                            }
                        }

                        zipStream.Close();
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        [DllImport("User32.dll", SetLastError = true)]
        private static extern IntPtr FindWindow(string className, string windowName);

        [DllImport("User32.dll")]
        private static extern IntPtr SendMessage(IntPtr hdlrWnd, uint msg, UIntPtr winParam, IntPtr lfooParam);
    }

    public class VersionLineClass
    {
        /* MultiLignes :
         *version;surversion;date;url
         * 1.0.1;3;25/06/2011;http://ncu.dl.sourceforge.net/project/mail2voice/1.0.0%20RC1/Mail2Voice_1.0.0_RC1.zip */

        /***************************
        *      Constructor
        ***************************/
        public VersionLineClass(string line)
        {
            List<string> elems = line.Split(';').ToList();
            this.Version = elems[0];
            this.Subversion = elems[1];
            this.Date = elems[2];
            this.Url = elems[3];
        }

        /***************************
         *      Public methods
         ***************************/
        public string Version { get; set; }

        public string Subversion { get; set; }

        public string Date { get; set; }

        public string Url { get; set; }
    }
}