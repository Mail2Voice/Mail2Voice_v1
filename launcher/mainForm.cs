﻿/*
 *   Copyright: 2011-2013, Mail2Voice
 *   Authors: Olivier VILLEDIEU, Laurent CLAUDE, Matthieu HAZON
 *   
 *   This file is part of Mail2Voice Launcher.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceLauncher
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Forms;

    public partial class MainForm : Form
    {
        /***************************
         *      Attributes
         ***************************/
        private bool cancelWanted = false;

        private string distantVersionFile = @"http://www.mail2voice.org/versions/m2v.txt";
        private string zippedFile = @"./new_versionning.zip";
        private string unzippedDirectory = @"./";
        private string exeFileWithPrefix = @"./mail2voice.exe";
        private string exeFile = @"mail2voice.exe";
        private string fileVersionPath = @"./version.txt";
        private bool isActivated = false;
        private string applicationId = "mail2voice";
        private bool noClose = false;

        /***************************
         *      Constructor
         ***************************/
        public MainForm(bool noclose)
        {
            this.noClose = noclose;
            this.InitializeComponent();
        }

        /***************************
         *      Private methods
         ***************************/
        private void CloseApp()
        {
            this.Dispose();
            this.Close();
            Application.Exit();
        }

        private void LoadApp()
        {
            if (!this.Created || !this.Visible)
            {
                this.Load += delegate { this.LoadApp(); };
                return;
            }

            if (this.cancelWanted)
            {
                return;
            }

            Application.DoEvents();

            // Close Mail2Voice if running
            if (!this.noClose)
            {
                LauncherLibrary.Singleton.CloseWindow(this.applicationId);
            }

            // Window loaded
            bool isConnected = LauncherLibrary.Singleton.IsConnected();
            if (this.cancelWanted)
            {
                return;
            }

            if (isConnected == false)
            {
                // Connection KO
                if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                {
                    this.StartMail2Voice();
                }

                this.Close();
                return;
            }
            else
            {
                // Delete previous version file
                if (File.Exists(this.fileVersionPath))
                {
                    File.Delete(this.fileVersionPath);
                }

                // Download version file
                if (!LauncherLibrary.Singleton.DownloadFile(this.distantVersionFile, this.fileVersionPath))
                {
                    if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                    {
                        this.StartMail2Voice();
                    }

                    this.Close();
                    return;
                }

                // Read latest version available
                if (this.cancelWanted)
                {
                    return;
                }

                if (!File.Exists(this.fileVersionPath))
                {
                    if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                    {
                        this.StartMail2Voice();
                    }

                    this.Close();
                    return;
                }
                else
                {
                    List<VersionLineClass> fileVersionLines = new List<VersionLineClass>();
                    StreamReader sr = new StreamReader(this.fileVersionPath);
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        if (line.Split(';').Length == 4)
                        {
                            fileVersionLines.Add(new VersionLineClass(line));
                        }
                    }

                    sr.Close();
                    if (this.cancelWanted)
                    {
                        return;
                    }

                    if (fileVersionLines.Count == 0)
                    {
                        // MiseAJourAffichage("Erreur rencontrée durant la lecture du fichier de version, démarrage de Mail2Voice");
                        if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                        {
                            this.StartMail2Voice();
                        }

                        this.Close();
                        return;
                    }
                   
                    string currentVersion = string.Empty;
                    VersionLineClass selectedVersion = null;
                    bool downloadFinished = false;
                    bool isNewVersionAvailable = false;
                    bool alreadyQuestion = false;
                    LauncherLibrary.Level selectedLevel = LauncherLibrary.Level.debug;

                    if (this.cancelWanted)
                    {
                        return;
                    }

                    foreach (VersionLineClass vlc in fileVersionLines)
                    {
                        selectedVersion = null;
                        if (selectedLevel == LauncherLibrary.Level.debug)
                        {
                            currentVersion = LauncherLibrary.Singleton.GetMail2VoiceVersionMax(Application.ExecutablePath);
                            if (LauncherLibrary.Singleton.IsNewVersion(currentVersion, vlc.Version + vlc.Subversion))
                            {
                                selectedVersion = vlc;
                                isNewVersionAvailable = true;
                            }
                        }
                        else
                        {
                            currentVersion = LauncherLibrary.Singleton.GetFileVersion(this.exeFileWithPrefix);
                            if (LauncherLibrary.Singleton.IsNewVersion(currentVersion, vlc.Version))
                            {
                                selectedVersion = vlc;
                                isNewVersionAvailable = true;
                            }
                        }

                        if (selectedVersion != null)
                        {
                            if (!alreadyQuestion)
                            {
                                if (MessageBox.Show("An update is available, do you want to install it?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                                {
                                    // Check if Mail2Voice is already running before starting
                                    if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                                    {
                                        this.StartMail2Voice();
                                    }

                                    this.Close();
                                    return;
                                }

                                alreadyQuestion = true;
                            }

                            try
                            {
                                if (!LauncherLibrary.Singleton.DownloadFile(selectedVersion.Url, this.zippedFile))
                                {
                                    if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                                    {
                                        this.StartMail2Voice();
                                    }

                                    this.Close();
                                    return;
                                }
                               
                                LauncherLibrary.Singleton.UnZipFile(this.zippedFile, this.unzippedDirectory);

                                downloadFinished = true;
                            }
                            catch
                            {
                                downloadFinished = false;
                            }
                        }

                        if (downloadFinished)
                        {
                            break;
                        }
                            
                        if (this.cancelWanted) 
                        {
                            return;
                        }
                    }

                    if (!isNewVersionAvailable)
                    {
                        if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                        {
                            this.StartMail2Voice();
                        }

                        this.Close();
                        return;
                    }
                    else
                    {
                        if (downloadFinished)
                        {
                            if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                            {
                                this.StartMail2Voice();
                            }

                            this.Close();
                            return;
                        }
                        else
                        {
                            if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
                            {
                                this.StartMail2Voice();
                            }

                            this.Close();
                            return;
                        }
                    }
                }
            }
        }

        private void StartMail2Voice()
        {
            File.Delete(this.zippedFile);
            File.Delete(this.fileVersionPath);

            // Check if Mail2Voice exists before starting it
            if (!File.Exists(this.exeFileWithPrefix))
            {
                MessageBox.Show("File " + this.exeFileWithPrefix + " not found. Please reinstall Mail2Voice.");
                this.CloseApp();
            }

            // Start Mail2Voice
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = this.exeFile;
            startInfo.Arguments = "noupdate";
            Process.Start(startInfo);

            // Close application
            this.CloseApp();
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
            if (!this.isActivated)
            {
                this.isActivated = true;
                this.LoadApp();
            }
        }

        /********************************
         *      Private event handlers
         ********************************/   
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.cancelWanted = true;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to stop the updater?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            this.cancelWanted = true;
            if (!LauncherLibrary.Singleton.IsWindowsVisible(this.applicationId))
            {
                this.StartMail2Voice();
            }

            this.Close();
        }
    }
}
