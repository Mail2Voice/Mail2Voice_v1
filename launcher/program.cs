﻿/*
 *   Copyright: 2011-2013, Mail2Voice
 *   Authors: Olivier VILLEDIEU, Laurent CLAUDE, Matthieu HAZON
 *   
 *   This file is part of Mail2Voice Launcher.
 *
 *   Mail2Voice is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mail2Voice is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mail2Voice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mail2VoiceLauncher
{
    using System;
    using System.Windows.Forms;

    public static class Program
    {
        // Point d'entrée principal de l'application.
        [STAThread]
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            bool noclose = false;
            if (args.Length > 0)
            {
                foreach (string s in args)
                {
                    if (s == "noclose") 
                    {
                        noclose = true;
                    }
                }
            }

            Application.Run(new MainForm(noclose));
        }
    }
}
