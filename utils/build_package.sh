#!/bin/bash

if [ $# -ne 1 ]; then
	echo "Usage: $0 x.x.x.x"
	exit -1
fi

if [ ! -f mail2voice.sln ]; then
    echo "$0 must be started from source root directory."
	exit -2
fi

if [[ $1 != [0-9]\.[0-9]\.[0-9]\.[0-9] ]]; then
	echo "Wrong version format. Must be x.x.x.x !"
	exit -3
fi

export PATH=$PATH:"/c/Program Files (x86)/Git/bin":"/c/Program Files (x86)/Xamarin Studio/bin"

sed -b -i "s/[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+/$1/" properties/AssemblyInfo.cs

git add properties/AssemblyInfo.cs
git commit -m"Build release $1"
git tag $1

rm -rf bin
mdtool build --configuration:"Release|x86" mail2voice.sln

mv bin "mail2voice-$1"

utils/7za920/7za.exe a mail2voice-$1.zip mail2voice-$1/

exit 0