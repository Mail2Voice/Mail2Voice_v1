@echo off

set release_directory=./bin/release/
set output_directory=./bin/mail2voice/
set nom_application =mail2voice


echo ================================================
echo Verification existance de :  %release_directory%
if exist %release_directory% goto EXIST
echo repertoire non existant... Fin de la compression
goto ENDOFBATCH

:EXIST
echo Repertoire existant... Debut de la compression
if exist %output_directory% goto EXISTOUTPUT

:COMPRESSION
echo ================================================
echo D�but de la compression des fichiers de %nom_application%
Utils\netz-bin\netz.exe -s -w %release_directory%mail2voice.exe %release_directory%ImapX.dll %release_directory%Istrib.Sound.dll %release_directory%Istrib.Sound.Mp3.dll  %release_directory%yeti.mmedia.dll %release_directory%yeti.mp3.dll -o %output_directory%
echo Fin de la compression des fichiers de %nom_application%

echo Copie des fichiers non compactables
copy "%release_directory%lame_enc.dll" "%output_directory%"
copy "%release_directory%Microsoft.*.dll" "%output_directory%"
copy "%release_directory%License.txt" "%output_directory%"
copy "%release_directory%Interop.ADODB.dll" "%output_directory%"
copy "%release_directory%Interop.CDO.dll" "%output_directory%"
echo Fin de la copie des fichiers non compactables

echo Copie des fichiers de langue
echo Cr�ation du fichier de langue
md "%output_directory%lng"
copy "%release_directory%lng\*.lng" "%output_directory%lng"
echo Fin de la copie des fichiers de langue


goto ENDOFBATCH



:EXISTOUTPUT
echo ================================================
echo R�pertoire de sortie existant : %output_directory%
echo Suppression du r�pertoire de sortie
rd /S /Q "%output_directory%"
echo Suppression de %output_directory% termin�e
goto :COMPRESSION


:ENDOFBATCH
echo ================================================
echo Fin du traitement
pause