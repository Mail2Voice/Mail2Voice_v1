#!/bin/bash

# Move to project root directory
scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
outputDir=$scriptDir/..
cd $outputDir

output=$PWD/locales/"default.lng"

rm $output

t='    ' # Store tab
n=$'\n' # Store newline special character

# Write header
echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>$n"\
"<Mail2Voice>$n"\
"$t<Description>$n"\
"$t$t<Name>English</Name>$n"\
"$t$t<CultureName>en-US</CultureName>$n"\
"$t$t<Author>Mail2Voice</Author>$n"\
"$t$t<Mail>contact@mail2voice.org</Mail>$n"\
"$t</Description>$n"\
"$t<Language>" >> $output

# Loop through .Designer.cs files to extract all translatable widgets name and default text
forms=`find gui/ -name *.Designer.cs | sort`
for form in $forms
do
    echo $form 
    formName=`perl -ne '/this\.Name *= *"(.*)"/ && print "$1"' $form`
    echo "$t$t<$formName>" >> $output
    perl -ne '/this\.([a-zA-Z0-9_]+)(\.Text|\.HeaderText)\s*=\s*"(.*)"/ && print "            <setting name=\"$1\" value=\"$3\" />\n"' $form | sort >> $output
    echo "$t$t</$formName>" >> $output
done

# Loop through panels/*Control.cs files to extract default voice strings
echo "$t$t<voiceSentences>" >> $output
controls=`find gui/panels -name *Control.cs | sort`
for control in $controls
do
    echo $control
    grep -q AddVoiceString $control
    if [ $? -eq 0 ]; then
        controlName=`perl -ne '/public\spartial\sclass\s(.*)\s:\sUserControl/ && print "$1"' $control`
        echo "$t$t$t<!-- $controlName -->" >> $output
        perl -ne '/AddVoiceString\(\s*"(.*)",\s*"(.*)"/ && print "            <setting name=\"$1\" value=\"$2\" />\n"' $control | sort >> $output
    fi
done
echo "$t$t</voiceSentences>" >> $output

# Close XML sections and file
echo "$t</Language>$n"\
"</Mail2Voice>" >> $output
     
echo "Default locale generated in $output"

exit 0
